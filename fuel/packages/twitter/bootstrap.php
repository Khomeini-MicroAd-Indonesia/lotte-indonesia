<?php

Autoloader::add_core_namespace('Twitter');

Autoloader::add_classes(array(
    /**
     * Twitter classes.
     */
    'Twitter\\TwitterAPIExchange' => __DIR__ . '/classes/TwitterAPIExchange.php',
));
