<?php

/* detail.twig */
class __TwigTemplate_8f1e7adb0ba1daec1f548456b1dfc30b795a37d9304c2c2b7c3b7f4807907743 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_promo' => array($this, 'block_menu_promo'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 5
        echo Uri::base();
        echo "assets/css/custom/event-detail.css\" rel=\"stylesheet\">
";
    }

    // line 7
    public function block_menu_promo($context, array $blocks = array())
    {
        // line 8
        echo "    is-active
";
    }

    // line 11
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 12
        echo "    <div class=\"row event-detail\">
        <div class=\"large-2 medium-3 small-12 columns left-section\">
            <ul class=\"accordion year\" data-accordion=\"myAccordionGroup\" role=\"tablist\">
            ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["event_list"]) ? $context["event_list"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["event_year"] => $context["event_data"]) {
            // line 16
            echo "                <li class=\"accordion-navigation content-nav\">
                    <a href=\"#panel1c2015";
            // line 17
            echo (isset($context["event_year"]) ? $context["event_year"] : null);
            echo "\">";
            echo (isset($context["event_year"]) ? $context["event_year"] : null);
            echo "<span class=\"year-icon\"></span></a>
                    <div id=\"panel1c2015";
            // line 18
            echo (isset($context["event_year"]) ? $context["event_year"] : null);
            echo "\" class=\"content ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("active") : (""));
            echo "\">
                        <ul class=\"month\">
                        ";
            // line 20
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["event_data"]) ? $context["event_data"] : null));
            foreach ($context['_seq'] as $context["event_month"] => $context["detail"]) {
                // line 21
                echo "                            <li class=\"month-detail\"><a href=\"";
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/event/";
                echo (isset($context["event_year"]) ? $context["event_year"] : null);
                echo "/";
                echo (isset($context["event_month"]) ? $context["event_month"] : null);
                echo "\">";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "month_display");
                echo "</a></li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['event_month'], $context['detail'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "                        </ul>
                    </div>
                </li>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['event_year'], $context['event_data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "            </ul>
        </div>
        <div class=\"large-10 medium-9 small-12 columns right-section\">
        ";
        // line 30
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["event_detail"]) ? $context["event_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
            // line 31
            echo "            <div class=\"row banner-event\">
                <div class=\"large-10 medium-10 small-12 small-only-text-center columns\">
                    <img src=\"";
            // line 33
            echo Uri::base();
            echo "media/event/";
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "banner");
            echo "\">
                </div>
                <div class=\"large-2 medium-2 small-12 columns share-to\">
                    Share to :
                    <ul>
                        <li><img class=\"social-media facebook\"> </li>
                        <li><img class=\"social-media twitter\"> </li>
                        <li><img class=\"social-media gplus\"> </li>
                    </ul>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"large-10 medium-10 columns\">
                    <h3 class=\"title-event\">";
            // line 46
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "title");
            echo "</h3>
                    <p class=\"date-desc\">";
            // line 47
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "date");
            echo "</p>
                    <div class=\"pre-tag-event\">
                        <p class=\"tag-event\">
                            <span><img src=\"";
            // line 50
            echo Uri::base();
            echo "assets/img/icon/event.png\"></span>
                            Event
                        </p>
                        <hr class=\"gap-line\"/>
                        <div style=\"clear: both\"></div>
                    </div>
                    <p>";
            // line 56
            echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "content");
            echo "</p>
                    <div class=\"brand-slider\">
                        <div class=\"large-12 medium-12 small-12 large-centered medium-centered columns end\">
                            <ul class=\"text-center slide5\" id=\"brand-slide\">
                            ";
            // line 60
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["event_slide"]) ? $context["event_slide"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["slide"]) {
                // line 61
                echo "                                <li>
                                    <div class=\"event-display\">
                                        <img src=\"";
                // line 63
                echo Uri::base();
                echo "media/event/";
                echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "image");
                echo "\" class=\"bottom-gap\">
                                    </div>
                                    <div class=\"event-desc\">
                                        <p class=\"desc\">";
                // line 66
                echo $this->getAttribute((isset($context["slide"]) ? $context["slide"] : null), "desc");
                echo "</p>
                                    </div>
                                </li>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['slide'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "                            </ul>
                        </div>
                    </div>
                    <div class=\"link-event\">
                        <p>Publikasi media :</p>
                        <ul>
                        ";
            // line 76
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["event_publication"]) ? $context["event_publication"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["publication"]) {
                // line 77
                echo "                            <li><a href=\"";
                echo $this->getAttribute((isset($context["publication"]) ? $context["publication"] : null), "url");
                echo "\">";
                echo $this->getAttribute((isset($context["publication"]) ? $context["publication"] : null), "url");
                echo "</a></li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['publication'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "                        </ul>
                    </div>


                    <ul class=\"large-block-grid-2 related-link\">
                    ";
            // line 84
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["event_prev"]) ? $context["event_prev"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
                // line 85
                echo "                        <li>
                            <a href=\"";
                // line 86
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/event/detail/";
                echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "slug");
                echo "\"><span><img class=\"icon-prev-next prev\"></span>";
                echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "title");
                echo "</a>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["event_next"]) ? $context["event_next"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["event"]) {
                // line 90
                echo "                        <li>
                            <a href=\"";
                // line 91
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/event/detail/";
                echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "slug");
                echo "\"><span>";
                echo $this->getAttribute((isset($context["event"]) ? $context["event"] : null), "title");
                echo "<img class=\"icon-prev-next next\"></span></a>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "                    </ul>
                    <ul class=\"large-block-grid-3 medium-block-grid-3 small-block-grid-2 related-event\">
                    ";
            // line 96
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["promo_image"]) ? $context["promo_image"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["promo"]) {
                // line 97
                echo "                        <li class=\"promo-thumb-list\">
                            <a href=\"";
                // line 98
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/promo/detail/";
                echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "slug");
                echo "\"><img src=\"";
                echo Uri::base();
                echo "media/promo/";
                echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "image");
                echo "\"></a>
                            <a href=\"";
                // line 99
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/promo/detail/";
                echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "slug");
                echo "\">
                                <div class=\"promo-thumb-desc\">
                                    <p class=\"description\">";
                // line 101
                echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "title");
                echo "title</p>
                                    <p class=\"sub-description\">";
                // line 102
                echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "date");
                echo "date</p>
                                </div>
                            </a>
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promo'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 107
            echo "                    </ul>
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['event'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "        </div>
    </div>
";
    }

    // line 115
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 116
        echo "    <script src=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.js\"></script>
    <script>
        \$(document).foundation();
        \$(document).ready(function(){
            \$('.bxslider').bxSlider();
            \$('.slide5').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                slideWidth: 900,
                slideMargin: 10,
                infiniteLoop : false,
                hideControlOnEnd : true
            });

        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 116,  342 => 115,  336 => 111,  327 => 107,  316 => 102,  312 => 101,  305 => 99,  295 => 98,  292 => 97,  288 => 96,  284 => 94,  271 => 91,  268 => 90,  263 => 89,  250 => 86,  247 => 85,  243 => 84,  236 => 79,  225 => 77,  221 => 76,  213 => 70,  203 => 66,  195 => 63,  191 => 61,  187 => 60,  180 => 56,  171 => 50,  165 => 47,  161 => 46,  143 => 33,  139 => 31,  135 => 30,  130 => 27,  113 => 23,  98 => 21,  94 => 20,  87 => 18,  81 => 17,  78 => 16,  61 => 15,  56 => 12,  53 => 11,  48 => 8,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
