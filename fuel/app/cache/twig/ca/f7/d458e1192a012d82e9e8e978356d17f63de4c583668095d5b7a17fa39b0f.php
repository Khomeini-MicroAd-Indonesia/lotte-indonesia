<?php

/* pages/views/template_frontend.twig */
class __TwigTemplate_caf7d458e1192a012d82e9e8e978356d17f63de4c583668095d5b7a17fa39b0f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_home' => array($this, 'block_menu_home'),
            'menu_about_us' => array($this, 'block_menu_about_us'),
            'menu_product' => array($this, 'block_menu_product'),
            'menu_promo' => array($this, 'block_menu_promo'),
            'menu_careers' => array($this, 'block_menu_careers'),
            'menu_contact_us' => array($this, 'block_menu_contact_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"ie8\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
    <title>";
        // line 6
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 7
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <link rel=\"shortcut icon\" href=\"";
        // line 9
        echo Uri::base();
        echo "assets/img/favicon.ico\" />
    <link rel=\"stylesheet\" href=\"";
        // line 10
        echo Uri::base();
        echo "assets/foundation/css/normalize.min.css\">
    <link href=\"";
        // line 11
        echo Uri::base();
        echo "assets/foundation/css/foundation-apps-1.1.0.min.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 12
        echo Uri::base();
        echo "assets/foundation/foundation-icons/foundation-icons.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" href=\"";
        // line 13
        echo Uri::base();
        echo "assets/foundation/css/foundation.min.css\">
    <link href=\"";
        // line 14
        echo Uri::base();
        echo "assets/css/custom/custom.css\" rel=\"stylesheet\" type=\"text/css\">
    ";
        // line 15
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 16
        echo "    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 22
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
<!--header-->
<div class=\"\">
    <div class=\" default-color hidden-for-small-only\">
        <div class=\"row\" style=\"padding-top: 18px;padding-bottom: 18px;\">
            <ul class=\"logo\">
                <li class=\"large-6 medium-6 small-12 small-only-text-center columns\"><a href=\"";
        // line 32
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/home\"><img src=\"";
        echo Uri::base();
        echo "assets/img/logo-lotte.png\"></a></li>
                <li class=\"large-3 medium-3 large-offset-1 medium-offset-1 small-12 small-only-text-center columns has-form\">
                <form class=\"navbar-form hidden-for-small-only\" role=\"search\" action=\"";
        // line 34
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/search\" id=\"form-search-product\">
                    <div >
                        <div class=\"row collapse\" style=\"position: relative\">
                            <input name=\"keyword\" type=\"text\" placeholder=\"Search\" id=\"txt-search-keyword\">
                            <a href=\"";
        // line 38
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/search\" class=\"alert button expand\"><img src=\"";
        echo Uri::base();
        echo "assets/img/icon/icon-search.png\"> </a>
                        </div>
                    </div>
                </form>
                </li>
                <li class=\"large-2 medium-2 small-12 small-only-text-center columns\">
                    <ul class=\"lang\">
                        ";
        // line 45
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["lang_item"]) {
            // line 46
            echo "                            <li>
                                <a href=\"";
            // line 47
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\">
                                    <img class=\"";
            // line 48
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "class");
            echo "\" src=\"";
            echo (Uri::base() . $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "icon"));
            echo "\" title=\"";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label");
            echo "\" />
                                </a>
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--menu-->
<div class=\"off-canvas-wrap\" data-offcanvas>
    <div class=\"inner-wrap\">
        <div class=\"\">
            <div class=\"hidden-for-small-only\">
                <div class=\"large-12 middle-12 columns menu\">
                    <div class=\"row\">
                        <ul class=\"small-block-grid-6 primary menu-bar ng-scope\">
                            <li class=\"";
        // line 66
        $this->displayBlock('menu_home', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/home\"><span class=\"ci-home\" style=\"font-size: 2rem;\"></span>";
        echo twig_upper_filter($this->env, Lang::get("mn_home"));
        echo "</a></li>
                            <li class=\"";
        // line 67
        $this->displayBlock('menu_about_us', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/about-us\"><span class=\"ci-info\" style=\"font-size: 2rem;\"></span>";
        echo twig_upper_filter($this->env, Lang::get("mn_about_us"));
        echo "</a></li>
                            <li class=\"";
        // line 68
        $this->displayBlock('menu_product', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"javascript:\" class=\"button\" data-dropdown=\"autoCloseExample\" aria-controls=\"autoCloseExample\" aria-expanded=\"false\"><span class=\"ci-product\" style=\"font-size: 2rem;\"></span>";
        echo twig_upper_filter($this->env, Lang::get("mn_product"));
        echo "</a>
                                <ul id=\"autoCloseExample\" class=\"f-dropdown\" data-dropdown-content tabindex=\"-1\" aria-hidden=\"true\" aria-autoclose=\"false\" tabindex=\"-1\">
                                    ";
        // line 70
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["product_submenus"]) ? $context["product_submenus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product_menu_item"]) {
            if (($this->getAttribute((isset($context["product_menu_item"]) ? $context["product_menu_item"] : null), "slug") != "product-others")) {
                // line 71
                echo "                                        <li><a href=\"";
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/product-details/";
                echo $this->getAttribute((isset($context["product_menu_item"]) ? $context["product_menu_item"] : null), "slug");
                echo "\">";
                echo twig_upper_filter($this->env, $this->getAttribute((isset($context["product_menu_item"]) ? $context["product_menu_item"] : null), "title"));
                echo "</a></li>
                                    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product_menu_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "                                    <li><a href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/product-others\">";
        echo Lang::get("mn_product_others");
        echo "</a></li>
                                </ul>
                            </li>
                            <li class=\"";
        // line 76
        $this->displayBlock('menu_promo', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"javascript:\" class=\"button\" data-dropdown=\"autoCloseExample2\" aria-controls=\"autoCloseExample2\" aria-expanded=\"false\"><span class=\"ci-promotion\" style=\"font-size: 2rem;\"></span>";
        echo twig_upper_filter($this->env, Lang::get("mn_news_and_events"));
        echo "</a>
                                <ul id=\"autoCloseExample2\" class=\"f-dropdown\" data-dropdown-content tabindex=\"-2\" aria-hidden=\"true\" aria-autoclose=\"false\" tabindex=\"-2\">
                                    <li><a href=\"";
        // line 78
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/event\">";
        echo twig_upper_filter($this->env, Lang::get("mn_news_and_events_dropdown"));
        echo "</a></li>
                                    <li><a href=\"";
        // line 79
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/promo\">PROMO</a></li>
                                </ul>
                            </li>
                            <li class=\"";
        // line 82
        $this->displayBlock('menu_careers', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/career\"><span class=\"ci-career\" style=\"font-size: 2rem;\"></span>";
        echo twig_upper_filter($this->env, Lang::get("mn_careers"));
        echo "</a></li>
                            <li class=\"";
        // line 83
        $this->displayBlock('menu_contact_us', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/contact-us\"><span class=\"ci-contact\" style=\"font-size: 2rem;\"></span>";
        echo twig_upper_filter($this->env, Lang::get("mn_contact_us"));
        echo "</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"show-for-small-only\" style=\"\">
                <nav class=\"tab-bar default-color\">
                    <div class=\"small-2 columns\">
                        <a class=\"left-off-canvas-toggle menu-icon\" href=\"#\"><span></span></a>
                    </div>
                    <div class=\"small-8 text-center columns\">
                        <a href=\"";
        // line 94
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/home\">
                         <img src=\"";
        // line 95
        echo Uri::base();
        echo "assets/img/logo-lotte.png\" class=\"m-logo\">
                        </a>
                    </div>
                    <div class=\"small-2 columns m-seach\">
                        <form class=\"navbar-form\" role=\"search\" action=\"";
        // line 99
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/search\" id=\"form-search-product\">
                            <div >
                                <div class=\"row collapse\">
                                    <input name=\"keyword\" type=\"search\" placeholder=\"Search\" id=\"txt-search-keyword\">
                                </div>
                            </div>
                        </form>
                    </div>
                </nav>

                <aside class=\"left-off-canvas-menu\">
                    <ul class=\"off-canvas-list\">
                        <li>
                            <ul class=\"small-block-grid-2\">
                                <li class=\"logo-toogle\"><img src=\"";
        // line 113
        echo Uri::base();
        echo "assets/img/icon/logo-lotte-grey.jpg\"></li>
                                <li class=\"close-toogle\"><button class=\"exit-off-canvas\"><img src=\"";
        // line 114
        echo Uri::base();
        echo "assets/img/icon/close.png\"> </button></li>
                            </ul>
                        </li>
                        ";
        // line 118
        echo "                        <li><a href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/home\">HOME</a></li>
                        <li><a href=\"";
        // line 119
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/about-us\">ABOUT US</a></li>
                        <li class=\"has-submenu\"><a href=\"#\">PRODUCT</a>
                            <ul class=\"left-submenu\">
                                <li class=\"back\"><a href=\"#\">Back</a></li>
                                ";
        // line 123
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["product_submenus"]) ? $context["product_submenus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product_menu_item"]) {
            if (($this->getAttribute((isset($context["product_menu_item"]) ? $context["product_menu_item"] : null), "slug") != "product-others")) {
                // line 124
                echo "                                    <li><a href=\"";
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/product-details/";
                echo $this->getAttribute((isset($context["product_menu_item"]) ? $context["product_menu_item"] : null), "slug");
                echo "\">";
                echo twig_upper_filter($this->env, $this->getAttribute((isset($context["product_menu_item"]) ? $context["product_menu_item"] : null), "title"));
                echo "</a></li>
                                ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product_menu_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "                                <li><a href=\"";
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/product-others\">OTHERS</a></li>
                            </ul>
                        </li>
                        <li class=\"has-submenu\"><a href=\"#\">NEWS & EVENTS</a>
                            <ul class=\"left-submenu\">
                                <li class=\"back\"><a href=\"#\">Back</a></li>
                                <li><a href=\"";
        // line 132
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/promo\">PROMO</a></li>
                                <li><a href=\"";
        // line 133
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/event\">NEWS & EVENT</a></li>
                            </ul>
                        </li>
                        <li><a href=\"";
        // line 136
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/career\">CAREERS</a></li>
                        <li><a href=\"";
        // line 137
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/contact-us\">CONTACT US</a></li>
                    </ul>
                </aside>
            </div>
        </div>
        <!--custom content-->
        ";
        // line 143
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 152
        echo "        <!--end custom content-->

        <a class=\"exit-off-canvas\"></a>
    </div>
</div>

<!--footer-->
<footer class=\"grey f-grey\">
    <div class=\"row\">
        <div class=\"large-12 columns copyright\">
            <div class=\"large-4 medium-12 small-12 columns no-padd text-center gap no-gap\">Copyright &copy; 2015 PT. Lotte Indonesia. All rights reserved.</div>
            <div class=\"large-1 medium-6 hide-for-small-only columns no-padd text-center gap\"><a href=\"";
        // line 163
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/home\">";
        echo Lang::get("mn_home");
        echo "</a></div>
            <div class=\"large-1 medium-6 hide-for-small-only columns end no-padd text-center\"><a href=\"";
        // line 164
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/contact-us\">";
        echo Lang::get("mn_contact_us");
        echo "</a></div>
        </div>
    </div>
</footer>
    <script src=\"";
        // line 168
        echo Uri::base();
        echo "assets/foundation/js/vendor/jquery.js\"></script>
    <script>
        document.write('<script src=";
        // line 170
        echo Uri::base();
        echo "assets/foundation/js/vendor/' +
                ('__proto__' in {} ? 'zepto' : 'jquery') +
                '.js><\\/script>')
    </script>
    <script src=\"";
        // line 174
        echo Uri::base();
        echo "assets/foundation/js/vendor/modernizr.js\"></script>
    <script src=\"";
        // line 175
        echo Uri::base();
        echo "assets/js/jquery.lazyload-any.js\"></script>
    <script src=\"";
        // line 176
        echo Uri::base();
        echo "assets/foundation/js/foundation.min.js\"></script>
    <script src=\"";
        // line 177
        echo Uri::base();
        echo "assets/foundation/js/foundation/foundation.dropdown.js\"></script>
    <script src=\"";
        // line 178
        echo Uri::base();
        echo "assets/foundation/js/foundation/foundation.orbit.js\"></script>
    <script>
        \$(document).foundation();
        \$(\"label\").on(\"click\", function(){
            \$(this).siblings(\".sub-menu\").slideToggle();
        });
    </script>
";
        // line 185
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 225
        echo "</body>
</html>";
    }

    // line 15
    public function block_frontend_css($context, array $blocks = array())
    {
        echo " ";
    }

    // line 66
    public function block_menu_home($context, array $blocks = array())
    {
    }

    // line 67
    public function block_menu_about_us($context, array $blocks = array())
    {
    }

    // line 68
    public function block_menu_product($context, array $blocks = array())
    {
    }

    // line 76
    public function block_menu_promo($context, array $blocks = array())
    {
    }

    // line 82
    public function block_menu_careers($context, array $blocks = array())
    {
    }

    // line 83
    public function block_menu_contact_us($context, array $blocks = array())
    {
    }

    // line 143
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 144
        echo "                ";
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 151
        echo "        ";
    }

    // line 144
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 145
        echo "                    <div class=\"scroll-top-wrapper \">
                        <span class=\"scroll-top-inner\">
                            <div class=\"fi-arrow-up large\"></div>
                        </span>
                    </div>
                ";
    }

    // line 185
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 186
        echo "    <script type=\"text/javascript\">
        \$('#form-search-product').submit(function() {
            
            var keyword = \$.trim(\$('#txt-search-keyword').val());
            //alert(keyword);
            if (keyword.length <= 0) {
                return false;
            } else {
                return true;
            }
        });
        \$(document).ready(function(){
            \$('.search-trigger').click(function(e){
                e.preventDefault();
                \$('.search').toggleClass('triggered');
            });
        });
        ";
        // line 203
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 223
        echo "    </script>
";
    }

    // line 203
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 204
        echo "            \$(function(){
                \$(document).on( 'scroll', function(){
                    if (\$(window).scrollTop() > 100) {
                        \$('.scroll-top-wrapper').addClass('show');
                    } else {
                        \$('.scroll-top-wrapper').removeClass('show');
                    }
                });
                \$('.scroll-top-wrapper').on('click', scrollToTop);
            });

            function scrollToTop() {
                verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
                element = \$('body');
                offset = element.offset();
                offsetTop = offset.top;
                \$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
            }
        ";
    }

    public function getTemplateName()
    {
        return "pages/views/template_frontend.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  523 => 204,  520 => 203,  515 => 223,  513 => 203,  494 => 186,  491 => 185,  482 => 145,  479 => 144,  475 => 151,  472 => 144,  469 => 143,  464 => 83,  459 => 82,  454 => 76,  449 => 68,  444 => 67,  439 => 66,  433 => 15,  428 => 225,  426 => 185,  416 => 178,  412 => 177,  408 => 176,  404 => 175,  400 => 174,  393 => 170,  388 => 168,  379 => 164,  373 => 163,  360 => 152,  358 => 143,  349 => 137,  345 => 136,  339 => 133,  335 => 132,  325 => 126,  311 => 124,  306 => 123,  299 => 119,  294 => 118,  288 => 114,  284 => 113,  267 => 99,  260 => 95,  256 => 94,  238 => 83,  230 => 82,  224 => 79,  218 => 78,  211 => 76,  202 => 73,  188 => 71,  183 => 70,  176 => 68,  168 => 67,  160 => 66,  144 => 52,  130 => 48,  126 => 47,  123 => 46,  119 => 45,  107 => 38,  100 => 34,  93 => 32,  80 => 22,  72 => 16,  70 => 15,  66 => 14,  62 => 13,  58 => 12,  54 => 11,  50 => 10,  46 => 9,  41 => 7,  37 => 6,  30 => 1,);
    }
}
