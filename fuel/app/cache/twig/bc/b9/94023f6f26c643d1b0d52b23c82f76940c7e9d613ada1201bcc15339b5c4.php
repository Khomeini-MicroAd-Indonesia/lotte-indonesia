<?php

/* list_prize.twig */
class __TwigTemplate_bcb994023f6f26c643d1b0d52b23c82f76940c7e9d613ada1201bcc15339b5c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
\t<!-- dataTables css -->
\t";
        // line 6
        echo Asset::css("datatables/dataTables.bootstrap.css");
        echo "
";
    }

    // line 9
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 10
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 13
        echo "\t\tPromo
\t\t<small>Prize List</small>
\t</h1>
\t";
        // line 17
        echo "\t<ol class=\"breadcrumb\">
\t\t<li><a href=\"";
        // line 18
        echo Uri::base();
        echo "backend\">Home</a></li>
\t\t<li><a href=\"";
        // line 19
        echo Uri::base();
        echo "backend/promo\">Prize</a></li>
\t\t<li class=\"active\">Prize List</li>
\t</ol>
</section>
";
    }

    // line 25
    public function block_backend_content($context, array $blocks = array())
    {
        // line 26
        echo "
";
        // line 27
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 28
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 30
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 33
        echo "
";
        // line 34
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 35
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 37
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 40
        echo "
\t<div class=\"box box-solid\">
\t\t<div class=\"box-body text-right\">
\t\t\t<a href=\"";
        // line 43
        echo Uri::base();
        echo "backend/promo/prize/add\">
\t\t\t\t<button class=\"btn btn-default\">Create</button>
\t\t\t</a>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"box\">
\t\t\t\t<div class=\"box-body table-responsive\">
\t\t\t\t\t<table id=\"table-promo-prizes\" class=\"table table-bordered table-striped\">
\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th>ID</th>
                                                        <th>Promo</th>
                                                        <th>Slug</th>
\t\t\t\t\t\t\t<th>Image</th>
\t\t\t\t\t\t\t<th>Status</th>
\t\t\t\t\t\t\t<th>Seq</th>
                                                        <th>Desc</th>
\t\t\t\t\t\t\t<th>&nbsp;</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 66
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["prize_list"]) ? $context["prize_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["prize"]) {
            // line 67
            echo "\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<td>";
            // line 68
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "id");
            echo "</td>
                                                        <td>";
            // line 69
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "get_promo_name", array(), "method");
            echo "</td>
                                                        <td>";
            // line 70
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "slug");
            echo "</td>
\t\t\t\t\t\t\t<td><img src=\"";
            // line 71
            echo Uri::base();
            echo "media/promo/thumbnail/";
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "filename");
            echo "\"</td>
\t\t\t\t\t\t\t<td>";
            // line 72
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "get_status_name", array(), "method");
            echo "</td>
\t\t\t\t\t\t\t<td>";
            // line 73
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "seq");
            echo "</td>
                                                        <td>";
            // line 74
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "desc");
            echo "</td>
\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t<div class=\"btn-group\">
\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default dropdown-toggle\" data-toggle=\"dropdown\">
\t\t\t\t\t\t\t\t\t\tAction <span class=\"caret\"></span>
\t\t\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" role=\"menu\">
\t\t\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 81
            echo Uri::base();
            echo "backend/promo/prize/edit/";
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "id");
            echo "\">Edit</a></li>
\t\t\t\t\t\t\t\t\t\t
                                        <li><a class=\"btn-delete\" data-url-delete=\"";
            // line 83
            echo Uri::base();
            echo "backend/promo/prize/delete/";
            echo $this->getAttribute((isset($context["prize"]) ? $context["prize"] : null), "id");
            echo "\" href=\"#\">Delete</a></li>
\t\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prize'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div><!-- /.box-body -->
\t\t\t</div><!-- /.box -->
\t\t</div>
\t</div>
";
    }

    // line 97
    public function block_backend_js($context, array $blocks = array())
    {
        // line 98
        echo "\t";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
\t<!-- DATA TABES SCRIPT -->
\t";
        // line 100
        echo Asset::js("plugins/datatables/jquery.dataTables.js");
        echo "
\t";
        // line 101
        echo Asset::js("plugins/datatables/dataTables.bootstrap.js");
        echo "
\t<!-- custom script -->
\t<script type=\"text/javascript\">
\t\t\$(function() {
\t\t\t\$(\"#table-promo-prizes\").dataTable( {
\t\t\t\t\"aoColumns\": [ 
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\tnull,
\t\t\t\t\t{ \"bSearchable\": false, \"bSortable\": false },
\t\t\t\t\t{ \"bSearchable\": false, \"bSortable\": false }
\t\t\t\t]
\t\t\t} );
\t\t} );
\t</script>
\t<!-- Dialog Confirmation Script -->
\t";
        // line 118
        echo Asset::js("backend-dialog.js");
        echo "
\t<script type=\"text/javascript\">
\t\tjQuery('#table-promo-prizes').on('click', '.btn-delete', function(e){
\t\t\te.preventDefault();
\t\t\tvar my = jQuery(this),
\t\t\t\turl_del = my.data('url-delete');
\t\t\tvar yes_callback = function(e){
\t\t\t\te.preventDefault();
\t\t\t\tjQuery(location).attr('href', url_del);
\t\t\t}
\t\t\tbackend_dialog.show_dialog_confirm('Are you sure want to delete this?', yes_callback);
\t\t});
        jQuery('#table-promo-prizes').on('click', '.btn-rearrange', function(e){
\t\t\te.preventDefault();
\t\t\tvar my = jQuery(this),
\t\t\t\turl_del = my.data('url-rearrange');
\t\t\tvar yes_callback = function(e){
\t\t\t\te.preventDefault();
\t\t\t\tjQuery(location).attr('href', url_del);
\t\t\t}
\t\t\tbackend_dialog.show_dialog_confirm('Are you sure want to rearrange from this?', yes_callback);
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "list_prize.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  245 => 118,  225 => 101,  221 => 100,  215 => 98,  212 => 97,  202 => 89,  188 => 83,  181 => 81,  171 => 74,  167 => 73,  163 => 72,  157 => 71,  153 => 70,  149 => 69,  145 => 68,  142 => 67,  138 => 66,  112 => 43,  107 => 40,  101 => 37,  97 => 35,  95 => 34,  92 => 33,  86 => 30,  82 => 28,  80 => 27,  77 => 26,  74 => 25,  65 => 19,  61 => 18,  58 => 17,  53 => 13,  49 => 10,  46 => 9,  40 => 6,  34 => 4,  31 => 3,);
    }
}
