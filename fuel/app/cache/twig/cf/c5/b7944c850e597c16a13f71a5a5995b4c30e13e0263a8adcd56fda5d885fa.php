<?php

/* product_details.twig */
class __TwigTemplate_cfc5b7944c850e597c16a13f71a5a5995b4c30e13e0263a8adcd56fda5d885fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_product' => array($this, 'block_menu_product'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 5
        echo Uri::base();
        echo "assets/css/custom/animate.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo Uri::base();
        echo "assets/css/custom/product-details.css\" rel=\"stylesheet\">
";
    }

    // line 8
    public function block_menu_product($context, array $blocks = array())
    {
        // line 9
        echo "    is-active
";
    }

    // line 11
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 12
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["product_detail"]) ? $context["product_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 13
            echo "    <div class=\"row banner\">
        <div class=\"large-12 columns product-banner\">
            <img src=\"";
            // line 15
            echo Uri::base();
            echo "media/product/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "banner");
            echo "\">
        </div>
    </div>
    <div class=\"row\">
        <div class=\"large-12 columns overview-animate\">
            <div class=\"overview\">
                <div class=\"pre-title-center\">
                    <p class=\"title\">OVERVIEW</p>
                    <div style=\"clear: both\"></div>
                </div>
                ";
            // line 25
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["product_overview"]) ? $context["product_overview"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
                // line 26
                echo "                <p class=\"desc-overview\">";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "content");
                echo "</p>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "            </div>
        </div>
        ";
            // line 30
            if ((twig_length_filter($this->env, (isset($context["product_variant"]) ? $context["product_variant"] : null)) > 0)) {
                // line 31
                echo "        <div class=\"pre-title-center with-desc\">
            <p class=\"title\">VARIANTS</p>
            <div style=\"clear: both\"></div>
        </div>
        <p class=\"title-desc text-center\">";
                // line 35
                echo Lang::get("txt_variasi");
                echo " ";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
                echo "</p>
        ";
            }
            // line 37
            echo "    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "    
    ";
        // line 40
        if ((twig_length_filter($this->env, (isset($context["product_variant"]) ? $context["product_variant"] : null)) > 0)) {
            // line 41
            echo "    <div class=\"brand-slider\">
        <div class=\"large-10 medium-10 large-centered medium-centered columns\">
            <ul class=\"text-center slide5\" id=\"brand-slide\" data-equalizer=\"\">
            ";
            // line 44
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["product_variant"]) ? $context["product_variant"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
                // line 45
                echo "                <li >
                    <div class=\"variants\" data-equalizer-watch=\"\">
                        <img src=\"";
                // line 47
                echo Uri::base();
                echo "media/product/";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "image");
                echo "\" class=\"bottom-gap\">
                        <p class=\"title-variants\">";
                // line 48
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
                echo "</p>
                        <p class=\"desc\">";
                // line 49
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "desc");
                echo "</p>
                    </div>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "            </ul>
        </div>
    </div>
    ";
        }
        // line 57
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["product_tvc"]) ? $context["product_tvc"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 58
            echo "    <div class=\"row video\" data-equalizer>
        <div class=\"large-7 medium-7 small-12 columns\" data-equalizer-watch>
            <div class=\"video-container\">
                <iframe width=\"640\" height=\"360\" src=\"";
            // line 61
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "link");
            echo "\" frameborder=\"0\" allowfullscreen></iframe>
            </div>
        </div>
        <div class=\"large-5 medium-5 small-12 columns video-desc\" data-equalizer-watch>
            <div class=\"pre-title\">
                <p class=\"title\">TV COMMERCIAL</p>
                <div style=\"clear: both\"></div>
            </div>
            <p class=\"sub-title\">";
            // line 69
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
            echo "</p>
            <p>(Deskripsi iklan)</p>
            <p>";
            // line 71
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "desc");
            echo "</p>
        </div>
        <div style=\"clear: both;\"></div>
    </div>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "    
    
    <div class=\"row socmed text-center\" id=\"socmed-row\"><img src=\"";
        // line 78
        echo Uri::base();
        echo "assets/img/icon/load.gif\"> </div>
    
    ";
        // line 80
        if ((twig_length_filter($this->env, (isset($context["product_instagram"]) ? $context["product_instagram"] : null)) > 0)) {
            // line 81
            echo "    <div class=\"row socmed instagram\">
        ";
            // line 82
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["product_instagram"]) ? $context["product_instagram"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
                // line 83
                echo "        <div class=\"large-12 medium-12 small-12 columns socmed-section\">
            <p class=\"socmed-kind\">INSTAGRAM <span class=\"right\">@";
                // line 84
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "link");
                echo "</span></p>
            <div class=\"description-socmed\">
                <div class=\"row\">
                    <div class=\"large-2 medium-2 small-12 small-only-text-center columns\">
                        <img src=\"";
                // line 88
                echo Uri::base();
                echo "media/product/";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "image");
                echo "\">
                    </div>
                    <div class=\"large-10 medium-10 small-12 columns\">
                        <p class=\"date\">";
                // line 91
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
                echo "</p>
                        <div class=\"large-9 medium-8 small-12 columns\">
                            <p>";
                // line 93
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "content");
                echo "</p>
                        </div>
                        <div class=\"large-3 medium-4 small-12 columns\">
                            <a href=\"https://instagram.com/";
                // line 96
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "link");
                echo "/\" target=\"_blank\">
                                <p class=\"button-socmed instagram\">Follow us on Instagram</p>
                            </a>
                            <div style=\"clear: both\"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 107
            echo "    </div>
    ";
        }
        // line 109
        echo "    ";
        $this->displayBlock('back_to_top', $context, $blocks);
    }

    public function block_back_to_top($context, array $blocks = array())
    {
        // line 110
        echo "        ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
    ";
    }

    // line 114
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 115
        echo "    ";
        // line 116
        echo "    <script src=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.js\"></script>
    <script type=\"text/javascript\">
        //\$(document).foundation();
        \$(document).ready(function(){
            //\$('.socmed-section').lazyload();
            \$('.bxslider').bxSlider();
            \$('.slide5').bxSlider({
                minSlides: 1,
                maxSlides: 3,
                slideWidth: 270,
                slideMargin: 20,
                infiniteLoop : false,
                hideControlOnEnd : true,
                auto:false,
                moveSlides: 1
            });
        });

        \$.ajax({
            method: \"POST\",
            url: \"";
        // line 136
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/get-product-socmed\",
            data: {slug: \"";
        // line 137
        echo (isset($context["product_slug"]) ? $context["product_slug"] : null);
        echo "\"}
        }).done(function( res ) {
            \$('#socmed-row').html(res);
            \$('.socmed-section').on('click', '#facebook-next', function(){
                if (!\$('.current', '.facebook-main').hasClass('last')) {
                    var fb_curr = \$('.current', '.facebook-main'),
                        fb_next = fb_curr.next();
                    fb_curr.removeClass('current');
                    fb_next.addClass('current');
                    if (\$('.current', '.facebook-main').hasClass('last')) {
                        \$('#facebook-next').addClass('disabled');
                    }
                    if (\$('#facebook-back').hasClass('disabled')) {
                        \$('#facebook-back').removeClass('disabled');
                    }
                }
            });
            \$('.socmed-section').on('click', '#facebook-back', function(){
                if (!\$('.current', '.facebook-main').hasClass('first')) {
                    var fb_curr = \$('.current', '.facebook-main'),
                        fb_prev = fb_curr.prev();
                    fb_curr.removeClass('current');
                    fb_prev.addClass('current');
                    if (\$('.current', '.facebook-main').hasClass('first')) {
                        \$('#facebook-back').addClass('disabled');
                    }
                    if (\$('#facebook-next').hasClass('disabled')) {
                        \$('#facebook-next').removeClass('disabled');
                    }
                }
            });
            \$('.socmed-section').on('click', '#twitter-next', function(){
                if (!\$('.current', '.twitter-main').hasClass('last')) {
                    var fb_curr = \$('.current', '.twitter-main'),
                        fb_next = fb_curr.next();
                    fb_curr.removeClass('current');
                    fb_next.addClass('current');
                    if (\$('.current', '.twitter-main').hasClass('last')) {
                        \$('#twitter-next').addClass('disabled');
                    }
                    if (\$('#twitter-back').hasClass('disabled')) {
                        \$('#twitter-back').removeClass('disabled');
                    }
                }
            });
            \$('.socmed-section').on('click', '#twitter-back', function(){
                if (!\$('.current', '.twitter-main').hasClass('first')) {
                    var fb_curr = \$('.current', '.twitter-main'),
                        fb_prev = fb_curr.prev();
                    fb_curr.removeClass('current');
                    fb_prev.addClass('current');
                    if (\$('.current', '.twitter-main').hasClass('first')) {
                        \$('#twitter-back').addClass('disabled');
                    }
                    if (\$('#twitter-next').hasClass('disabled')) {
                        \$('#twitter-next').removeClass('disabled');
                    }
                }
            });
        });
        ";
        // line 197
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 200
        echo "    </script>
";
    }

    // line 197
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 198
        echo "            ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "product_details.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  394 => 198,  391 => 197,  386 => 200,  384 => 197,  321 => 137,  317 => 136,  293 => 116,  291 => 115,  288 => 114,  281 => 110,  274 => 109,  270 => 107,  253 => 96,  247 => 93,  242 => 91,  234 => 88,  227 => 84,  224 => 83,  220 => 82,  217 => 81,  215 => 80,  210 => 78,  206 => 76,  195 => 71,  190 => 69,  179 => 61,  174 => 58,  169 => 57,  163 => 53,  153 => 49,  149 => 48,  143 => 47,  139 => 45,  135 => 44,  130 => 41,  128 => 40,  125 => 39,  118 => 37,  111 => 35,  105 => 31,  103 => 30,  99 => 28,  90 => 26,  86 => 25,  71 => 15,  67 => 13,  62 => 12,  59 => 11,  54 => 9,  51 => 8,  45 => 6,  41 => 5,  36 => 4,  33 => 3,);
    }
}
