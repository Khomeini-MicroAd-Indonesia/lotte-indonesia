<?php

/* search.twig */
class __TwigTemplate_3a6f30eaa167454164bfd9f352de6ec5da157992982b41493f193c7ec67701bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'front_js' => array($this, 'block_front_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/css/custom/search.css\" rel=\"stylesheet\">
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div id='content' class=\"tab-content search-content\">
        <div class=\"active\" id=\"searchactive\">
            <div class=\"row search\">
                <div class=\"search-header\">
                    <div class=\"large-9 medium-12 small-12 columns\">
                            <h2 class=\"search-results\">SEARCH RESULTS</h2>
                        </div>
                    <div class=\"large-3 medium-12 small-12 columns search-for\">
                            <h3>Your Search For :</h3>
                            <p>";
        // line 17
        echo (isset($context["keyword"]) ? $context["keyword"] : null);
        echo "</p>
                        </div>
                    <hr class=\"search-line\">
                </div>
                <div class=\"row\">
                    ";
        // line 22
        if ((twig_length_filter($this->env, (isset($context["search_result"]) ? $context["search_result"] : null)) < 1)) {
            // line 23
            echo "                    <div class=\"no-result\">
                        <div class=\"content-result\">
                            <img src=\"";
            // line 25
            echo Uri::base();
            echo "assets/img/icon/icon-no-search.png\">
                            <p class=\"\"style=\"font-size: 16px;font-weight: bold;font-weight: bold;color: #ffffff\">";
            // line 26
            echo Lang::get("txt-no-result");
            echo "</p>
                        </div>
                    </div>
                    ";
        }
        // line 30
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["search_result"]) ? $context["search_result"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            if (($this->getAttribute((isset($context["res"]) ? $context["res"] : null), "status") == "event")) {
                // line 31
                echo "                    <div class=\"text-center large-6 medium-6 small-6 columns hidden\">
                        <div class=\"product-list\" id=\"search-index-";
                // line 32
                echo $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index");
                echo "\">
                            <div class=\"wrapper-product-image\" id=\"imgsearch\">
                                <img alt=\"";
                // line 34
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "title");
                echo "\" src=\"";
                echo Uri::base();
                echo "media/event/";
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "image");
                echo "\" style=\"max-width:200px; max-height:200px;\" />
                            </div>
                            <p class=\"product-name\">";
                // line 36
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "title");
                echo "</p>
                        </div>
                        </div>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                </div>

                ";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["search_result"]) ? $context["search_result"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            if (($this->getAttribute((isset($context["res"]) ? $context["res"] : null), "status") == "product")) {
                // line 43
                echo "                    <div class=\"text-center large-3 medium-3 small-6 columns end hidden\" id=\"search-index-";
                echo $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index");
                echo "\">
                        <a href=\"#\">
                        <div class=\"product-list\">
                            <div class=\"wrapper-product-image\" id=\"imgsearch\">
                                <img alt=\"";
                // line 47
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "title");
                echo "\" src=\"";
                echo Uri::base();
                echo "media/product/";
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "image");
                echo "\" />
                            </div>
                            <p class=\"product-name\">";
                // line 49
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "title");
                echo "</p>
                        </div>
                        </a>
                    </div>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                
                ";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["search_result"]) ? $context["search_result"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        foreach ($context['_seq'] as $context["_key"] => $context["res"]) {
            if (($this->getAttribute((isset($context["res"]) ? $context["res"] : null), "status") == "promo")) {
                // line 56
                echo "                <div class=\"text-center large-3 medium-3 small-6 colums hidden\" id=\"search-index-";
                echo $this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index");
                echo "\">
                    <div class=\"product-list\">
                        <div class=\"wrapper-product-image\" id=\"imgsearch\">
                            <img alt=\"";
                // line 59
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "title");
                echo "\" src=\"";
                echo Uri::base();
                echo "media/promo/";
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "image");
                echo "\" />
                        </div>
                        <p class=\"product-name\">";
                // line 61
                echo $this->getAttribute((isset($context["res"]) ? $context["res"] : null), "title");
                echo "</p>
                    </div>
                </div>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['res'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "            </div>
        </div>
    </div>
";
    }

    // line 70
    public function block_front_js($context, array $blocks = array())
    {
        // line 71
        echo "    ";
        $this->displayParentBlock("front_js", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        var curr_idx = 0;
        function show_more_search() {
            for (inc = 0 ; inc < 4 ; inc++) {
                curr_idx++;
                var obj = \$('#search-index-'+curr_idx);
                if (obj.length > 0) {
                    obj.removeClass('hidden');
                    \$('#cnt-shown-product').text(curr_idx);
                }
            }
            if (curr_idx >= ";
        // line 83
        echo twig_length_filter($this->env, (isset($context["search_result"]) ? $context["search_result"] : null));
        echo ") {
                \$('#btn-show-more-search').remove();
            }
        }
        \$('#btn-show-more-search').click(function(){
            show_more_search();
        });
        show_more_search();
    </script>
";
    }

    public function getTemplateName()
    {
        return "search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 83,  223 => 71,  220 => 70,  213 => 65,  199 => 61,  190 => 59,  183 => 56,  172 => 55,  169 => 54,  154 => 49,  145 => 47,  137 => 43,  126 => 42,  122 => 40,  108 => 36,  99 => 34,  94 => 32,  91 => 31,  79 => 30,  72 => 26,  68 => 25,  64 => 23,  62 => 22,  54 => 17,  43 => 8,  40 => 7,  33 => 4,  30 => 3,);
    }
}
