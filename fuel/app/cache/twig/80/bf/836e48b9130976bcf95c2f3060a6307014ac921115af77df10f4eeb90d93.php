<?php

/* event.twig */
class __TwigTemplate_80bf836e48b9130976bcf95c2f3060a6307014ac921115af77df10f4eeb90d93 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"assets/css/custom/event.css\" rel=\"stylesheet\">
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"row event\" data-equalizer>
        <div class=\"large-2 medium-2 small-2 columns sidebar-year\" data-equalizer-watch>
            <ul class=\"accordion year\" data-accordion=\"myAccordionGroup\">
                <li class=\"accordion-navigation content-nav\">
                    <a href=\"#panel1c\">2015<span class=\"year-icon\"></span></a>
                    <div id=\"panel1c\" class=\"content\">
                        <ul class=\"month\">
                            <li class=\"month-detail\"><a href=\"#\">December</a></li>
                            <li class=\"month-detail\"><a href=\"#\">November</a></li>
                            <li class=\"month-detail\"><a href=\"#\">October</a></li>
                            <li class=\"month-detail\"><a href=\"#\">September</a></li>
                            <li class=\"month-detail\"><a href=\"#\">August</a></li>
                            <li class=\"month-detail\"><a href=\"#\">July</a></li>
                            <li class=\"month-detail\"><a href=\"#\">June</a></li>
                            <li class=\"month-detail\"><a href=\"#\">May</a></li>
                            <li class=\"month-detail\"><a href=\"#\">April</a></li>
                            <li class=\"month-detail\"><a href=\"#\">March</a></li>
                            <li class=\"month-detail\"><a href=\"#\">February</a></li>
                            <li class=\"month-detail\"><a href=\"#\">January</a></li>
                        </ul>
                    </div>
                </li>
                <li class=\"accordion-navigation content-nav\">
                    <a href=\"#panel2c\">2014<span class=\"year-icon\"></span></a>
                    <div id=\"panel2c\" class=\"content\">
                        <ul class=\"month\">
                            <li class=\"month-detail\"><a href=\"#\">December</a></li>
                            <li class=\"month-detail\"><a href=\"#\">November</a></li>
                            <li class=\"month-detail\"><a href=\"#\">October</a></li>
                            <li class=\"month-detail\"><a href=\"#\">September</a></li>
                            <li class=\"month-detail\"><a href=\"#\">August</a></li>
                            <li class=\"month-detail\"><a href=\"#\">July</a></li>
                            <li class=\"month-detail\"><a href=\"#\">June</a></li>
                            <li class=\"month-detail\"><a href=\"#\">May</a></li>
                            <li class=\"month-detail\"><a href=\"#\">April</a></li>
                            <li class=\"month-detail\"><a href=\"#\">March</a></li>
                            <li class=\"month-detail\"><a href=\"#\">February</a></li>
                            <li class=\"month-detail\"><a href=\"#\">January</a></li>
                        </ul>
                    </div>
                </li>
                <li class=\"accordion-navigation content-nav\">
                    <a href=\"#panel3c\">2013<span class=\"year-icon\"></span></a>
                    <div id=\"panel3c\" class=\"content\">
                        <ul class=\"month\">
                            <li class=\"month-detail\"><a href=\"#\">December</a></li>
                            <li class=\"month-detail\"><a href=\"#\">November</a></li>
                            <li class=\"month-detail\"><a href=\"#\">October</a></li>
                            <li class=\"month-detail\"><a href=\"#\">September</a></li>
                            <li class=\"month-detail\"><a href=\"#\">August</a></li>
                            <li class=\"month-detail\"><a href=\"#\">July</a></li>
                            <li class=\"month-detail\"><a href=\"#\">June</a></li>
                            <li class=\"month-detail\"><a href=\"#\">May</a></li>
                            <li class=\"month-detail\"><a href=\"#\">April</a></li>
                            <li class=\"month-detail\"><a href=\"#\">March</a></li>
                            <li class=\"month-detail\"><a href=\"#\">February</a></li>
                            <li class=\"month-detail\"><a href=\"#\">January</a></li>
                        </ul>
                    </div>
                </li>
                <li class=\"accordion-navigation content-nav\">
                    <a href=\"#panel4c\">2012<span class=\"year-icon\"></span></a>
                    <div id=\"panel4c\" class=\"content\">
                        <ul class=\"month\">
                            <li class=\"month-detail\"><a href=\"#\">December</a></li>
                            <li class=\"month-detail\"><a href=\"#\">November</a></li>
                            <li class=\"month-detail\"><a href=\"#\">October</a></li>
                            <li class=\"month-detail\"><a href=\"#\">September</a></li>
                            <li class=\"month-detail\"><a href=\"#\">August</a></li>
                            <li class=\"month-detail\"><a href=\"#\">July</a></li>
                            <li class=\"month-detail\"><a href=\"#\">June</a></li>
                            <li class=\"month-detail\"><a href=\"#\">May</a></li>
                            <li class=\"month-detail\"><a href=\"#\">April</a></li>
                            <li class=\"month-detail\"><a href=\"#\">March</a></li>
                            <li class=\"month-detail\"><a href=\"#\">February</a></li>
                            <li class=\"month-detail\"><a href=\"#\">January</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class=\"large-9 medium-9 small-9 columns wrapper-content\" data-equalizer-watch>
            <div class=\"row\" data-equalizer>
                <div class=\"large-3 medium-3 small-3 columns\" style=\"padding-right: 0\" data-equalizer-watch>
                    <a class=\"th\" style=\"border: none\">
                        <img src=\"assets/img/event/event-xylitol.png\">
                    </a>
                </div>
                <div class=\"large-9 medium-9 small-9 columns reset\" data-equalizer-watch>
                    <p class=\"title-event\">Senyum Jutawan Xylitol Grand Prize Hand Over</p>
                    <p class=\"date-event\">Senin, 19 Oktober 2015</p>
                    <hr class=\"hr-event\">
                    <p class=\"detail-event\">Setelah ±1 tahun consumer promo ‘Senyum Jutawan Xylitol’ diadakan dan berhasil mendapati 3000 pemenang yang tersebar di seluruh Indonesia, LOTTE mendapati pemenang beruntung yang mendapatkan kode unik untuk hadiah utama RP 100 Juta (tidak dipotong pajak) a/n Ibu.Novita dari BSD Tangerang. Acara penyerahan plakat hadiah ...</p>
                    <div class=\"large-4 medium-4 small-4 columns\" style=\"padding: 0; margin-top: 0.585rem\">
                        <a href=\"#\" class=\"btn-readmore\">READ MORE</a>
                    </div>
                    <div class=\"large-8 medium-8 small-8 columns shareto\">
                        Share to
                        <ul>
                            <li>
                                <img class=\"social-media facebook\">
                            </li>
                            <li>
                                <img class=\"social-media twitter\">
                            </li>
                            <li>
                                <img class=\"social-media gplus\">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"hr-divider\">
            <div class=\"row\">
                <div class=\"large-3 medium-3 small-3 columns\">
                    <a class=\"th\" style=\"border: none\">
                        <img src=\"assets/img/event/event-xylitol-2.png\">
                    </a>
                </div>
                <div class=\"large-9 medium-3 small-3 columns\">
                    <p class=\"title-event\">GAK SEGER GAK PEDE – HOP Sampling</p>
                    <p class=\"date-event\">Senin, 19 Oktober 2015</p>
                    <hr class=\"hr-event\">
                    <p class=\"detail-event\">Melalui event ini selain ingin meningkatkan brand awareness produk LOTTE Xylitol, GAK SEGER GAK PEDE juga bertujuan menekankan fungsi ‘REFRESHING’ yang dicari oleh kawula muda Indonesia yang juga merupakan target market dari LOTTE Xylitol. Walaupun hanya diadakan saat weekend, event yang dibarengi dengan sampling ini ...</p>
                    <div class=\"large-4 medium-4 small-4 columns\" style=\"padding: 0; margin-top: 0.585rem\">
                        <a href=\"#\" class=\"btn-readmore\">READ MORE</a>
                    </div>
                    <div class=\"large-8 medium-4 small-4 columns shareto\">
                        Share to
                        <ul>
                            <li>
                                <img class=\"social-media facebook\">
                            </li>
                            <li>
                                <img class=\"social-media twitter\">
                            </li>
                            <li>
                                <img class=\"social-media gplus\">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"hr-divider\">
            <div class=\"row\">
                <div class=\"large-3 medium-3 small-3 columns\">
                    <a class=\"th\" style=\"border: none\">
                        <img src=\"assets/img/event/event-choco.png\">
                    </a>
                </div>
                <div class=\"large-9 medium-9 small-9 columns\">
                    <p class=\"title-event\">Press Conference Lotte Choco Pie</p>
                    <p class=\"date-event\">Senin, 19 Oktober 2015</p>
                    <hr class=\"hr-event\">
                    <p class=\"detail-event\">Mengukuhkan posisi LOTTE Choco Pie di pasar Indonesia dimana Ibu yang memiliki anak berusia 6-12 tahun, melalui press conference LOTTE Indonesia ingin memberitahukan bahwa di tahun 2015 ini LOTTE Choco Pie akan melaksanakan campaign bertajuk ‘Best Bonding Best Moment’ dengan aktifitas marketing yang menarik secara national.</p>
                    <div class=\"large-4 medium-4 small-4 columns\" style=\"padding: 0; margin-top: 0.585rem\">
                        <a href=\"#\" class=\"btn-readmore\">READ MORE</a>
                    </div>
                    <div class=\"large-8 medium-8 small-8 columns shareto\">
                        Share to
                        <ul>
                            <li>
                                <img class=\"social-media facebook\">
                            </li>
                            <li>
                                <img class=\"social-media twitter\">
                            </li>
                            <li>
                                <img class=\"social-media gplus\">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"hr-divider\">
            <div class=\"row\">
                <div class=\"large-3 medium-3 small-3 columns\">
                    <a class=\"th\" style=\"border: none\">
                        <img src=\"assets/img/event/event-jutawan.png\">
                    </a>
                </div>
                <div class=\"large-9 medium-9 small-9 columns\">
                    <p class=\"title-event\">Senyum Jutawan Xylitol</p>
                    <p class=\"date-event\">Senin, 19 Oktober 2015</p>
                    <hr class=\"hr-event\">
                    <p class=\"detail-event\">Event yang bertajuk ‘Senyum Jutawan Xylitol’ ini adalah consumer promo pertama yang diadakan secara national dimana LOTTE Xylitol berkesempatan untuk bagi-bagi hadiah dengan total Milyaran Rupiah untuk loyal konsumen yang beruntung dan berhasil verifikasi kode unik yang bisa ditemukan dalam kemasan. Promo yang berjalan ...</p>
                    <div class=\"large-4 medium-4 small-4 columns\" style=\"padding: 0; margin-top: 0.585rem\">
                        <a href=\"#\" class=\"btn-readmore\">READ MORE</a>
                    </div>
                    <div class=\"large-8 medium-8 small-8 columns shareto\">
                        Share to
                        <ul>
                            <li>
                                <img class=\"social-media facebook\">
                            </li>
                            <li>
                                <img class=\"social-media twitter\">
                            </li>
                            <li>
                                <img class=\"social-media gplus\">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"hr-divider\">
            <div class=\"row\">
                <div class=\"large-3 medium-3 small-3 columns\">
                    <a class=\"th\" style=\"border: none\">
                        <img src=\"assets/img/event/event-toppo.png\">
                    </a>
                </div>
                <div class=\"large-9 medium-9 small-9 columns\">
                    <p class=\"title-event\">Toppo Top Girl</p>
                    <p class=\"date-event\">Senin, 19 Oktober 2015</p>
                    <hr class=\"hr-event\">
                    <p class=\"detail-event\">Ingin meningkatkan brand awareness, Toppo (stick pretzel isi cokelat) yang di distribusikan oleh LOTTE Trade & Distribution di Indonesia sukses mengadaptasi regional  campaign  dengan tema Toppo Top Girl. Ajang kompetisi ini dimulai dengan proses pendaftaran via online (pengiriman video melalui Media Social dan Microsite) dan offline ... </p>
                    <div class=\"large-4 medium-4 small-4 columns\" style=\"padding: 0; margin-top: 0.585rem\">
                        <a href=\"#\" class=\"btn-readmore\">READ MORE</a>
                    </div>
                    <div class=\"large-8 medium-8 small-8 columns shareto\">
                        Share to
                        <ul>
                            <li>
                                <img class=\"social-media facebook\">
                            </li>
                            <li>
                                <img class=\"social-media twitter\">
                            </li>
                            <li>
                                <img class=\"social-media gplus\">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"hr-divider\">
            <ul class=\"pagination page\">
                <li class=\"arrow unavailable\"><a href=\"\">&laquo;</a></li>
                <li class=\"arrow unavailable\" style=\"margin-right: 0.625rem\"><a href=\"\">&lsaquo;</a></li>
                <li class=\"current\"><a href=\"\">1</a></li>
                <li><a href=\"\">2</a></li>
                <li><a href=\"\">3</a></li>
                <li><a href=\"\">4</a></li>
                <li><a href=\"\">5</a></li>
                <li class=\"unavailable\"><a href=\"\">&hellip;</a></li>
                <li class=\"arrow\" style=\"margin-left: 0.625rem\"><a href=\"\">&rsaquo;</a></li>
                <li class=\"arrow\"><a href=\"\">&raquo;</a></li>
            </ul>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "event.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 8,  37 => 7,  32 => 4,  29 => 3,);
    }
}
