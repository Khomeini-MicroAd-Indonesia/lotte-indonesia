<?php

/* promo.twig */
class __TwigTemplate_9f7002c355b6a52358c4c9b2b40dd0225748ef51ff7959c19e4795350d15dbc3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_promo' => array($this, 'block_menu_promo'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/css/custom/promo.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_menu_promo($context, array $blocks = array())
    {
        // line 7
        echo "    is-active
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "<div class=\"row promo-thumb\">
    <ul class=\"large-block-grid-4 medium-block-grid-4 small-block-grid-1 promo-list\">
        ";
        // line 12
        if ((twig_length_filter($this->env, (isset($context["promo_image"]) ? $context["promo_image"] : null)) < 1)) {
            // line 13
            echo "        <p>";
            echo Lang::get("txt_no-promo");
            echo "</p>
        ";
        }
        // line 15
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promo_image"]) ? $context["promo_image"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["promo"]) {
            // line 16
            echo "        <li class=\"promo-thumb-list\">
            <a href=\"";
            // line 17
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/promo/detail/";
            echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "slug");
            echo "\"><img src=\"";
            echo Uri::base();
            echo "media/promo/";
            echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "image");
            echo "\"></a>
            <a href=\"";
            // line 18
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/promo/detail/";
            echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "slug");
            echo "\">
                <div class=\"promo-thumb-desc\">
                    <p class=\"description\">";
            // line 20
            echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "title");
            echo "</p>
                    <p class=\"sub-description\">";
            // line 21
            echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "date");
            echo "</p>
                </div>
            </a>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "    </ul>
</div>
";
    }

    // line 30
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 31
        echo "    <script type=\"text/javascript\">
    </script>
";
    }

    public function getTemplateName()
    {
        return "promo.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 31,  110 => 30,  104 => 26,  93 => 21,  89 => 20,  82 => 18,  72 => 17,  69 => 16,  64 => 15,  58 => 13,  56 => 12,  52 => 10,  49 => 9,  44 => 7,  41 => 6,  34 => 4,  31 => 3,);
    }
}
