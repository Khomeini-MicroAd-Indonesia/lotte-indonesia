<?php

/* event.twig */
class __TwigTemplate_1c42ac6f769a0be7843c41131fd33c7af271a3f6d635a0fe4f9f08142eb9fd2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/css/custom/event.css\" rel=\"stylesheet\">
";
    }

    // line 7
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 8
        echo "    <div class=\"row event\" data-equalizer>
        <div class=\"large-2 medium-2 small-12 columns sidebar-year\" data-equalizer-watch>
            <ul class=\"accordion year\" data-accordion=\"myAccordionGroup\">
            ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["event_list"]) ? $context["event_list"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["event_year"] => $context["event_data"]) {
            // line 12
            echo "                <li class=\"accordion-navigation content-nav\">
                    <a href=\"#panel1c";
            // line 13
            echo (isset($context["event_year"]) ? $context["event_year"] : null);
            echo "\">";
            echo (isset($context["event_year"]) ? $context["event_year"] : null);
            echo "<span class=\"year-icon\"></span></a>
                    <div id=\"panel1c";
            // line 14
            echo (isset($context["event_year"]) ? $context["event_year"] : null);
            echo "\" class=\"content ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("active") : (""));
            echo "\">
                        <ul class=\"month\">
                        ";
            // line 16
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["event_data"]) ? $context["event_data"] : null));
            foreach ($context['_seq'] as $context["event_month"] => $context["detail"]) {
                // line 17
                echo "                            <li class=\"month-detail\"><a href=\"";
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/event/";
                echo (isset($context["event_year"]) ? $context["event_year"] : null);
                echo "/";
                echo (isset($context["event_month"]) ? $context["event_month"] : null);
                echo "\">";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "month_display");
                echo "</a></li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['event_month'], $context['detail'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "                        </ul>
                    </div>
                </li>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['event_year'], $context['event_data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "            </ul>
        </div>
        <div class=\"large-9 medium-9 small-12 columns wrapper-content\" data-equalizer-watch>
        ";
        // line 26
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["event_display"]) ? $context["event_display"] : null));
        foreach ($context['_seq'] as $context["event_year"] => $context["event_data"]) {
            // line 27
            echo "            ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["event_data"]) ? $context["event_data"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
                // line 28
                echo "            <div class=\"row\" data-equalizer>
                <div class=\"large-3 medium-3 small-12 small-only-text-center columns m-displaypic\" style=\"padding-right: 0\" data-equalizer-watch>
                    <a href=\"";
                // line 30
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/event/detail/";
                echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "slug");
                echo "\" class=\"th\" style=\"border: none\">
                        <img src=\"";
                // line 31
                echo Uri::base();
                echo "media/event/";
                echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "image");
                echo "\">
                    </a>
                </div>
                <div class=\"large-9 medium-9 small-12 columns reset\" data-equalizer-watch>
                    <p class=\"title-event\">";
                // line 35
                echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "title");
                echo "</p>
                    <p class=\"date-event\">";
                // line 36
                echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "date");
                echo "</p>
                    <hr class=\"hr-event\">
                    <p class=\"detail-event\">";
                // line 38
                echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "teaser");
                echo " ...</p>
                    <div class=\"large-4 medium-5 small-12 columns m-readmore\" style=\"padding: 0; margin-top: 0.585rem\">
                        <a href=\"";
                // line 40
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/event/detail/";
                echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "slug");
                echo "\" class=\"btn-readmore\">READ MORE</a>
                    </div>
                    <div class=\"large-1 medium-2 small-2 columns shareto\" style=\"margin-top: 1.5%\">
                        Share to
                        </div>
                    <div class=\"large-7 medium-5 small-10 columns shareto\">
                        <ul>
                            <li>
                                <div class=\"social-media facebook\"></div>
                            </li>
                            <li>
                                <div class=\"social-media twitter\"></div>
                            </li>
                            <li>
                                <div class=\"social-media gplus\"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class=\"hr-divider\">
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 62
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['event_year'], $context['event_data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "            <div>
                ";
        // line 64
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "event.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 64,  199 => 63,  193 => 62,  163 => 40,  158 => 38,  153 => 36,  149 => 35,  140 => 31,  134 => 30,  130 => 28,  125 => 27,  121 => 26,  116 => 23,  99 => 19,  84 => 17,  80 => 16,  73 => 14,  67 => 13,  64 => 12,  47 => 11,  42 => 8,  39 => 7,  32 => 4,  29 => 3,);
    }
}
