<?php

/* contact_us.twig */
class __TwigTemplate_2244a1c8283812110263629ff61612607fb556dc94ba67d231fc2e1fb1da6768 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->displayBlock('frontend_content', $context, $blocks);
    }

    public function block_frontend_content($context, array $blocks = array())
    {
        // line 2
        echo "    <html>
    <body>
    <div>
        <p>Kepada Team Marketing Lotte Indonesia,</p>
        <p>Pengunjung dengan data sebagai berikut: </p>
        <p>Nama : ";
        // line 7
        echo (isset($context["contact_name"]) ? $context["contact_name"] : null);
        echo "</p>
        <p>Email : ";
        // line 8
        echo (isset($context["contact_email"]) ? $context["contact_email"] : null);
        echo "</p>
        <p>Comment: ";
        // line 9
        echo (isset($context["contact_comment"]) ? $context["contact_comment"] : null);
        echo "</p>
    </div>
    </body>
    </html>
";
    }

    public function getTemplateName()
    {
        return "contact_us.twig";
    }

    public function getDebugInfo()
    {
        return array (  41 => 9,  37 => 8,  33 => 7,  26 => 2,  20 => 1,);
    }
}
