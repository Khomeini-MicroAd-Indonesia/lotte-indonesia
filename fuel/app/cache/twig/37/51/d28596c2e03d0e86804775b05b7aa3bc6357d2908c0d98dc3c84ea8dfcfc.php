<?php

/* ./pages/views/template_frontend.twig */
class __TwigTemplate_3751d28596c2e03d0e86804775b05b7aa3bc6357d2908c0d98dc3c84ea8dfcfc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_home' => array($this, 'block_menu_home'),
            'menu_about_us' => array($this, 'block_menu_about_us'),
            'menu_product' => array($this, 'block_menu_product'),
            'menu_promo' => array($this, 'block_menu_promo'),
            'menu_careers' => array($this, 'block_menu_careers'),
            'menu_contact_us' => array($this, 'block_menu_contact_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <link rel=\"stylesheet\" href=\"assets/foundation/css/foundation.min.css\">
    <link href=\"assets/foundation/css/foundation-apps-1.1.0.css\" rel=\"stylesheet\">
    <link href=\"assets/foundation/css/foundation-apps-1.1.0.min.css\" rel=\"stylesheet\">
    <link href=\"assets/foundation/foundation-icons/foundation-icons.css\" rel=\"stylesheet\">
    <link href=\"assets/css/custom/custom.css\" rel=\"stylesheet\" type=\"text/css\">
    ";
        // line 12
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 13
        echo "    <script src=\"assets/foundation/js/vendor/modernizr.js\"></script>
    <script src=\"assets/js/modernizr.js\"></script>
</head>
<body>
<!--header-->
<div class=\"\">
    <div class=\" default-color\">
        <div class=\"row\" style=\"padding-top: 18px;padding-bottom: 18px;\">
            <ul class=\"logo\">
                <li class=\"large-6 medium-6 small-12 small-only-text-center columns\"><img src=\"assets/img/logo-lotte.png\"></li>
                <li class=\"large-3 medium-3 large-offset-1 medium-offset-1 small-12 small-only-text-center columns has-form\">
                    <div >
                        <div class=\"row collapse\">
                            <input type=\"text\" placeholder=\"Find Stuff\">
                            <a href=\"#\" class=\"alert button expand\"><img src=\"assets/img/icon/icon-search.png\"> </a>
                        </div>
                    </div>
                </li>
                <li class=\"large-2 medium-2 small-12 small-only-text-center columns\">
                    <ul class=\"lang\">
                        <li><img class=\"ind\" src=\"assets/img/icon-lang-ina.png\"></li>
                        <li><img class=\"eng\" src=\"assets/img/icon-lang-eng.png\"></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--menu-->
<div class=\"off-canvas-wrap\" data-offcanvas>
    <div class=\"inner-wrap\">
        <div class=\"\">
            <div class=\"hidden-for-small-only\">
                <div class=\"large-12 middle-12 columns menu\">
                    <div class=\"row\">
                        <ul class=\"small-block-grid-6 primary menu-bar ng-scope\">
                            <li class=\"";
        // line 49
        $this->displayBlock('menu_home', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"";
        echo Uri::base();
        echo "\"><span class=\"ci-home\" style=\"font-size: 2rem;\"></span>HOME</a></li>
                            <li class=\"";
        // line 50
        $this->displayBlock('menu_about_us', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"#\"><span class=\"ci-info\" style=\"font-size: 2rem;\"></span>ABOUT US</a></li>
                            <li class=\"";
        // line 51
        $this->displayBlock('menu_product', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"#\" class=\"button\" data-dropdown=\"autoCloseExample\" aria-controls=\"autoCloseExample\" aria-expanded=\"false\"><span class=\"ci-product\" style=\"font-size: 2rem;\"></span>PRODUCT</a>
                                <ul id=\"autoCloseExample\" class=\"f-dropdown\" data-dropdown-content tabindex=\"-1\" aria-hidden=\"true\" aria-autoclose=\"false\" tabindex=\"-1\">
                                    <li><a href=\"";
        // line 53
        echo Uri::base();
        echo "product-details\">CHOCOPIE</a></li>
                                    <li><a href=\"";
        // line 54
        echo Uri::base();
        echo "product-details\">KOALA</a></li>
                                    <li><a href=\"";
        // line 55
        echo Uri::base();
        echo "product-details\">TOPPO</a></li>
                                    <li><a href=\"";
        // line 56
        echo Uri::base();
        echo "product-details\">XYLITOL</a></li>
                                </ul>
                            </li>
                            <li class=\"";
        // line 59
        $this->displayBlock('menu_promo', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"#\" class=\"button\" data-dropdown=\"autoCloseExample2\" aria-controls=\"autoCloseExample2\" aria-expanded=\"false\"><span class=\"ci-promotion\" style=\"font-size: 2rem;\"></span>NEWS & EVENTS</a>
                                <ul id=\"autoCloseExample2\" class=\"f-dropdown\" data-dropdown-content tabindex=\"-2\" aria-hidden=\"true\" aria-autoclose=\"false\" tabindex=\"-2\">
                                    <li><a href=\"";
        // line 61
        echo Uri::base();
        echo "promo\">PROMO</a></li>
                                    <li><a href=\"";
        // line 62
        echo Uri::base();
        echo "event\">NEWS & EVENT</a></li>
                                </ul>
                            </li>
                            <li class=\"";
        // line 65
        $this->displayBlock('menu_careers', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"#\"><span class=\"ci-career\" style=\"font-size: 2rem;\"></span>CAREERS</a></li>
                            <li class=\"";
        // line 66
        $this->displayBlock('menu_contact_us', $context, $blocks);
        echo "\"><a marked=\"1\" href=\"#\"><span class=\"ci-contact\" style=\"font-size: 2rem;\"></span>CONTACT US</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class=\"show-for-small-only\" style=\"\">
                <nav class=\"tab-bar\">
                    <a class=\"left-off-canvas-toggle menu-icon\" href=\"#\">Menu<span></span></a>
                </nav>

                <aside class=\"left-off-canvas-menu\">
                    <ul class=\"off-canvas-list\">
                        <li><a href=\"#\">HOME</a></li>
                        <li><a href=\"#\">ABOUT US</a></li>
                        <li><a href=\"#\">PRODUCT</a></li>
                        <li class=\"has-submenu\"><a href=\"#\">Link 2 w/ submenu</a>
                            <ul class=\"left-submenu\">
                                <li class=\"back\"><a href=\"#\">Back</a></li>
                                <li><label>Level 2</label></li>
                                <li><a href=\"#\">...</a></li>
                            </ul>
                        </li>
                        <li><a href=\"#\">NEWS & EVENTS</a></li>
                        <li><a href=\"#\">CAREERS</a></li>
                        <li><a href=\"#\">CONTACT US</a></li>
                    </ul>
                </aside>
            </div>
        </div>
        <!--custom content-->
        ";
        // line 96
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 99
        echo "        <!--end custom content-->

        <a class=\"exit-off-canvas\"></a>
    </div>
</div>

<!--footer-->
<footer class=\"grey f-grey\">
    <div class=\"row\">
        <div class=\"large-12 columns copyright\">
            <div class=\"large-4 medium-12 small-12 columns no-padd text-center gap no-gap\">Copyright &copy; 2015 PT. Lotte Indonesia. All rights reserved.</div>
            <div class=\"large-1 medium-6 small-6 columns no-padd text-center gap\">Home</div>
            <div class=\"large-1 medium-6 small-6 columns end no-padd text-center\">Contact Us</div>
        </div>
    </div>
</footer>


    <script>
        document.write('<script src=assets/foundation/js/vendor/' +
                ('__proto__' in {} ? 'zepto' : 'jquery') +
                '.js><\\/script>')
    </script>
    <script src=\"assets/foundation/js/vendor/jquery.js\"></script>
    <script src=\"assets/foundation/js/foundation.min.js\"></script>
    <script src=\"assets/foundation/js/foundation/foundation.dropdown.js\"></script>
    <script src=\"assets/foundation/js/foundation/foundation.orbit.js\"></script>
    <script>
        \$(document).foundation();
        \$(\"label\").on(\"click\", function(){
            \$(this).siblings(\".sub-menu\").slideToggle();
        });
    </script>
";
        // line 132
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 133
        echo "</body>
</html>";
    }

    // line 12
    public function block_frontend_css($context, array $blocks = array())
    {
        echo " ";
    }

    // line 49
    public function block_menu_home($context, array $blocks = array())
    {
    }

    // line 50
    public function block_menu_about_us($context, array $blocks = array())
    {
    }

    // line 51
    public function block_menu_product($context, array $blocks = array())
    {
    }

    // line 59
    public function block_menu_promo($context, array $blocks = array())
    {
    }

    // line 65
    public function block_menu_careers($context, array $blocks = array())
    {
    }

    // line 66
    public function block_menu_contact_us($context, array $blocks = array())
    {
    }

    // line 96
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 97
        echo "
        ";
    }

    // line 132
    public function block_frontend_js($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "./pages/views/template_frontend.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 132,  249 => 97,  246 => 96,  241 => 66,  236 => 65,  231 => 59,  226 => 51,  221 => 50,  216 => 49,  210 => 12,  205 => 133,  203 => 132,  168 => 99,  166 => 96,  133 => 66,  129 => 65,  123 => 62,  119 => 61,  114 => 59,  108 => 56,  104 => 55,  100 => 54,  96 => 53,  91 => 51,  87 => 50,  81 => 49,  43 => 13,  41 => 12,  28 => 1,  187 => 143,  184 => 142,  50 => 10,  47 => 9,  42 => 7,  39 => 6,  34 => 4,  31 => 3,);
    }
}
