<?php

/* career.twig */
class __TwigTemplate_516d3637351e64f78f88c9a5843a325a96ce80869fc74f194172c2a813d66f9f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_careers' => array($this, 'block_menu_careers'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/css/custom/career.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_menu_careers($context, array $blocks = array())
    {
        // line 7
        echo "    is-active
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"row banner\">
        <div class=\"large-12 columns\">
            ";
        // line 12
        echo $this->getAttribute((isset($context["data_career_banner"]) ? $context["data_career_banner"] : null), "content");
        echo "
        </div>
    </div>
    ";
        // line 15
        if ((twig_length_filter($this->env, (isset($context["career_detail"]) ? $context["career_detail"] : null)) < 1)) {
            // line 16
            echo "        <div class=\"row no-career\">
            <div class=\"large-6 large-centered columns no-career-desc\">
                <div class=\"large-2 columns\">
                    <img src=\"";
            // line 19
            echo Uri::base();
            echo "assets/img/icon/icon-no-job-vacancy.jpg\">
                </div>
                <div class=\"large-10 columns\">
                    <div>
                        <p><b>No job vacancy</b> is available now</p>
                        <p>Please come back any soon or contact us on <a href=\"mailto:recruitment@lotte.co.id\"> recruitment@lotte.co.id</a></p>
                    </div>
                </div>
            </div>
        </div>
    ";
        } else {
            // line 30
            echo "        <div class=\"row career\">
            <div class=\"pre-title-center first-child\">
                <p class=\"title\">CAREER</p>
                <div style=\"clear: both\"></div>
            </div>
            <div class=\"large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns\">
                ";
            // line 36
            echo (isset($context["page_content"]) ? $context["page_content"] : null);
            echo "
            </div>

            <div class=\"pre-title-center\">
                <p class=\"title\">JOB VACANCIES</p>
                <div style=\"clear: both\"></div>
            </div>
            <ol class=\"accordion\" data-accordion role=\"tablist\">
                ";
            // line 44
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["career_detail"]) ? $context["career_detail"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["career"]) {
                // line 45
                echo "                    <li class=\"large-10 large-offset-1 medium-10 medium-offset-1 small-12 ";
                echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) ? ("end") : (""));
                echo " columns accordion-navigation\">
                        <a href=\"#panel-career-";
                // line 46
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
                echo "\" class=\"career-title\" role=\"tab\" id=\"panel-career-heading-";
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
                echo "\" aria-controls=\"panel-career-";
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
                echo "\">";
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "name");
                echo "<span class=\"career-view\" style=\"\">See Requirements<span class=\"year-icon\"></span></span></a>
                        <div id=\"panel-career-";
                // line 47
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
                echo "\" class=\"content ";
                echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("active") : (""));
                echo "\" role=\"tabpanel\" aria-labelledby=\"panel-career-heading-";
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "id");
                echo "\">
                            <div class=\"slide-down\">
                                ";
                // line 49
                echo $this->getAttribute((isset($context["career"]) ? $context["career"] : null), "req");
                echo "
                            </div>
                        </div>
                    </li>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "            </ol>

            <div class=\"large-10 large-centered medium-10 medium-centered small-12 columns\">
                ";
            // line 57
            echo $this->getAttribute((isset($context["data_career_howtoapplay"]) ? $context["data_career_howtoapplay"] : null), "content");
            echo "
            </div>
        </div>
    ";
        }
        // line 61
        echo "
    ";
        // line 62
        $this->displayBlock('back_to_top', $context, $blocks);
    }

    public function block_back_to_top($context, array $blocks = array())
    {
        // line 63
        echo "        ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
    ";
    }

    // line 67
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 68
        echo "    <script src=\"";
        echo Uri::base();
        echo "assets/foundation/js/foundation/foundation.accordion.js\"></script>
    <script>
        \$(document).ready(function(){
        });
        \$(\".accordion li\").on(\"click\", \"a:eq(0)\", function (event)
        {
            var li_parent = \$(this).parent();
            if(li_parent.hasClass('active'))
                \$(\".accordion li div.content:visible\").slideToggle(\"normal\");
            else
            {
                \$(\".accordion li div.content:visible\").slideToggle(\"normal\");
                \$(this).parent().find(\".content\").slideToggle(\"normal\");
            }
        });
        \$(document).foundation({

        });
        ";
        // line 86
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 89
        echo "    </script>
";
    }

    // line 86
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 87
        echo "            ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "career.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  227 => 87,  224 => 86,  219 => 89,  217 => 86,  195 => 68,  192 => 67,  185 => 63,  179 => 62,  176 => 61,  169 => 57,  164 => 54,  145 => 49,  136 => 47,  126 => 46,  121 => 45,  104 => 44,  93 => 36,  85 => 30,  71 => 19,  66 => 16,  64 => 15,  58 => 12,  54 => 10,  51 => 9,  46 => 7,  43 => 6,  36 => 4,  33 => 3,);
    }
}
