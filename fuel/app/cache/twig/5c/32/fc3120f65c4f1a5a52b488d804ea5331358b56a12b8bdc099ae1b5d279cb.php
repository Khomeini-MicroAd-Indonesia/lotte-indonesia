<?php

/* promo_details.twig */
class __TwigTemplate_5c32fc3120f65c4f1a5a52b488d804ea5331358b56a12b8bdc099ae1b5d279cb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_promo' => array($this, 'block_menu_promo'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"assets/css/custom/promo-details.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_menu_promo($context, array $blocks = array())
    {
        // line 7
        echo "    is-active
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"row promo-details\">
        <div class=\"large-2 medium-2 small-2 columns left-promo\">
            <ul>
                <li><a href=\"\" class=\"side-menu is-active\"><span>Matcha Green Tea Lotte Photo Competition</span></a></li>
                <li><a href=\"\" class=\"side-menu\"><span>Lorem Ipsum Sit Dolor Amet (Max 45 Character)</span></a> </li>
                <li><a href=\"\" class=\"side-menu\"><span>Lorem Ipsum Sit Dolor Amet (Max 45 Character)</span></a> </li>
                <li><a href=\"\" class=\"side-menu\"><span>Lorem Ipsum Sit Dolor Amet (Max 45 Character)</span></a> </li>
                <li><a href=\"\" class=\"side-menu\"><span>Lorem Ipsum Sit Dolor Amet (Max 45 Character)</span></a> </li>
                <li><a href=\"\" class=\"side-menu\"><span>Lorem Ipsum Sit Dolor Amet (Max 45 Character)</span></a> </li>
                <li><a href=\"\" class=\"side-menu\"><span>Lorem Ipsum Sit Dolor Amet (Max 45 Character)</span></a> </li>
                <li><a href=\"\" class=\"side-menu\"><span>Lorem Ipsum Sit Dolor Amet (Max 45 Character)</span></a> </li>
            </ul>
        </div>
        <div class=\"large-10 medium-10 small-10 columns right-promo\">
            <div class=\"large-11 large-offset-1 medium-11 medium-offset-1 small-11 small-offset-1 columns\">
                <div class=\"row banner-section\">
                    <div class=\"large-10 medium-10 small-10 columns\">
                        <img src=\"assets/img/promo-details/banner/banner-matcha-green-latte.jpg\">
                    </div>
                    <div class=\"large-2 medium-2 small-2 columns share-to\">
                        Share to :
                        <ul>
                            <li><img class=\"social-media facebook\"> </li>
                            <li><img class=\"social-media twitter\"> </li>
                            <li><img class=\"social-media gplus\"> </li>
                        </ul>
                    </div>
                </div>
                <div class=\"header-promo-section\">
                    <h1 class=\"title\">Matcha Green Tea LOTTE photo competition</h1>
                    <h3 class=\"info-periode\">Periode : 1 - 31 December 2015</h3>
                    <div class=\"pre-tag-promo\">
                        <p class=\"tag-promo\"><span><img src=\"assets/img/icon/promo.png\"></span>Promo</p>
                        <div style=\"clear: both\"></div>
                    </div>
                </div>
                <div class=\"promo-desc-section\">
                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">DESCRIPTION</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    <p>Lorem ipsum dolor sit amet, autem clita accusamus te quo, eos facete salutatus et. Ius an stet quot omittantur, mel id tota brute contentiones. At putent admodum eum, no his nisl temporibus, sea cu delectus vivendum. Ei vis idque soleat singulis, dicat veritus pertinacia ex mei. Numquam propriae mei te, aperiri ocurreret te eos.</p>
                    <p>Habeo mucius gubergren ne eum, graece epicuri ocurreret usu ut, ei homero dolore populo ius. Usu illud mazim dolorum ut, nec dicat praesent id. Oratio lucilius nec ad. Soluta molestie sea ex, summo admodum sed te. Vitae putant signiferumque sit ex, ius id tollit tritani denique.</p>
                </div>
                <div class=\"promo-desc-section\">
                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">SYARAT DAN KETENTUAN</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    <p class=\"custom-desc\">SYARAT PEMAIN</p>
                    <ol>
                        <li>Lorem ipsum dolor sit amet, autem clita accusamus te quo, eos facete salutatus et. Ius an stet quot omittantur, mel id tota </li>
                        <li>Brute contentiones. At putent admodum eum, no his nisl temporibus, sea cu delectus vivendum. Ei vis idque soleat singulis, </li>
                        <li>Dicat veritus pertinacia ex mei. Numquam propriae mei te, aperiri ocurreret te eos.</li>
                        <li>Habeo mucius gubergren ne eum, graece epicuri ocurreret usu ut, ei homero dolore populo ius. Usu illud mazim dolorum ut,</li>
                        <li>Nec dicat praesent id. Oratio lucilius nec ad. Soluta molestie sea ex, summo admodum sed te. Vitae putant signiferumque sit </li>
                        <li>Ex, ius id tollit tritani denique.</li>
                    </ol>
                    <p class=\"custom-desc\">SYARAT PEMAIN</p>
                    <ol>
                        <li>Lorem ipsum dolor sit amet, autem clita accusamus te quo, eos facete salutatus et. Ius an stet quot omittantur, mel id tota </li>
                        <li>Brute contentiones. At putent admodum eum, no his nisl temporibus, sea cu delectus vivendum. Ei vis idque soleat singulis, </li>
                        <li>Dicat veritus pertinacia ex mei. Numquam propriae mei te, aperiri ocurreret te eos.</li>
                        <li>Habeo mucius gubergren ne eum, graece epicuri ocurreret usu ut, ei homero dolore populo ius. Usu illud mazim dolorum ut,</li>
                        <li>Nec dicat praesent id. Oratio lucilius nec ad. Soluta molestie sea ex, summo admodum sed te. Vitae putant signiferumque sit </li>
                        <li>Ex, ius id tollit tritani denique.</li>
                    </ol>
                </div>
                <div class=\"promo-desc-section\">
                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">PRIZE</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    <ul class=\"large-block-grid-2 medium-block-grid-2 small-block-grid-2 prize-section\">
                        <li>
                            <img src=\"assets/img/promo-details/promo-prize/mobil.jpg\">
                            <p>2 unit mobil Suzuki Ritz</p>
                        </li>
                        <li>
                            <img src=\"assets/img/promo-details/promo-prize/goody-bag.jpg\">
                            <p>20 merchandise LOTTE</p>
                        </li>
                    </ul>
                </div>
                <div class=\"promo-desc-section\">
                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">MECHANISM</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    <ul class=\"large-block-grid-2 medium-block-grid-2 small-block-grid-2 mechanism-section\">
                        <li>
                            <div>
                                <p class=\"text-left step\">1</p>
                                <img src=\"assets/img/promo-details/promo-mechanism/step-1.jpg\">
                                <p class=\"step-desc\">LIKE Facebook page kami <br/>facebook/MatchaGreenTeaLoatte</p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p class=\"text-left step\">2</p>
                                <img src=\"assets/img/promo-details/promo-mechanism/step-2.jpg\">
                                <p class=\"step-desc\">Upload foto cara seru kamu <br/>menikmati Matcha Green Tea Loatte</p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p class=\"text-left step\">3</p>
                                <img src=\"assets/img/promo-details/promo-mechanism/step-3.jpg\">
                                <p class=\"step-desc\">Jangan lupa sertakan hashtag <br/>#matchaloatteseru</p>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p class=\"text-left step\">4</p>
                                <img src=\"assets/img/promo-details/promo-mechanism/step-4.jpg\">
                                <p class=\"step-desc\">Lihat foto kamu & peserta lainnya</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class=\"promo-desc-section\">
                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">INFO LEBIH LANJUT</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    <img src=\"assets/img/promo-details/others/matcha.jpg\">
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 142
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 143
        echo "    <script type=\"text/javascript\">

    </script>
";
    }

    public function getTemplateName()
    {
        return "promo_details.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 143,  184 => 142,  50 => 10,  47 => 9,  42 => 7,  39 => 6,  34 => 4,  31 => 3,);
    }
}
