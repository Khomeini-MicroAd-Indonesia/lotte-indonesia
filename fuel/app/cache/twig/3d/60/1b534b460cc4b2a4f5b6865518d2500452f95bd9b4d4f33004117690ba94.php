<?php

/* about_us.twig */
class __TwigTemplate_3d601b534b460cc4b2a4f5b6865518d2500452f95bd9b4d4f33004117690ba94 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_about_us' => array($this, 'block_menu_about_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 5
        echo Uri::base();
        echo "assets/css/custom/about-us.css\" rel=\"stylesheet\">
";
    }

    // line 7
    public function block_menu_about_us($context, array $blocks = array())
    {
        // line 8
        echo "    is-active
";
    }

    // line 10
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"row about-us\">
        <div class=\"large-6 medium-12 small-12 columns greeting\">
            <img src=\"";
        // line 13
        echo Uri::base();
        echo "assets/img/about-us/greetings.jpg\">
            <p class=\"greeting-img\">Werther and Lotte from<br/>
                The Sorrows of Young Werther novel</p>
        </div>
        <div class=\"large-6 medium-12 small-12 columns greeting-detail\">
            <div class=\"inner-greeting\">
                <div class=\"pre-title\">
                    <p class=\"title\">";
        // line 20
        echo Lang::get("txt_title_greetings");
        echo "</p>
                    <div style=\"clear: both\"></div>
                </div>
                <p>";
        // line 23
        echo Lang::get("txt_greetings01");
        echo "</p>
                <p>";
        // line 24
        echo Lang::get("txt_greetings02");
        echo "</p>
                <p>";
        // line 25
        echo Lang::get("txt_greetings03");
        echo "</p>
                <p>";
        // line 26
        echo Lang::get("txt_greetings04");
        echo "</p>
                <p>";
        // line 27
        echo Lang::get("txt_greetings05");
        echo "</p>
            </div>

        </div>
    </div>
    <div class=\"bg-core-values\">
        <div class=\"row vision-section\">
            <div class=\"large-12 medium-12 small-12 columns\">
                <div class=\"pre-title visions\">
                    <p class=\"title\">";
        // line 36
        echo Lang::get("txt_vision");
        echo "</p>
                    <div style=\"clear: both\"></div>
                </div>
                <div class=\"row\">
                    <div class=\"large-4 medium-12 small-12  columns end\">

                        <p>";
        // line 42
        echo Lang::get("txt_vision2020");
        echo "</p>

                    </div>
                </div>
                <div class=\"row core-values\">
                    <div class=\"large-10 large-offset-1 medium-10 medium-offset-1 small-12 columns end\">
                        <div class=\"pre-title\">
                            <p class=\"title\">";
        // line 49
        echo Lang::get("txt_core_value");
        echo "</p>
                            <div style=\"clear: both\"></div>
                        </div>
                        <ul class=\"large-6 medium-6 small-12 columns\">
                            <li class=\"large-6 medium-6 small-12 columns\">";
        // line 53
        echo Lang::get("txt_core01");
        echo "</li>
                            <li class=\"large-6 medium-6 small-12 columns\">";
        // line 54
        echo Lang::get("txt_core02");
        echo "</li>
                            <li class=\"large-6 medium-6 small-12 columns\">";
        // line 55
        echo Lang::get("txt_core03");
        echo "</li>
                            <li class=\"large-6 medium-6 small-12 columns\">";
        // line 56
        echo Lang::get("txt_core04");
        echo "</li>
                            <li class=\"large-6 medium-6 small-12 columns end\">";
        // line 57
        echo Lang::get("txt_core05");
        echo "</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class=\"img-vision-scale\">
            </div>
        </div>

    </div>
    <div class=\"row definition\" data-equalizer>
        <div class=\"pre-title-center\">
            <p class=\"title\">";
        // line 70
        echo Lang::get("txt_core_value_definition");
        echo "</p>
            <div style=\"clear: both\"></div>
        </div>
        <div class=\"row core-values\">
            <div class=\"large-4 medium-6 small-12 columns text-center bottom-gap\">
                <div class=\"values-list\" data-equalizer-watch>
                    <div class=\"\" style=\"background-color: #ffffff\">
                        <img src=\"";
        // line 77
        echo Uri::base();
        echo "assets/img/about-us/icon/passions.png\">
                        <p class=\"title-core\">";
        // line 78
        echo Lang::get("txt_core01");
        echo "</p>
                    </div>
                    <p style=\"text-align: left;background-color: #f4f4f4;padding: 20px\">";
        // line 80
        echo Lang::get("txt_value01");
        echo "</p>
                </div>
            </div>
            <div class=\"large-4 medium-6 small-12 columns text-center bottom-gap\">
                <div class=\"values-list\" data-equalizer-watch>
                    <div class=\"\" style=\"background-color: #ffffff\">
                        <img src=\"";
        // line 86
        echo Uri::base();
        echo "assets/img/about-us/icon/action-oriented.png\">
                        <p class=\"title-core\">";
        // line 87
        echo Lang::get("txt_core03");
        echo "</p>
                    </div>
                    <p style=\"text-align: left;background-color: #f4f4f4;padding: 20px\">";
        // line 89
        echo Lang::get("txt_value02");
        echo "</p>
                </div>
            </div>
            <div class=\"large-4 medium-6 small-12 columns text-center bottom-gap\">
                <div class=\"values-list\" data-equalizer-watch>
                    <div class=\"\" style=\"background-color: #ffffff\">
                        <img src=\"";
        // line 95
        echo Uri::base();
        echo "assets/img/about-us/icon/responsibility.png\">
                        <p class=\"title-core\">";
        // line 96
        echo Lang::get("txt_core05");
        echo "</p>
                    </div>
                    <p style=\"text-align: left;background-color: #f4f4f4;padding: 20px\">";
        // line 98
        echo Lang::get("txt_value03");
        echo "</p>
                </div>
            </div>
            <div class=\"large-4 large-offset-2 medium-6 medium-offset-2 small-12 columns text-center bottom-gap\">
                <div class=\"values-list\" data-equalizer-watch>
                    <div class=\"\" style=\"background-color: #ffffff\">
                        <img src=\"";
        // line 104
        echo Uri::base();
        echo "assets/img/about-us/icon/teamwork.png\">
                        <p class=\"title-core\">";
        // line 105
        echo Lang::get("txt_core02");
        echo "</p>
                    </div>
                    <p style=\"text-align: left;background-color: #f4f4f4;padding: 20px\">";
        // line 107
        echo Lang::get("txt_value04");
        echo "</p>
                </div>
            </div>
            <div class=\"large-4 medium-6 small-12 columns end text-center bottom-gap\">
                <div class=\"values-list\" data-equalizer-watch>
                    <div class=\"\" style=\"background-color: #ffffff\">
                        <img src=\"";
        // line 113
        echo Uri::base();
        echo "assets/img/about-us/icon/trust.png\">
                        <p class=\"title-core\">";
        // line 114
        echo Lang::get("txt_core04");
        echo "</p>
                    </div>
                    <p style=\"text-align: left;background-color: #f4f4f4;padding: 20px\">";
        // line 116
        echo Lang::get("txt_value05");
        echo "</p>
                </div>
            </div>
        </div>

    </div>
    <div class=\"bg-core-values \">
        <div class=\"row expansion\">
            <div class=\"pre-title-center\">
                <p class=\"title\">";
        // line 125
        echo Lang::get("txt_expansion");
        echo "</p>
                <div style=\"clear: both\"></div>
            </div>
            <div class=\"large-6 columns\">
                <ul class=\"large-block-grid-2 medium-block-grid-2 small-block-grid-1 expansion-list\" data-equalizer>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Guylian</p>
                            <p>Established: 1938</p>
                            <p>Manuf. Premium Chocolate</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte Vietnam Co.,</p>
                            <p>Established: 1996</p>
                            <p>Manuf. & Selling Gum</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte Confectionery Pilipinas</p>
                            <p>Established: 2009</p>
                            <p>Rep. & Sales Office</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte USA Inc.</p>
                            <p>Established: 1978</p>
                            <p>Manuf. Gum Base & Sales</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte India</p>
                            <p>Established: 2005</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte Singapore Pte. Ltd</p>
                            <p>Established: 2010</p>
                            <p>Rep. & Sales Office</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Thai Lotte Co., Ltd.</p>
                            <p>Established: 1989</p>
                            <p>Manuf. & Selling Gum, Koala</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte Taiwan Co.,</p>
                            <p>Established: 2005</p>
                            <p>Rep. & Sales Office</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte Wedel Poland</p>
                            <p>Established: 2010</p>
                            <p>Manuf. Premium Chocolate</p>
                        </div>
                    </li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">PT Lotte Indonesia</p>
                            <p>Established: 1993</p>
                            <p>Manuf. & Selling Gum, Candy</p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class=\"large-6 columns\">
                <ul class=\"large-block-grid-2 expansion-list\" data-equalizer>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte KF RUS</p>
                            <p>Established: 2007</p>
                        </div>
                    </li>
                    <li></li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte China Investment</p>
                            <p>Established; 1994</p>
                            <p>Manuf. & Selling Gum, Choco, Biscuit</p>
                        </div>
                    </li>
                    <li></li>
                    <li>
                        <div data-equalizer-watch>
                            <p class=\"title-list\">Lotte Malaysia Sdn.Bhd.</p>
                            <p>Established: 2007</p>
                            <p>Rep. & Sales Office</p>
                        </div>
                    </li>
                </ul>
                <img src=\"";
        // line 226
        echo Uri::base();
        echo "assets/img/about-us/expansion.png\" class=\"expansion-img\">
            </div>
        </div>
    </div>
    ";
        // line 230
        if ((twig_length_filter($this->env, (isset($context["achievement_list"]) ? $context["achievement_list"] : null)) > 0)) {
            // line 231
            echo "    <div class=\"row achievement\">
        <div class=\"brand-slider\">
            <div class=\"large-10 medium-10 large-centered medium-centered columns\">
                <ul class=\"brand-slider-list\">
                    ";
            // line 235
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["achievement_list"]) ? $context["achievement_list"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["achievement"]) {
                // line 236
                echo "                        <li><img src=\"";
                echo ((Uri::base() . $this->getAttribute((isset($context["achievement"]) ? $context["achievement"] : null), "get_image_path", array(), "method")) . $this->getAttribute((isset($context["achievement"]) ? $context["achievement"] : null), "filename"));
                echo "\" title=\"";
                echo $this->getAttribute((isset($context["achievement"]) ? $context["achievement"] : null), "title");
                echo "\"></li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['achievement'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 238
            echo "                </ul>
            </div>
        </div>
    </div>
    ";
        }
        // line 243
        echo "    <div class=\"row history\">
        <div class=\"pre-title-center\">
            <p class=\"title\">";
        // line 245
        echo Lang::get("txt_history_title");
        echo "</p>
            <div style=\"clear: both\"></div>
        </div>
        <p class=\"text-center\">";
        // line 248
        echo Lang::get("txt_history");
        echo "</p>
        <div class=\"row text-center\">
            <img src=\"";
        // line 250
        echo Uri::base();
        echo "assets/img/about-us/icon/lotte.png\" class=\"logo-top-history\">
        </div>
        <div class=\"row history-timeline\">
            <ol id=\"timeline\" class=\"large-12 large-centered columns\">
                ";
        // line 254
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["history_list"]) ? $context["history_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["history_item"]) {
            if (((isset($context["current_lang"]) ? $context["current_lang"] : null) == "en")) {
                // line 255
                echo "                <li class=\"large-5 medium-5 small-12 columns\">
                    <div>
                        <time>";
                // line 257
                echo $this->getAttribute((isset($context["history_item"]) ? $context["history_item"] : null), "year");
                echo "</time>
                        ";
                // line 258
                echo $this->getAttribute((isset($context["history_item"]) ? $context["history_item"] : null), "description");
                echo "
                    </div>
                </li>
                ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['history_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 262
        echo "                ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["history_list"]) ? $context["history_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["history_item"]) {
            if (((isset($context["current_lang"]) ? $context["current_lang"] : null) == "id")) {
                // line 263
                echo "                <li class=\"large-5 medium-5 small-12 columns\">
                    <div>
                        ";
                // line 265
                if (($this->getAttribute((isset($context["history_item"]) ? $context["history_item"] : null), "descriptionid") == "")) {
                    // line 266
                    echo "                            <time>";
                    echo $this->getAttribute((isset($context["history_item"]) ? $context["history_item"] : null), "year");
                    echo "</time>
                            ";
                    // line 267
                    echo $this->getAttribute((isset($context["history_item"]) ? $context["history_item"] : null), "description");
                    echo "
                        ";
                }
                // line 269
                echo "                        ";
                if (($this->getAttribute((isset($context["history_item"]) ? $context["history_item"] : null), "descriptionid") != "")) {
                    // line 270
                    echo "                            <time>";
                    echo $this->getAttribute((isset($context["history_item"]) ? $context["history_item"] : null), "year");
                    echo "</time>
                            ";
                    // line 271
                    echo $this->getAttribute((isset($context["history_item"]) ? $context["history_item"] : null), "descriptionid");
                    echo "
                        ";
                }
                // line 273
                echo "                    </div>
                </li>
                ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['history_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 276
        echo "            </ol>
        </div>
        <div class=\"row text-center\">
            <img src=\"";
        // line 279
        echo Uri::base();
        echo "assets/img/about-us/icon/lotte.png\">
            <p>";
        // line 280
        echo Lang::get("txt_present");
        echo "</p>
        </div>
    </div>
";
    }

    // line 285
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 286
        echo "    <script src=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.js\"></script>
    <script type=\"text/javascript\">
        \$(document).foundation({
            equalizer : {
                // Specify if Equalizer should make elements equal height once they become stacked.
                equalize_on_stack: true
            }
        });

        \$(document).ready(function(){
            \$('.brand-slider-list').bxSlider({
                minSlides: 2,
                maxSlides: 5,
                slideWidth: 170,
                slideMargin: 10,
                infiniteLoop : false,
                hideControlOnEnd : true
            });
            \$( \"ul.history-list > li:nth-child(odd)\" ).css( \"float\", \"right\" );
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "about_us.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  516 => 286,  513 => 285,  505 => 280,  501 => 279,  496 => 276,  487 => 273,  482 => 271,  477 => 270,  474 => 269,  469 => 267,  464 => 266,  462 => 265,  458 => 263,  452 => 262,  441 => 258,  437 => 257,  433 => 255,  428 => 254,  421 => 250,  416 => 248,  410 => 245,  406 => 243,  399 => 238,  388 => 236,  384 => 235,  378 => 231,  376 => 230,  369 => 226,  265 => 125,  253 => 116,  248 => 114,  244 => 113,  235 => 107,  230 => 105,  226 => 104,  217 => 98,  212 => 96,  208 => 95,  199 => 89,  194 => 87,  190 => 86,  181 => 80,  176 => 78,  172 => 77,  162 => 70,  146 => 57,  142 => 56,  138 => 55,  134 => 54,  130 => 53,  123 => 49,  113 => 42,  104 => 36,  92 => 27,  88 => 26,  84 => 25,  80 => 24,  76 => 23,  70 => 20,  60 => 13,  56 => 11,  53 => 10,  48 => 8,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
