<?php

/* home.twig */
class __TwigTemplate_7f3388e416cb0b24a05681659fee5bb6ea37af4fa5af512980d8780f49ab9113 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_home' => array($this, 'block_menu_home'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 5
        echo Uri::base();
        echo "assets/css/custom/animate.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 6
        echo Uri::base();
        echo "assets/css/custom/home.css\" rel=\"stylesheet\">
";
    }

    // line 8
    public function block_menu_home($context, array $blocks = array())
    {
        // line 9
        echo "    is-active
";
    }

    // line 11
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 12
        echo "<div class=\"slideshow-wrapper\">
    <ul class=\"example-orbit-content\" data-orbit>
        ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["home_banner"]) ? $context["home_banner"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 15
            echo "            <li data-orbit-slide=\"headline-";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "id");
            echo "\" data-equalizer=\"\">
                <div class=\"large-7 large-push-5 medium-12 small-12 columns\" style=\"position : relative ;background-color: #";
            // line 16
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "bgcolor");
            echo "\" data-equalizer-watch=\"\">
                    <img src=\"";
            // line 17
            echo Uri::base();
            echo "media/homebanner/";
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "image");
            echo "\" class=\"banner-img\">
                </div>
                <div class=\"large-5 large-pull-7 medium-12 small-12 columns\" style=\"background-color: #";
            // line 19
            echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "bgcolor");
            echo "\" data-equalizer-watch=\"\">
                    ";
            // line 20
            if (($this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "slug") == "lotte-other")) {
                // line 21
                echo "                        
                    <div class=\"large-7  small-12 columns m-desc\" style=\"padding: 38px 0px;\">
                        <a href=\"";
                // line 23
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/product-others\">
                            <div class=\"banner-desc\" style=\"color: #";
                // line 24
                echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "fontcolor");
                echo "; padding-right: 35px\">
                                <img src=\"";
                // line 25
                echo Uri::base();
                echo "media/homebanner/";
                echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "logo");
                echo "\" alt=\"banner logo\" class=\"right banner-title\" style=\"padding-bottom: 10px\">
                                <hr style=\" border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));\">
                                <p class=\"text-right description\" style=\"clear: both; font-family: muliregular; font-size: 12px;\">";
                // line 27
                echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "content");
                echo "</p>
                            </div>
                        </a>
                    </div>
                        ";
                // line 31
                if ((!$this->getAttribute((isset($context["user"]) ? $context["user"] : null), "subscribed"))) {
                    // line 32
                    echo "                            <a href=\"";
                    echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                    echo "/product-others\" class=\"button right\" style=\"color: #";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "fontcolor");
                    echo ";background-color: #";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "bgcolor");
                    echo "; border: solid 1px #FFFFFF;box-shadow: 0px 5px 1px rgba(0, 0, 0, 0.3);padding: 0.85em 3em;letter-spacing: 5px;border-radius: 5px;font-family: muliregular; font-size: 10px;position: absolute;right: 0px;bottom: 36px;\">SEE MORE</a>
                        ";
                }
                // line 33
                echo "       
                    ";
            }
            // line 35
            echo "                    ";
            if (($this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "slug") != "lotte-other")) {
                // line 36
                echo "                        
                    <div class=\"large-7  small-12 columns m-desc\" style=\"padding: 38px 0px;\">
                        <a href=\"";
                // line 38
                echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                echo "/product-details/";
                echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "slug");
                echo "\">
                            <div class=\"banner-desc\" style=\"color: #";
                // line 39
                echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "fontcolor");
                echo "; padding-right: 35px\">
                                <img src=\"";
                // line 40
                echo Uri::base();
                echo "media/homebanner/";
                echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "logo");
                echo "\" alt=\"banner logo\" class=\"right banner-title\" style=\"padding-bottom: 10px\">
                                <hr style=\" border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));\">
                                <p class=\"text-right description\" style=\"clear: both; font-family: muliregular; font-size: 12px;\">";
                // line 42
                echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "content");
                echo "</p>
                            </div>
                        </a>
                    </div>
                        ";
                // line 46
                if ((!$this->getAttribute((isset($context["user"]) ? $context["user"] : null), "subscribed"))) {
                    // line 47
                    echo "                            <a href=\"";
                    echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
                    echo "/product-details/";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "slug");
                    echo "\" class=\"button right\" style=\"color: #";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "fontcolor");
                    echo ";background-color: #";
                    echo $this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "bgcolor");
                    echo "; border: solid 1px #FFFFFF;box-shadow: 0px 5px 1px rgba(0, 0, 0, 0.3);padding: 0.85em 3em;letter-spacing: 5px;border-radius: 5px;font-family: muliregular; font-size: 10px;position: absolute;right: 0px;bottom: 36px;\">SEE MORE</a>
                        ";
                }
                // line 49
                echo "                    ";
            }
            // line 50
            echo "                    
                </div>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "    </ul>
</div>
        <div class=\"brand-slider\">
            <div class=\"large-10 medium-10 small-10 large-centered medium-centered small-centered columns\">
            <ul class=\"brand-slider-list\">
            ";
        // line 59
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["home_product"]) ? $context["home_product"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["brand"]) {
            // line 60
            echo "                <li><a href=\"";
            echo $this->getAttribute((isset($context["brand"]) ? $context["brand"] : null), "link");
            echo "\"><img src=\"";
            echo Uri::base();
            echo "media/product/";
            echo $this->getAttribute((isset($context["brand"]) ? $context["brand"] : null), "image");
            echo "\"></a></li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['brand'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 61
        echo "    
            </ul>
            </div>
        </div>
        ";
        // line 65
        if ((twig_length_filter($this->env, Config::get("config_basic.latest_tvc")) > 0)) {
            // line 66
            echo "        <div class=\"row section text-center tvc\">
            <div class=\"large-10 columns large-centered\">
                <h3 class=\"title white default-color\">LATEST TVC</h3>
                <div class=\"youtube youtube-color\">
                    <div class=\"video-container\">
                        <iframe width=\"640\" height=\"360\" src=\"";
            // line 71
            echo Config::get("config_basic.latest_tvc");
            echo "\" frameborder=\"0\" allowfullscreen></iframe>
                    </div>
                </div>
                <div class=\"youtube-color visit-desc\">
                    <p><span class=\"white\">VISIT OUR</span> <span class=\"red\">YOUTUBE CHANNEL</span></p>
                </div>
            </div>
        </div>
        ";
        }
        // line 80
        echo "        <div class=\"row  event-promo\">
            <div class=\"pre-title\">
                <p class=\"title\">PROMO & EVENT</p>
                <div style=\"clear: both\"></div>
            </div>
            <div class=\"large-7 columns text-center left-event\" style=\"\" data-equalizer>
            ";
        // line 86
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, (isset($context["promo_event_preview"]) ? $context["promo_event_preview"] : null), 0, 1));
        foreach ($context['_seq'] as $context["_key"] => $context["preview_item"]) {
            // line 87
            echo "                <a href=\"";
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo "/detail/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "slug");
            echo "\">
                    <div class=\"large-6 middle-6 small-12 columns variant-display .m-variant-display\" style=\"padding-left: 0px;background-color: #f1f1f1\" data-equalizer-watch=\"\">
                        <img src=\"";
            // line 89
            echo Uri::base();
            echo "media/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo "/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "image");
            echo "\" style=\"width: 335px; height: 335px\">
                    </div>
                    <div class=\"large-6 middle-6  small-12 columns\" style=\"background-color: #ebebeb\" data-equalizer-watch=\"\">
                        <div class=\"\" style=\"padding: 50px 40px 40px\">
                            <a href=\"";
            // line 93
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo "/detail/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "slug");
            echo "\" class=\"link-desc\">
                                <img src=\"";
            // line 94
            echo Uri::base();
            echo "assets/img/icon/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo ".png\">
                                <h3 style=\"text-transform: uppercase;font-weight:600;font-size: 1.125rem;margin-top: 0.7875rem;color:#505050;font-family: 'Helvetica 65 Medium','Myriad Pro'\">";
            // line 95
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "title");
            echo "</h3>
                                <p class=\"description\">";
            // line 96
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "teaser");
            echo "</p>
                                <a href=\"";
            // line 97
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo "/detail/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "slug");
            echo "\" class=\"link-desc\">SEE MORE...</a>
                            </a>
                        </div>
                    </div>
                </a>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['preview_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "            </div>
            <div class=\"large-5 columns end right-event\">
            ";
        // line 105
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, (isset($context["promo_event_preview"]) ? $context["promo_event_preview"] : null), 1, twig_length_filter($this->env, (isset($context["promo_event_preview"]) ? $context["promo_event_preview"] : null))));
        foreach ($context['_seq'] as $context["_key"] => $context["preview_item"]) {
            // line 106
            echo "                    <div class=\"row m-thumb-small-event outer-div right-animation\">
                        <a href=\"";
            // line 107
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo "/detail/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "slug");
            echo "\">
                        <div class=\"large-3 middle-3 small-3 columns thumb-small-event\">
                            <img src=\"";
            // line 109
            echo Uri::base();
            echo "media/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo "/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "image");
            echo "\">
                        </div>
                        </a>

                        <div class=\"large-1 middle-1 small-1 columns sub-icon-promo middle-div\"><img src=\"";
            // line 113
            echo Uri::base();
            echo "assets/img/icon/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo ".png\" class=\"icon-promo\"></div>
                        <div class=\"large-8 middle-8 small-8 columns middle-div\">
                            <a href=\"";
            // line 115
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo "/detail/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "slug");
            echo "\">
                                <p class=\"description\">";
            // line 116
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "title");
            echo "</p>
                            </a>
                            <a href=\"";
            // line 118
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "type");
            echo "/detail/";
            echo $this->getAttribute((isset($context["preview_item"]) ? $context["preview_item"] : null), "slug");
            echo "\" class=\"link-desc\">SEE MORE...</a>
                        </div>
                    </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['preview_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 122
        echo "            </div>
        </div>
        <div class=\"row\">
            <div class=\"large-6 medium-6 small-12 center-block columns see-more-button\">
                <a href=\"";
        // line 126
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/promo\" class=\"white\">
                    <div class=\"row see-more text-center default-color\">
                        SEE MORE PROMOS
                    </div>
                </a>
            </div>
            <div class=\"large-6 medium-6 small-12 center-block columns see-more-button\">
                <a href=\"";
        // line 133
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/event\" class=\"white\">
                    <div class=\"row see-more text-center default-color\">
                        SEE MORE EVENTS
                    </div>
                </a>
            </div>
            ";
        // line 139
        $this->displayBlock('back_to_top', $context, $blocks);
        // line 142
        echo "        </div>
";
    }

    // line 139
    public function block_back_to_top($context, array $blocks = array())
    {
        // line 140
        echo "                ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
            ";
    }

    // line 145
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 146
        echo "    <script src=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.js\"></script>

    <script type=\"text/javascript\">
        \$(document).ready(function(){
            \$('.brand-slider-list').bxSlider({
                minSlides: 2,
                maxSlides: 6,
                slideWidth: 170,
                slideMargin: 10,
                infiniteLoop : true,
                hideControlOnEnd : true,
                auto : true,
                moveSlides: 1
            });
        });
        ";
        // line 161
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 164
        echo "    </script>
";
    }

    // line 161
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 162
        echo "            ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  452 => 162,  449 => 161,  444 => 164,  442 => 161,  423 => 146,  420 => 145,  413 => 140,  410 => 139,  405 => 142,  403 => 139,  394 => 133,  384 => 126,  378 => 122,  364 => 118,  359 => 116,  351 => 115,  344 => 113,  333 => 109,  324 => 107,  321 => 106,  317 => 105,  313 => 103,  297 => 97,  293 => 96,  289 => 95,  283 => 94,  275 => 93,  264 => 89,  254 => 87,  250 => 86,  242 => 80,  230 => 71,  223 => 66,  221 => 65,  215 => 61,  202 => 60,  198 => 59,  191 => 54,  182 => 50,  179 => 49,  167 => 47,  165 => 46,  158 => 42,  151 => 40,  147 => 39,  141 => 38,  137 => 36,  134 => 35,  130 => 33,  120 => 32,  118 => 31,  111 => 27,  104 => 25,  100 => 24,  96 => 23,  92 => 21,  90 => 20,  86 => 19,  79 => 17,  75 => 16,  70 => 15,  66 => 14,  62 => 12,  59 => 11,  54 => 9,  51 => 8,  45 => 6,  41 => 5,  36 => 4,  33 => 3,);
    }
}
