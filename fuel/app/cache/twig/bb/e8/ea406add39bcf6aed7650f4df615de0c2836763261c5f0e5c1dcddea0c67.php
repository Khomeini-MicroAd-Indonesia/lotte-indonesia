<?php

/* superadmin.twig */
class __TwigTemplate_bbe8ea406add39bcf6aed7650f4df615de0c2836763261c5f0e5c1dcddea0c67 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/base.twig");

        $this->blocks = array(
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"form-box\" id=\"login-box\">
\t";
        // line 5
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 6
            echo "\t<div class=\"alert alert-danger alert-dismissable\">
\t\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t\t";
            // line 8
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
\t</div>
\t";
        }
        // line 11
        echo "\t<div class=\"header\">Superadmin Registration</div>
\t<form action=\"\" method=\"post\">
\t\t<div class=\"body bg-gray\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"text\" name=\"fullname\" class=\"form-control\" placeholder=\"Fullname*\" value=\"";
        // line 15
        echo (isset($context["txtfullname"]) ? $context["txtfullname"] : null);
        echo "\" required />
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"text\" name=\"phone\" class=\"form-control\" placeholder=\"Phone\" value=\"";
        // line 18
        echo (isset($context["txtphone"]) ? $context["txtphone"] : null);
        echo "\" />
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"text\" name=\"email\" class=\"form-control\" placeholder=\"Email*\" value=\"";
        // line 21
        echo (isset($context["txtemail"]) ? $context["txtemail"] : null);
        echo "\" required />
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" name=\"password\" class=\"form-control\" placeholder=\"Password*\" required />
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<input type=\"password\" name=\"password_confirm\" class=\"form-control\" placeholder=\"Password Confirm*\" required />
\t\t\t</div>
\t\t\t<div class=\"form-group text-red\">
\t\t\t\t*) Required
\t\t\t</div>
\t\t</div>
\t\t<div class=\"footer\">                                                               
\t\t\t<button type=\"submit\" class=\"btn bg-olive btn-block\">Submit</button>
\t\t</div>
\t</form>
</div>
";
    }

    public function getTemplateName()
    {
        return "superadmin.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 21,  58 => 18,  52 => 15,  46 => 11,  40 => 8,  36 => 6,  34 => 5,  31 => 4,  28 => 3,);
    }
}
