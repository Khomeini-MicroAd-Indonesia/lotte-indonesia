<?php

/* career.twig */
class __TwigTemplate_ad8707043808434f4cbd7dbbd2c030449123b1f948c8ee480ac508aabc905f6f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_careers' => array($this, 'block_menu_careers'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"assets/css/custom/career.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_menu_careers($context, array $blocks = array())
    {
        // line 7
        echo "    is-active
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"row career\">
        <div class=\"pre-title\">
            <p class=\"title\">CAREER</p>
            <div style=\"clear: both\"></div>
        </div>
        <div class=\"large-7 medium-7 small-12 columns\">
            <p>Prolog/ general requirements.
                Lorem ipsum dolor sit amet, autem clita accusamus te quo, eos facete salutatus et. Ius an stet quot omittantur, mel id tota brute contentiones. At putent admodum eum, no his nisl temporibus, sea cu delectus vivendum. Ei vis idque soleat singulis, dicat veritus pertinacia ex </p>
        </div>
        <div class=\"large-5 medium-5 small-12 columns\">
            <p>How to apply</p>
            <ol>
                <li>Lorem ipsum sit dolor amet</li>
                <li>Lorem ipsum sit dolor amet,</li>
                <li>Kirim ke <a href=\"mailto:recruitment@lotte.co.id\">recruitment@lotte.co.id</a></li>
            </ol>

        </div>

        <div class=\"pre-title\">
            <p class=\"title\">JOB VACANCIES</p>
            <div style=\"clear: both\"></div>
        </div>
        <ul class=\"accordion\" data-accordion role=\"tablist\">
            ";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["career_id"]) {
            // line 35
            echo "            <li class=\"large-6 medium-6 ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "last")) ? ("end") : (""));
            echo " columns accordion-navigation\">
                <a href=\"#panel-career-";
            // line 36
            echo (isset($context["career_id"]) ? $context["career_id"] : null);
            echo "\" role=\"tab\" id=\"panel-career-heading-";
            echo (isset($context["career_id"]) ? $context["career_id"] : null);
            echo "\" aria-controls=\"panel-career-";
            echo (isset($context["career_id"]) ? $context["career_id"] : null);
            echo "\" class=\"career-title\">Job Position ";
            echo (isset($context["career_id"]) ? $context["career_id"] : null);
            echo "<span class=\"career-view\" style=\"\">See Requirements</span></a>
                <div id=\"panel-career-";
            // line 37
            echo (isset($context["career_id"]) ? $context["career_id"] : null);
            echo "\" class=\"content ";
            echo (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "first")) ? ("active") : (""));
            echo "\" role=\"tabpanel\" aria-labelledby=\"panel-career-heading-";
            echo (isset($context["career_id"]) ? $context["career_id"] : null);
            echo "\">
                    Panel ";
            // line 38
            echo (isset($context["career_id"]) ? $context["career_id"] : null);
            echo ". Lorem ipsum dolor
                    ";
            // line 40
            echo "                        ";
            // line 41
            echo "                        ";
            // line 42
            echo "                        ";
            // line 43
            echo "                        ";
            // line 44
            echo "                        ";
            // line 45
            echo "                        ";
            // line 46
            echo "                        ";
            // line 47
            echo "                        ";
            // line 48
            echo "                    ";
            // line 49
            echo "                </div>
            </li>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['career_id'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "        </ul>
        <div class=\"large-6 end columns\">
        <p>
            Closing statement.
            Lorem ipsum dolor sit amet, autem clita accusamus te quo, eos facete salutatus et. Ius an stet quot omittantur, mel id tota brute contentiones. At putent admodum eum, no his nisl temporibus, sea cu delectus vivendum. Ei vis idque soleat singulis, dicat veritus pertinacia ex mei. Numquam propriae mei te, aperiri ocurreret te eos.
        </p>
        </div>
    </div>
";
    }

    // line 62
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 63
        echo "    <script src=\"assets/foundation/js/foundation/foundation.accordion.js\"></script>
";
    }

    public function getTemplateName()
    {
        return "career.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 63,  166 => 62,  154 => 52,  138 => 49,  136 => 48,  134 => 47,  132 => 46,  130 => 45,  128 => 44,  126 => 43,  124 => 42,  122 => 41,  120 => 40,  116 => 38,  108 => 37,  98 => 36,  93 => 35,  76 => 34,  50 => 10,  47 => 9,  42 => 7,  39 => 6,  34 => 4,  31 => 3,);
    }
}
