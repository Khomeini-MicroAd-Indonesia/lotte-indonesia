<?php

/* product_others.twig */
class __TwigTemplate_1575076039ad50ed22949f39904417725631792eaf979c843b8f1398fee3f225 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_product' => array($this, 'block_menu_product'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.css\" rel=\"stylesheet\">
    <link href=\"";
        // line 5
        echo Uri::base();
        echo "assets/css/custom/product-others.css\" rel=\"stylesheet\">
";
    }

    // line 7
    public function block_menu_product($context, array $blocks = array())
    {
        // line 8
        echo "    is-active
";
    }

    // line 10
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"row others\">
        <div class=\"large-12 columns banner\">
        ";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banner"]) ? $context["banner"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
            // line 14
            echo "            <img src=\"";
            echo Uri::base();
            echo "media/product/";
            echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "banner");
            echo "\">
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "        </div>
        ";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["product_overview"]) ? $context["product_overview"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 18
            echo "        <div class=\"large-12 columns overview\">
            <div class=\"pre-title-center\">
                <p class=\"title\">OVERVIEW</p>
                <div style=\"clear: both\"></div>
            </div>
            <p>";
            // line 23
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "content");
            echo "</p>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "        <div class=\"large-12 columns product-list\">
        ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["other_detail"]) ? $context["other_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 28
            echo "            <div class=\"pre-title\">
                <p class=\"title\">";
            // line 29
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "name");
            echo "</p>
                <div style=\"clear: both\"></div>
            </div>
            
            <ul class=\"accordion\" data-accordion>
            ";
            // line 34
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "otherproducts"));
            foreach ($context['_seq'] as $context["_key"] => $context["other_product"]) {
                // line 35
                echo "                <li class=\"accordion-navigation\">
                    <a href=\"#panel1a";
                // line 36
                echo $this->getAttribute((isset($context["other_product"]) ? $context["other_product"] : null), "id");
                echo "\" class=\"title-product\">";
                echo $this->getAttribute((isset($context["other_product"]) ? $context["other_product"] : null), "title");
                echo "<span class=\"year-icon\"></span></a>
                    <div id=\"panel1a";
                // line 37
                echo $this->getAttribute((isset($context["other_product"]) ? $context["other_product"] : null), "id");
                echo "\" class=\"content\">
                        <ul class=\"accordion large-block-grid-4 medium-block-grid-3 small-block-grid-2 variant-product\" data-equalizer=\"test\">
                            ";
                // line 39
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["other_product"]) ? $context["other_product"] : null), "othervariants"));
                foreach ($context['_seq'] as $context["_key"] => $context["other_product_variant"]) {
                    // line 40
                    echo "                            <li class=\"accordion-navigation\" data-equalizer-watch=\"test\">
                                <div>
                                    <img src=\"";
                    // line 42
                    echo Uri::base();
                    echo "media/product/";
                    echo $this->getAttribute($this->getAttribute((isset($context["other_product_variant"]) ? $context["other_product_variant"] : null), "images"), "filename");
                    echo "\">
                                    <p class=\"title-variant\">";
                    // line 43
                    echo $this->getAttribute((isset($context["other_product_variant"]) ? $context["other_product_variant"] : null), "title");
                    echo "</p>
                                    <p>";
                    // line 44
                    echo $this->getAttribute((isset($context["other_product_variant"]) ? $context["other_product_variant"] : null), "desc");
                    echo "</p>
                                </div>
                            </li>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['other_product_variant'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 48
                echo "                        </ul>
                    </div>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['other_product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "            </ul>
            
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "        </div>
    </div>
";
    }

    // line 59
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 60
        echo "    <script src=\"";
        echo Uri::base();
        echo "assets/bxslider/jquery.bxslider.js\"></script>
    <script src=\"";
        // line 61
        echo Uri::base();
        echo "assets/js/foundation/foundation.equalizer.js\"></script>
    <script>
        \$(document).foundation({
            equalizer : {
                equalize_on_stack: true
//                act_on_hidden_el: true
            }
        });
        \$(document).ready(function(){
//            \$('.bxslider').bxSlider();
            \$('.brand-slider-list').bxSlider({
                minSlides: 1,
                maxSlides: 3,
                slideWidth: 340,
                slideMargin: 10,
                infiniteLoop : false,
                hideControlOnEnd : true
            });

        });
        \$('#next').click(function() {
            \$('.current').removeClass('current').hide()
                    .next().show().addClass('current');
            if (\$('.current').hasClass('last')) {
                \$('#next').attr('disabled', true);
            }
            \$('#back').attr('disabled', null);
        });
        \$('#back').click(function() {
            \$('.current').removeClass('current').hide()
                    .prev().show().addClass('current');
            if (\$('.current').hasClass('first')) {
                \$('#back').attr('disabled', true);
            }
            \$('#next').attr('disabled', null);
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "product_others.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 61,  188 => 60,  185 => 59,  179 => 55,  171 => 52,  162 => 48,  152 => 44,  148 => 43,  142 => 42,  138 => 40,  134 => 39,  129 => 37,  123 => 36,  120 => 35,  116 => 34,  108 => 29,  105 => 28,  101 => 27,  98 => 26,  89 => 23,  82 => 18,  78 => 17,  75 => 16,  64 => 14,  60 => 13,  56 => 11,  53 => 10,  48 => 8,  45 => 7,  39 => 5,  34 => 4,  31 => 3,);
    }
}
