<?php

/* product_details.twig */
class __TwigTemplate_3fedee149555d3a3d0e308248d9fd8fe6bbb3ccd65ad4b32d1beb6d11a7509d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_product' => array($this, 'block_menu_product'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"assets/bxslider/jquery.bxslider.css\" rel=\"stylesheet\">
    <link href=\"assets/css/custom/product-details.css\" rel=\"stylesheet\">
";
    }

    // line 7
    public function block_menu_product($context, array $blocks = array())
    {
        // line 8
        echo "    is-active
";
    }

    // line 10
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"row banner\">
        <div class=\"large-12 columns\">
            <img src=\"assets/img/product/banner/banner-chocopie.jpg\">
        </div>
    </div>
    <div class=\"row\">
        <div class=\"pre-title-center\">
            <p class=\"title\">VARIANTS</p>
            <div style=\"clear: both\"></div>
        </div>
        <p class=\"title-desc text-center\">Variasi rasa/ kemasan produk Choco Pie</p>
    </div>
    <div class=\"brand-slider\">
        <div class=\"large-10 medium-10 large-centered medium-centered columns\">
            <ul class=\"text-center slide5\" id=\"brand-slide\">
                <li>
                    <div class=\"variants\">
                        <img src=\"assets/img/product/variants/chocopie-12pcs.jpg\" class=\"bottom-gap\">
                        <p class=\"title-variants\">CHOCOPIE 12 PCS</p>
                        <p class=\"desc\">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse </p>
                    </div>
                </li>
                <li>
                    <div class=\"variants\">
                        <img src=\"assets/img/product/variants/chocopie-12pcs-2.jpg\" class=\"bottom-gap\">
                        <p class=\"title-variants\">CHOCOPIE 12 PCS</p>
                        <p class=\"desc\">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse </p>
                    </div>
                </li>
                <li>
                    <div class=\"variants\">
                        <img src=\"assets/img/product/variants/chocopie-6pcs.jpg\" class=\"bottom-gap\">
                        <p class=\"title-variants\">CHOCOPIE 6 PCS</p>
                        <p class=\"desc\">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse </p>
                    </div>
                </li>
                <li>
                    <div class=\"variants\">
                        <img src=\"assets/img/product/variants/chocopie-6pcs.jpg\" class=\"bottom-gap\">
                        <p class=\"title-variants\">CHOCOPIE 6 PCS</p>
                        <p class=\"desc\">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse </p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class=\"row video\" data-equalizer>
        <div class=\"large-7 medium-7 small-12 columns\" data-equalizer-watch>
            <img src=\"assets/img/product/video/chocopie-video.jpg\">
        </div>
        <div class=\"large-5 medium-5 small-12 columns video-desc\" data-equalizer-watch>
            <div class=\"pre-title\">
                <p class=\"title\">TV COMMERCIAL</p>
                <div style=\"clear: both\"></div>
            </div>
            <p class=\"sub-title\">Choco Pie Indonesia</p>
            <p>(Deskripsi iklan)</p>
            <p>Lorem ipsum dolor sit amet, commune facilisi eu nam. Nominati sadipscing id duo. Id duo alia eruditi adipiscing. Rebum audire hendrerit cum ut, et mel minimum offendit intellegebat. </p>
            <p>Cu phaedrum vituperata inciderint nec. Ne nam atqui movet urbanitas. Qui ad qualisque periculis disputationi. Et mei habeo fabellas signiferumque. Aliquip deleniti volutpat ut sea. Sit ea nibh minim. Eros </p>
        </div>
    </div>
    <div class=\"row socmed\">
        <div class=\"pre-title-center\">
            <p class=\"title\">SOCIAL MEDIA</p>
            <div style=\"clear: both\"></div>
        </div>
        <div class=\"large-6 medium-6 small-12 columns socmed-section\">
            <p class=\"socmed-kind\">FACEBOOK <span class=\"right\">Lotte Xylitol ID</span></p>
            <div class=\"description-socmed\">
                <p class=\"date\">25 September 2015</p>
                <p>Lorem ipsum dolor sit amet, commune facilisi eu nam. Nominati sadipscing id duo. Id duo alia eruditi adipiscing. Rebum audire hendrerit cum ut, et mel minimum offendit intellegebat. Cu phaedrum vituperata inciderint nec. Ne nam atqui movet urbanitas.</p>
                <div class=\"\">
                    <p class=\"button-socmed\">Like us on Facebook</p>
                    <div style=\"clear: both\"></div>
                </div>

            </div>
        </div>
        <div class=\"large-6 medium-6 small-12 columns socmed-section\">
            <p class=\"socmed-kind\">TWITTER <span class=\"right\">@ Lotte Xylitol ID</span></p>
            <div class=\"description-socmed\">
                <p class=\"date\">25 September 2015</p>
                <p>Lorem ipsum dolor sit amet, commune facilisi eu nam. Nominati sadipscing id duo. Id duo alia eruditi adipiscing. Rebum audire hendrerit cum ut, et mel minimum offendit intellegebat. Cu phaedrum vituperata inciderint nec. Ne nam atqui movet urbanitas.</p>
                <div class=\"\">
                    <p class=\"button-socmed\">Like us on Facebook</p>
                    <div style=\"clear: both\"></div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"row socmed instagram\">
        <div class=\"large-12 medium-12 small-12 columns socmed-section\">
            <p class=\"socmed-kind\">INSTAGRAM <span class=\"right\">@ Lotte Xylitol ID</span></p>
            <div class=\"description-socmed\">
                <div class=\"row\">
                    <div class=\"large-2 medium-2 small-12 columns\">
                        <img src=\"assets/img/product/instagram-profile/chocopie-profile.jpg\">
                    </div>
                    <div class=\"large-10 medium-10 small-12 columns\">
                        <p class=\"date\">Brand Name</p>
                        <div class=\"large-9 medium-8 small-12 columns\">
                            <p>Lorem ipsum dolor sit amet, commune facilisi eu nam. Nominati sadipscing id duo. Id duo alia eruditi adipiscing. Rebum audire hendrerit cum ut, et mel minimum offendit intellegebat. Cu phaedrum vituperata inciderint nec. Ne nam atqui movet urbanitas.</p>
                        </div>
                        <div class=\"large-3 medium-4 small-12 columns\">
                            <p class=\"button-socmed instagram\">Follow us on Instagram</p>
                            <div style=\"clear: both\"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
";
    }

    // line 126
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 127
        echo "    <script src=\"assets/bxslider/jquery.bxslider.js\"></script>
    <script>
        \$(document).foundation();
        \$(document).ready(function(){
            \$('.bxslider').bxSlider();
            \$('.slide5').bxSlider({
                minSlides: 2,
                maxSlides: 3,
                slideWidth: 340,
                slideMargin: 10,
                infiniteLoop : false,
                hideControlOnEnd : true
            });

        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "product_details.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 127,  168 => 126,  51 => 11,  48 => 10,  43 => 8,  40 => 7,  34 => 4,  31 => 3,);
    }
}
