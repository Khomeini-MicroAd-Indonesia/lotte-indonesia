<?php

/* detail.twig */
class __TwigTemplate_0b2552ecf2e4d780478aa28e55fcdb4721fb0ccc577fee56b9d5fe68a8a47dbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_promo' => array($this, 'block_menu_promo'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/css/custom/promo-details.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_menu_promo($context, array $blocks = array())
    {
        // line 7
        echo "    is-active
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"row promo-details\" data-equalizer>
        <div class=\"large-2 medium-2 small-12 columns left-promo\" data-equalizer-watch>
            <ul>
        ";
        // line 13
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promo_detail"]) ? $context["promo_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            // line 14
            echo "            <li><a href=\"";
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/promo/detail/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "slug");
            echo "\" class=\"side-menu is-active\"><span>";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
            echo "</span></a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promo_list"]) ? $context["promo_list"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
            // line 17
            echo "            <li><a href=\"";
            echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
            echo "/promo/detail/";
            echo $this->getAttribute((isset($context["list"]) ? $context["list"] : null), "slug");
            echo "\" class=\"side-menu\"><span>";
            echo $this->getAttribute((isset($context["list"]) ? $context["list"] : null), "title");
            echo "</span></a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "            </ul>
        </div>
        <div class=\"large-10 medium-10 small-12 columns right-promo\" data-equalizer-watch>
        ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promo_detail"]) ? $context["promo_detail"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["detail"]) {
            echo " 
            <div class=\"large-11 large-offset-1 medium-11 medium-offset-1 small-12 columns\">
                <div class=\"row banner-section\">
                    <div class=\"large-10 medium-10 small-12 columns\">
                        <img src=\"";
            // line 26
            echo Uri::base();
            echo "media/promo/";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "banner");
            echo "\">
                    </div>
                    <div class=\"large-2 medium-2 small-12 columns share-to\">
                        Share to :
                        <ul>
                            <li><img class=\"social-media facebook\"> </li>
                            <li><img class=\"social-media twitter\"> </li>
                            <li><img class=\"social-media gplus\"> </li>
                        </ul>
                    </div>
                </div>
                <div class=\"header-promo-section\">
                    <h1 class=\"title\">";
            // line 38
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "title");
            echo "</h1>
                    <h3 class=\"info-periode\">Periode : ";
            // line 39
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "start");
            echo " - ";
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "end");
            echo "</h3>
                    <div class=\"pre-tag-promo\">
                        <p class=\"tag-promo\"><span><img src=\"";
            // line 41
            echo Uri::base();
            echo "assets/img/icon/promo.png\"></span>Promo</p>
                        <div style=\"clear: both\"></div>
                    </div>
                </div>
                <div class=\"promo-desc-section\">
                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">DESCRIPTION</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    <p>";
            // line 50
            echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "content");
            echo "</p>
                    
                </div>
                <div class=\"promo-desc-section\">
                    ";
            // line 54
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "terms")) > 0)) {
                // line 55
                echo "                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">SYARAT DAN KETENTUAN</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    ";
                // line 59
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "terms");
                echo "
                    ";
            }
            // line 61
            echo "                </div>
                <div class=\"promo-desc-section\">
                    ";
            // line 63
            if ((twig_length_filter($this->env, (isset($context["promo_prize"]) ? $context["promo_prize"] : null)) > 1)) {
                // line 64
                echo "                        <div class=\"pre-sub-title\">
                            <p class=\"sub-title\">PRIZE</p>
                            <div style=\"clear: both\"></div>
                        </div>
                        <ul class=\"large-block-grid-2 medium-block-grid-2 small-block-grid-2 prize-section\">
                            ";
                // line 69
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["promo_prize"]) ? $context["promo_prize"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["prize"]) {
                    // line 70
                    echo "                                <li>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['prize'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 72
                echo "                        </ul>
                    ";
            }
            // line 74
            echo "                </div>
                <div class=\"promo-desc-section\">
                    ";
            // line 76
            if ((twig_length_filter($this->env, (isset($context["promo_mechanism"]) ? $context["promo_mechanism"] : null)) > 0)) {
                // line 77
                echo "                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">MECHANISM</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    <ul class=\"large-block-grid-2 medium-block-grid-2 small-block-grid-2 mechanism-section\">
                    ";
                // line 82
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["promo_mechanism"]) ? $context["promo_mechanism"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["mechanism"]) {
                    // line 83
                    echo "                        <li>
                            <div>
                                <p class=\"text-left step\">";
                    // line 85
                    echo $this->getAttribute((isset($context["mechanism"]) ? $context["mechanism"] : null), "step");
                    echo "</p>
                                <img src=\"";
                    // line 86
                    echo Uri::base();
                    echo "media/promo/";
                    echo $this->getAttribute((isset($context["mechanism"]) ? $context["mechanism"] : null), "image");
                    echo "\">
                                <p class=\"step-desc\">";
                    // line 87
                    echo $this->getAttribute((isset($context["mechanism"]) ? $context["mechanism"] : null), "desc");
                    echo "</p>
                            </div>
                        </li>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mechanism'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 91
                echo "                    </ul>
                    ";
            }
            // line 93
            echo "                </div>
                ";
            // line 94
            if ((twig_length_filter($this->env, $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "more_info_link")) > 0)) {
                // line 95
                echo "                <div class=\"promo-desc-section\">
                    <div class=\"pre-sub-title\">
                        <p class=\"sub-title\">INFO LEBIH LANJUT</p>
                        <div style=\"clear: both\"></div>
                    </div>
                    <a href=\"";
                // line 100
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "more_info_link");
                echo "\" target=\"_blank\">
                        <img src=\"";
                // line 101
                echo Uri::base();
                echo "media/promo/";
                echo $this->getAttribute((isset($context["detail"]) ? $context["detail"] : null), "more_info_banner");
                echo "\" />
                    </a>
                </div>
                ";
            }
            // line 105
            echo "            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['detail'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "        </div>
    </div>
";
    }

    // line 111
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 112
        echo "    <script type=\"text/javascript\">

    </script>
";
    }

    public function getTemplateName()
    {
        return "detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  280 => 112,  277 => 111,  271 => 107,  264 => 105,  255 => 101,  251 => 100,  244 => 95,  242 => 94,  239 => 93,  235 => 91,  225 => 87,  219 => 86,  215 => 85,  211 => 83,  207 => 82,  200 => 77,  198 => 76,  194 => 74,  190 => 72,  183 => 70,  179 => 69,  172 => 64,  170 => 63,  166 => 61,  161 => 59,  155 => 55,  153 => 54,  146 => 50,  134 => 41,  127 => 39,  123 => 38,  106 => 26,  97 => 22,  92 => 19,  79 => 17,  74 => 16,  61 => 14,  57 => 13,  52 => 10,  49 => 9,  44 => 7,  41 => 6,  34 => 4,  31 => 3,);
    }
}
