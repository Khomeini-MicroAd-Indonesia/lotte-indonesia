<?php

/* promo.twig */
class __TwigTemplate_475e3bafe515a420bf08cf6b23bf864fe7c0e0bfec42f6d24b48140aba163170 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_promo' => array($this, 'block_menu_promo'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"assets/css/custom/promo.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_menu_promo($context, array $blocks = array())
    {
        // line 7
        echo "    is-active
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "<div class=\"row promo-thumb\">
    <ul class=\"large-block-grid-4 medium-block-grid-4 small-block-grid-2 promo-list\">
    ";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["promo_image"]) ? $context["promo_image"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["promo"]) {
            // line 13
            echo "        <li class=\"promo-thumb-list\">
            <a href=\"";
            // line 14
            echo Uri::base();
            echo "promo/detail\"><img src=\"";
            echo Uri::base();
            echo "media/promo/";
            echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "image");
            echo "\"></a>
            <a href=\"";
            // line 15
            echo Uri::base();
            echo "promo/detail\">
                <div class=\"promo-thumb-desc\">
                    <p class=\"description\">";
            // line 17
            echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "title");
            echo "</p>
                    <p class=\"sub-description\">";
            // line 18
            echo $this->getAttribute((isset($context["promo"]) ? $context["promo"] : null), "date");
            echo "</p>
                </div>
            </a>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['promo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "    </ul>
</div>
";
    }

    // line 27
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 28
        echo "    <script type=\"text/javascript\">
    </script>
";
    }

    public function getTemplateName()
    {
        return "promo.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 28,  95 => 27,  89 => 23,  78 => 18,  74 => 17,  69 => 15,  61 => 14,  58 => 13,  54 => 12,  50 => 10,  47 => 9,  42 => 7,  39 => 6,  34 => 4,  31 => 3,);
    }
}
