<?php

/* contact_us.twig */
class __TwigTemplate_777f6e1afda8b5e4d897c6a27e5e68000f6da318662611a3d46267066562a37f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_css' => array($this, 'block_frontend_css'),
            'menu_contact_us' => array($this, 'block_menu_contact_us'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'back_to_top' => array($this, 'block_back_to_top'),
            'frontend_js' => array($this, 'block_frontend_js'),
            'back_to_top_js' => array($this, 'block_back_to_top_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo Uri::base();
        echo "assets/css/custom/contact-us.css\" rel=\"stylesheet\">
";
    }

    // line 6
    public function block_menu_contact_us($context, array $blocks = array())
    {
        // line 7
        echo "    is-active
";
    }

    // line 9
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"row contact-us\">
        <div class=\"pre-title-center\">
            <p class=\"title\">CONTACT US</p>
        </div>
        <div class=\"large-4 medium-4 small-12 columns address\">
            <p class=\"title\">ADDRESS</p>
            <p>PT. LOTTE INDONESIA HEAD OFFICE</p>
            <address>
                Jl. Pela Raya No. 81<br>
                Jakarta Selatan 12140<br><br/>
                Tel: <a href=\"tel:+62-21-72780070\">+62-21-72780070</a><br>
                Fax: 02-21-72780081/ 82<br>
                Email: <a href=\"mailto:";
        // line 22
        echo Config::get("config_basic.contact_us_email_to");
        echo "\">";
        echo Config::get("config_basic.contact_us_email_to");
        echo "</a><br/>
            </address>
        </div>
        <div class=\"large-8 medium-8 small-12 columns\">
            <form action=\"\" method=\"post\" data-abide=\"ajax\" id=\"form-contact-us\">
                <div class=\"large-6 columns name-field ";
        // line 27
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "contact_name")) > 0)) ? ("error") : (""));
        echo "\">
                    <input name=\"contact_name\" type=\"text\" value=\"";
        // line 28
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "contact_name");
        echo "\" placeholder=\"Name*\" required=\"\"/>
                </div>
                <div class=\"large-6 columns email-field ";
        // line 30
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "contact_email")) > 0)) ? ("error") : (""));
        echo "\">
                    <input name=\"contact_email\" type=\"email\" value=\"";
        // line 31
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "contact_email");
        echo "\" placeholder=\"Email*\" required=\"\"/>
                </div>
                <div class=\"large-12 columns textarea-field ";
        // line 33
        echo (((twig_length_filter($this->env, $this->getAttribute((isset($context["err_msg"]) ? $context["err_msg"] : null), "contact_comment")) > 0)) ? ("error") : (""));
        echo "\">
                    <textarea name=\"contact_comment\" placeholder=\"Comment*\" required=\"\">";
        // line 34
        echo $this->getAttribute((isset($context["post_data"]) ? $context["post_data"] : null), "contact_comment");
        echo "</textarea>
                </div>
                <div class=\"large-12 columns g-recaptcha\" data-sitekey=\"6Leklw8TAAAAAP4RglzifNec2xs5bf_s8aYK_5hn\" data-callback=\"recaptchaCallback\"></div>
                <button type=\"send\">SEND</button>
                ";
        // line 39
        echo "                ";
        // line 40
        echo "                ";
        // line 41
        echo "                    ";
        // line 42
        echo "                    ";
        // line 43
        echo "                    ";
        // line 44
        echo "                ";
        // line 45
        echo "            </form>
        </div>

        <div class=\"large-12 columns\">
            <div class=\"pre-title-center\">
                <p class=\"title\">OUR LOCATION</p>
            </div>
            <iframe src=\"";
        // line 52
        echo Config::get("config_basic.contact_us_embed_url");
        echo "\" width=\"1171\" height=\"600\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>
        </div>
    </div>
    ";
        // line 55
        $this->displayBlock('back_to_top', $context, $blocks);
    }

    public function block_back_to_top($context, array $blocks = array())
    {
        // line 56
        echo "        ";
        $this->displayParentBlock("back_to_top", $context, $blocks);
        echo "
    ";
    }

    // line 60
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 61
        echo "    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script type=\"text/javascript\">
        \$(document).foundation({
            equalizer : {
                // Specify if Equalizer should make elements equal height once they become stacked.
                equalize_on_stack: true
            }
        });

        \$('.form-group .alert-danger').css(\"display\",\"none\");
        var flagCaptcha = false;

        var recaptchaCallback = function() {
            flagCaptcha = true;
        };

        \$('#form-contact-us').on('valid.fndtn.abide', function() {
            if (flagCaptcha) {
                \$(this).submit();
            }
        });
        ";
        // line 83
        $this->displayBlock('back_to_top_js', $context, $blocks);
        // line 86
        echo "    </script>
";
    }

    // line 83
    public function block_back_to_top_js($context, array $blocks = array())
    {
        // line 84
        echo "        ";
        $this->displayParentBlock("back_to_top_js", $context, $blocks);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "contact_us.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 84,  181 => 83,  176 => 86,  174 => 83,  150 => 61,  147 => 60,  140 => 56,  134 => 55,  128 => 52,  119 => 45,  117 => 44,  115 => 43,  113 => 42,  111 => 41,  109 => 40,  107 => 39,  100 => 34,  96 => 33,  91 => 31,  87 => 30,  82 => 28,  78 => 27,  68 => 22,  54 => 10,  51 => 9,  46 => 7,  43 => 6,  36 => 4,  33 => 3,);
    }
}
