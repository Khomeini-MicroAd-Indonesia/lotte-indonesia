<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 5/18/15
 * Time: 11:04 AM
 */
return array(

/* -----------------------------menu-----------------------------------*/
    'mn_home' => 'Beranda',
    'mn_about_us' => 'TENTANG KAMI',
    'mn_product' => 'PRODUK',
    'mn_product_others' => 'LAIN-LAIN',
    'mn_news_and_events' => 'ACARA & PROMO',
    'mn_news_and_events_dropdown' => 'ACARA',
    'mn_careers' => 'KARIR',
    'mn_contact_us' => 'Hubungi Kami',
    
/* --------------------------about_us----------------------------------*/
    'txt_title_greetings' => 'SAMBUTAN',
    'txt_greetings01' => 'LOTTE didirikan pada tahun 1948 dan tahun ini merayakan 67 tahun berdirinya perusahaan ini. Nama Perusahaan LOTTE berasal dari nama panggilan Lotte tokoh Charlotte dalam novel "The Sorrows of Young Werther" yang ditulis oleh sastrawan besar Jerman bernama Johann Wolfgang Von Goethe , dan nama ini digunakan untuk mewakili visi perusahaan untuk dicintai oleh semua orang.',
    'txt_greetings02' => 'Saat ini LOTTE tumbuh menjadi produsen permen nomor satu dengan sebutan "The Sweetheart of Your Mouth" melalui berbagai produk yang kami tawarkan termasuk permen karet, coklat dan kue. Kami sangat berterima kasih atas dukungan Anda hingga saat ini.',
    'txt_greetings03' => 'Selain itu, pengelompokkan dan pengembangan bisnis internasional menyebabkan pengembangan ke berbagai bidang di Jepang dan diawali oleh Lotteria, terutama di Asia dan Amerika, dan berkembang luas menjadi berbagai bidang, khususnya dalam industri bahan makanan di Korea Selatan.',
    'txt_greetings04' => 'LOTTE kini berkembang menjadi perusahaan global yang mempunyai lebih dari 50 anak perusahaan di Jepang dan luar negeri. Terbatasnya tempat untuk memperkenalkan dan berbagi informasi tentang bisnis dan produk kami, jadi kami sangat senang jika website ini bisa membantu anda mengenal dan memahami LOTTE.',
    'txt_greetings05' => 'LOTTE akan terus berkembang untuk tetap menjadi perusahaan yang dicintai oleh semua orang. Kami sangat berterima kasih atas dukungan Anda selama ini.',

    'txt_vision'      => 'VISI',
    'txt_vision2020'  => 'Lotte menjadi perusahaan yang berkembang dengan cepat dan dikagumi melalui penyediaan produk terpercaya yang membawa kebahagiaan dan menciptakan nilai baru untuk mendapatkan konsumen.',

    'txt_core_value'  => 'NILAI INTI DARI LOTTE INDONESIA :',
    'txt_core01'      => 'Semangat',
    'txt_core02'      => 'Kerjasama Tim',
    'txt_core03'      => 'Berorientasi pada Tindakan',
    'txt_core04'      => 'Integritas dan Terpercaya',
    'txt_core05'      => 'Bertanggung Jawab',

    'txt_core_value_definition' => 'DEFINISI - NILAI INTI DARI LOTTE INDONESIA',
    'txt_value01'     => 'Mengembangkan sikap positif dan optimis dalam menciptakan organisasi yang kompetitif dan kreatif. Didorong oleh semangat dan komitmen kepada pelanggan serta tekad untuk mengejar keunggulan. Senang  dengan apa yang dikerjakan dan berani untuk menghadapi dan mengatasi tantangan yang datang.',
    'txt_value02'     => 'Bekerja dengan penuh kerjasama dan koordinasi satu sama lain. Membangun dan mempertahankan hubungan yang sehat dan produktif dengan cara tukar pikiran, pandangan, dan pendapat untuk masa depan yang lebih baik.',
    'txt_value03'     => 'Bersedia mengambil tindakan praktis untuk menangani masalah atau situasi dalam rangka memenuhi kepuasan konsumen. Percaya bahwa tindakan kita memotivasi bukan hanya diri sendiri tapi juga perusahaan. Tidak takut mengambil tindakan lebih lanjut untuk pengembangan dan perbaikan diri untuk mencapai tujuan.',
    'txt_value04'     => 'Menciptakan lingkungan kerja dengan keterbukaan komunikasi, berbicara jujur, berbagi sudut pandang dan terbuka terhadap satu sama lain.',
    'txt_value05'     => 'Mematuhi aturan, peraturan dan standar etika. Bertanggung jawab atas tindakan sendiri, mengetahui apapun yang dilakukan tidak hanya berdampak pada diri sendiri tapi juga pada perusahaan. Kami bekerja dengan tulus dan penuh rasa tanggung jawab.',

    'txt_expansion'   => 'EKSPANSI LOTTE',

    'txt_history_title'     => 'SEJARAH',
    'txt_history'     => 'Sejarah berdirinya PT. Lotte Indonesia',
    'txt_present'     => 'Sekarang',

/*----------------------------promo--------------------------------------*/
    'txt_no-promo'     => 'Maaf, Belum Ada Promo yang Tersedia.',
    'txt-no-event'     => 'Maaf, Belum Ada Event yang Tersedia.',
    'txt-no-result'     => 'Maaf, Tidak ada Hasil. Coba kata kunci lain.',

/*----------------------------career--------------------------------------------*/
    'txt_no-job-vacancy' => 'Maaf, Belum Ada Lowongan Pekerjaan yang Tersedia',
    'txt_career_desc_1' => 'Sejak didirikan tahun 1948 di Jepang, LOTTE terus mengembangkan bisnis di seluruh dunia. LOTTE kini berkembang menjadi perusahaan global yang mempunyai lebih dari 50 anak perusahaan di Jepang dan luar negeri.',
    'txt_career_desc_2' => 'Kami, PT. LOTTE Indonesia dan PT. LOTTE Trade & Distribution merupakan bagian dari LOTTE Group yang bergerak di bisnis makanan. Tujuan kami adalah membuat produk yang disukai setiap orang di Indonesia. Rangkaian produk kami antara lain permen karet, permen manis dan biscuit. PT LOTTE Indonesia didirikan tahun1993 memproduksi produk unggulan LOTTE seperti LOTTE Xlylitol dan LOTTE Chocopie. Kami juga memiliki produk biscuit Koala March’s dan TOPPO.',
    'txt_career_desc_3' => 'Sementara itu PT LOTTE Trade & Distribution fokus binisnya pada penjualan dan distribusi produk kami ke seluruh Indonesia dan luar negeri.',
    'txt_how_to'        => 'Cara Melamar :',
    'txt_how_to_desc_1' => 'Klik lowongan pekerjaan yang tersedia dan lihat persyaratan yang dibutuhkan.',
    'txt_how_to_desc_2' => 'Perbarui CV Anda max 2 halaman dalam format PDF.',
    'txt_how_to_desc_3' => 'Kirim email ke <a href="mailto:recruitment@lotte.co.id">recruitment@lotte.co.id</a> dan silahkan menulis posisi yang Anda ingin lamar dalam email Subyek.',
/*----------------------------search--------------------------------------------*/
    'txt_no-search' => 'Maaf, Tidak ada hasil',
/*-----------------------product detail--------------------------------------*/
    'txt_variasi' => 'Variasi rasa / kemasan produk',
);
