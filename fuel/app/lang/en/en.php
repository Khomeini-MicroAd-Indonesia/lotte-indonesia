<?php
/**
 * Created by PhpStorm.
 * User: Albert
 * Date: 5/18/15
 * Time: 11:04 AM
 */
return array(

/* -----------------------------menu-----------------------------------*/
    'mn_home' => 'Home',
    'mn_about_us' => 'ABOUT US',
    'mn_product' => 'PRODUCT',
    'mn_product_others' => 'OTHERS',
    'mn_news_and_events' => 'EVENTS & PROMOS',
    'mn_careers' => 'CAREERS',
    'mn_contact_us' => 'Contact Us',
    'mn_news_and_events_dropdown' => 'EVENTS',
    
/* --------------------------about_us----------------------------------*/
    'txt_title_greetings' => 'GREETINGS',
    'txt_greetings01' => 'LOTTE was founded in 1948 and this year celebrates its 67th year. The Company name of LOTTE comes from the nickname of Lotte for Charlotte, the heroine in the novel The Sorrows of Young Werther written by the great German literary figure Johann Wolfgang Von Goethe and this name was used to represent the desire for the company to be loved by everyone.',
    'txt_greetings02' => 'Today LOTTE has grown into a general sweets producer family as "The Sweetheart of Your Mouth" through the wide variety of products we offer including chewing gum, chocolate and cookies. We are grategul for your support that has made this possible.',
    'txt_greetings03' => 'In addition, the diversification and internationalization of the Company\'s business have led to diversification into wide range of fields in Japan and represented by Lotteria, the mainly in Asia and America, and is expanding into and growing into a wide variety of fields, particulary in the foodstuffs industry in South Korea.',
    'txt_greetings04' => 'LOTTE has now grown into a global company group over 50 companies in Japan and overseas. There is not room to introduce all of them on our website, but there is much information about the main companies, so we would be pleased if you tool advantage of this information to deepen your understanding of LOTTE\'s business and products.',
    'txt_greetings05' => 'LOTTE will continue expanding its activities to remain a company loved by everyone. We would be grateful for your continuing support.',

    'txt_vision'      => 'VISION',
    'txt_vision2020'  => 'Lotte becomes the fastest growing & admired company which delivers trust worthy products that bring happiness & create new values to target consumer.',

    'txt_core_value'  => 'CORE VALUES OF LOTTE INDONESIA :',
    'txt_core01'      => 'Passion',
    'txt_core02'      => 'Team Work',
    'txt_core03'      => 'Action Oriented',
    'txt_core04'      => 'Integrity and Trust',
    'txt_core05'      => 'Responsibility',

    'txt_core_value_definition' => 'CORE VALUES LOTTE INDONESIA - DEFINITION',
    'txt_value01'     => 'We develop a positive and optimistic attitude in creating a competitive and creative organization. We are driven by our passion, commitment to our customers and determination in the pursuit of excellence. We are joyful in what we do. We are courageous to face and tackle the challenges that come our way. We push ourselves for continuous improvement. We have the discipline to do what needs to be done. We set higher goals with conviction that we will succeed.',
    'txt_value02'     => 'We are willing to take practical action to deal with problem or situation in order to meet our customer satisfaction. We believe our action motivates not just ourselves  but also to the company. We do not afraid taking action further development and improvement by ourselves in order to achieve our own goals.',
    'txt_value03'     => 'We abide in the rules, regulations and ethical standards. Qw believe in ownership, knowing each and everyone\'s role and comesponding duties to the company and the client. We are responsible for our actions, knowing that whatever we do reflects not just to ourselves but also to the company. We deliver and produce results barring constraints.',
    'txt_value04'     => 'We work in a cooperative and coordinated way. We establish and maintain healthy and productive relationships with internal and external customers through exchange thouhgts, views and opinions. We consider others perspectives to produce synergistic effects in paving the future.',
    'txt_value05'     => 'We work in an environment where there is open communication. We speak the truth. We share our view points. We are transparent to one another. We have confidence in the ability of our people to carry on the responsibility of his job in an excellent manner. We build trust by keeping our word.',

    'txt_expansion'   => 'LOTTE EXPANSION',

    'txt_history_title'     => 'HISTORY',
    'txt_history'     => 'The History of PT. Lotte Indonesia',
    'txt_present'     => 'Present',

/*-----------------------------promo--------------------------------------------*/
    'txt_no-promo'     => 'Sorry, There is no Promo Available',
    'txt-no-event'     => 'No event are currently being held',
    'txt-no-result'     => 'Sorry, there is no result. Try another keywords',

/*----------------------------career--------------------------------------------*/
    'txt_no-job-vacancy' => 'Sorry, There is no Job Vacancy Available',
    'txt_career_desc_1' => 'Since founded 1948 in Japan, LOTTE continue to develop its business worldwide. Now LOTTE has grown into a global company group with over 50 companies in Japan and overseas.',
    'txt_career_desc_2' => 'We, PT. LOTTE Indonesia and PT LOTTE Trade & Distribution are part of LOTTE Group focusing on confectionary business in Indonesia. Our goal is to produce products loved by as many people as possible. Our products are ranging from chewing gum, candy and biscuits. PT LOTTE Indonesia established in 1993 as our manufacture company, produces LOTTE Global brands such as LOTTE Xylitol and LOTTE Chocopie. We also have Koala March biscuits and TOPPO.',
    'txt_career_desc_3' => 'While out PT LOTTE Trade & Distibution is focusing in sales and distribution of our products to domestic and overseas market.',
    'txt_how_to'        => 'How to apply',
    'txt_how_to_desc_1' => 'Click our job vacancy and see the requirements we needed.',
    'txt_how_to_desc_2' => 'Update your CV max. 2 pages in format PDF.',
    'txt_how_to_desc_3' => 'Sent to email <a href="mailto:recruitment@lotte.co.id">recruitment@lotte.co.id</a> and please write the position you apply in Subject email.',
/*----------------------------search--------------------------------------------*/
    'txt_no-search' => 'Sorry, There is no Result',
/*-----------------------product detail--------------------------------------*/
    'txt_variasi' => 'Variety of Products',
);

