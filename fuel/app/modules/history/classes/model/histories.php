<?php
namespace History;

class Model_Histories extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'histories';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'event'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'event'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'event'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'year' => array(
			'label' => 'Year',
			'validation' => array(
				'required',
				'max_length' => array(4),
			)
		),
		'description' => array(
			'label' => 'Description',
			'validation' => array(
				'required',
			)
		),
                'descriptionid' => array(
			'label' => 'Descriptionid',
			'validation' => array()
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_form_data_basic() {
		return array(
			'attributes' => array(
				'name' => 'frm_history',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Year',
						'id' => 'year',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'year',
						'value' => $this->year,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Year',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Description',
						'id' => 'description',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'description',
						'value' => $this->description,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Descriptionid',
						'id' => 'descriptionid',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'descriptionid',
						'value' => $this->descriptionid,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Seq',
						'id' => 'seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '0',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
			)
		);
	}
}
