<?php
namespace Career;

class Careerutil{
	
    /**
     * get career data
     */
    
    public static function get_career_data(){
        
        $careers = \Career\Model_Careers::query()
                ->where('status', 1)
                ->order_by('created_at','desc')
                ->get();
                
        $data = array();
        
        foreach ($careers as $career){
            
            $data[] = array(
                'name'  => $career->name,
                'id'    => $career->id,
                'req'   => $career->req
            );
        }
        return $data;
    }

    
}