<?php
namespace Career;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'news';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/career');
        $this->_data_template['career_detail'] = \Career\Careerutil::get_career_data();
        
        $data_career_banner = $this->get_section_page($this->_meta_slug.$this->_current_lang.'/career-banner');
        if (empty($data_career_banner)) {
            $data_career_banner = $this->get_section_page('/career-banner');
        }
        $this->_data_template['data_career_banner'] = $data_career_banner;
        
        $data_career_howtoapplay = $this->get_section_page($this->_meta_slug.$this->_current_lang.'/career-how-to-apply');
        if (empty($data_career_howtoapplay)) {
            $data_career_howtoapplay = $this->get_section_page('/career-how-to-apply');
        }
        $this->_data_template['data_career_howtoapplay'] = $data_career_howtoapplay;
        
        return \Response::forge(\View::forge('career::frontend/career.twig', $this->_data_template, FALSE));
    }

}

