<?php

namespace Pages;


class Searchutil {

    /*
     * get product
     */

    public static function get_product($keyword) {

        $details = \Product\Model_ProductVariants::query()
                ->related('images')
                ->where('title', 'like', '%'.$keyword.'%')
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title' => $detail->title,
                'desc'  => $detail->desc,
                'image' => $detail->images->filename,
                'status'   => 'product'
            );
        }
        return $data;
    }
    
    /*
     * get event
     */

    public static function get_event($keyword) {

        $details = \Event\Model_Events::query()
                ->related('images')
                ->where('title', 'like', '%'.$keyword.'%')
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title'    => $detail->title,
                'content'  => $detail->content,
                'image'    => $detail->images->filename,
                'status'   => 'event'
            );
        }
        return $data;
    }
    
    /*
     * get promo
     */

    public static function get_promo($keyword) {

        $details = \Promo\Model_Promos::query()
                ->related('images')
                ->where('title', 'like', '%'.$keyword.'%')
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title' => $detail->title,
                'content'  => $detail->content,
                'image' => $detail->images->filename,
                'status'=> 'promo'
            );
        }
        return $data;
    }
    

}
