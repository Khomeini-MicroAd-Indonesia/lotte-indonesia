<?php

namespace Pages;

class Controller_Frontend_Home extends \Controller_Frontend {

    private $_module_url = '';
    private $_menu_key = 'home';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }
    
    private function _mergePromoEvent($promos, $events) {
        $data = array_merge($promos, $events);
        // Prioritize based on flag
        for ($idx=0; $idx<count($data)-1; $idx++) {
            for ($idy=$idx+1; $idy<count($data); $idy++) {
                $temp1 = $data[$idx];
                $temp2 = $data[$idy];
                if ($temp2['flag'] > $temp1['flag']) {
                    $data[$idx] = $temp2;
                    $data[$idy] = $temp1;
                }
            }
        }
        // Sort by post date DESC
        for ($idx=0; $idx<count($data)-1; $idx++) {
            for ($idy=$idx+1; $idy<count($data); $idy++) {
                $temp1 = $data[$idx];
                $temp2 = $data[$idy];
                if ($temp1['flag'] == 0 && strtotime($temp2['post_date']) > strtotime($temp1['post_date'])) {
                    $data[$idx] = $temp2;
                    $data[$idy] = $temp1;
                }
            }
        }
        return array_slice($data, 0, 4);
    }

    public function action_index() {
        
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/home');
        $this->_data_template['home_banner'] = \Homebanner\Bannerutil::get_home_banner();
        $this->_data_template['home_product'] = \Product\Productutil::get_home_product();
        $promo_preview = \Promo\Promoutil::get_promo_preview();
        $event_preview = \Event\Eventutil::get_event_preview();
        $this->_data_template['promo_event_preview'] = $this->_mergePromoEvent($promo_preview, $event_preview);
        return \Response::forge(\View::forge('pages::frontend/home.twig', $this->_data_template, FALSE));
    }

    public function action_about_us() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/about-us');
        $this->_data_template['achievement_list'] = \Achievement\Model_Achievements::query()
                ->where('status', 1)
                ->order_by('post_date', 'DESC')
                ->get();
        $this->_data_template['core_value_list'] = \CorpCoreValues\Model_CoreValues::query()
                ->where('status', 1)
                ->where('lang_id', $this->_current_lang)
                ->order_by('seq')
                ->get();
        $this->_data_template['history_list'] = \History\Model_Histories::query()
                ->where('status', 1)
                //->where('descriptionid', '!=', '')
                ->order_by('year')
                ->get();
        
        $data_greeting_image = $this->get_section_page($this->_meta_slug.$this->_current_lang.'/about-us-greeting-image');
        if (empty($data_greeting_image)) {
            $data_greeting_image = $this->get_section_page('/about-us-greeting-image');
        }
        $this->_data_template['data_greeting_image'] = $data_greeting_image;
        return \Response::forge(\View::forge('pages::frontend/about_us.twig', $this->_data_template, FALSE));
    }


    private function _send_email($post_data) {
        $data = array(
            'email_from' => 'website@lotte.co.id',
            'email_to' => \Config::get('config_basic.contact_us_email_to'),
            'email_subject' => '[Website lotte.co.id] Contact Us message from ' . $post_data['contact_name'],
            'email_reply_to' => array(
                'email' => $post_data['contact_email'],
                'name' => $post_data['contact_name']
            ), // Optional
            'email_data' => array(
                'base_url' => \Uri::base(),
                'contact_name' => $post_data['contact_name'],
                'contact_email' => $post_data['contact_email'],
                'contact_comment' => $post_data['contact_comment'],
            ),
            'email_view' => 'pages::email/contact_us.twig',
        );

        $email = \Email::forge();

        // Set the from address
        $email->from($data['email_from'], 'Lotte Indonesia');
        // Set the to address
        $email->to($data['email_to'],'Marketing Division Lotte Indonesia');
        // Set a subject
        $email->subject($data['email_subject']);
        // And set the body.
        $email->html_body(\View::forge($data['email_view'], $data['email_data']));
        $email->send();
        \Util_Email::queue_send_email($data);
    }

    private function _contact_submission() {
        $_post_data = \Input::post();
        //var_dump($_post_data); exit;
        if (count($_post_data) > 0) {
            $_err_msg = [];
            $val = \Validation::forge('contact_validation');
            $val->add('contact_name', 'Your Name')->add_rule('required');
            $val->add('contact_email', 'Your Email')->add_rule('required|valid_email');
            $val->set_message('required', 'Please fill in :label');
            $val->set_message('valid_email', 'Please fill in :label with valid email');
            if (!$val->run()) {
                foreach ($val->error() as $field => $error) {
                    $_err_msg[$field] = $error->get_message();
                }
            } else {

                $this->_send_email($_post_data);
                \Session::set_flash('contact_success_message', 'Thank you for your comment.');
                \Response::redirect(\Uri::current());
            }
            $this->_data_template['err_msg'] = $_err_msg;
            $this->_data_template['post_data'] = $_post_data;
        }
    }

    public function action_contact_us() {
        $this->_contact_submission();
        $this->_data_template['success_message'] = \Session::get_flash('contact_success_message');
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/contact-us');
        return \Response::forge(\View::forge('pages::frontend/contact_us.twig', $this->_data_template, FALSE));
    }

    public function action_search() {
        $this->set_meta_info($this->_meta_slug . $this->_current_lang . '/search');
        
        $keyword = \Input::get('keyword');
        
        $this->set_meta_info('search');
        $this->_data_template['meta_desc'] .= ' '.$keyword;
        
        if(strpos($keyword,'event') !== FALSE){
            
            $this->_data_template['search_result'] = \Pages\Searchutil::get_event($keyword);
            
        }else if(strpos($keyword,'promo') !== FALSE){
            
            $this->_data_template['search_result'] = \Pages\Searchutil::get_promo($keyword);
            
        }
        else{
            
            $this->_data_template['search_result'] = \Pages\Searchutil::get_product($keyword);
            
        }       
        $this->_data_template['keyword'] = $keyword;
        
        return \Response::forge(\View::forge('pages::frontend/search.twig', $this->_data_template, FALSE));
    }


}
