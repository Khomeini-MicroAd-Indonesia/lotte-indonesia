<?php

namespace CorpCoreValues;

class Model_CoreValues extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    private $lang_names = array('id' => 'Indonesia', 'en' => 'English');
    private $image_path = 'media/core-value/';
    protected static $_table_name = 'corporate_core_values';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'achivements' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'achivements' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'achivements' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'name' => array(
            'label' => 'Core Value Name',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'description' => array(
            'label' => 'Core Value Description',
            'validation' => array(
                'required',
            )
        ),
        'icon' => array(
            'label' => 'Core Value Icon',
            'validation' => array(
                'max_length' => array(500),
            )
        ),
        'lang_id' => array(
            'label' => 'Lang ID',
            'validation' => array(
                'required',
            )
        ),
        'bg_color_code' => array(
            'label' => 'BG Color Code',
            'validation' => array(
                'required',
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'seq' => array(
            'label' => 'Sequence',
            'validation' => array(
                'required',
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public static function get_as_array($filter = array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
                $data[$item->id] = $item->title;
            }
        }
        return $data;
    }

    public function get_image_path() {
        return $this->image_path;
    }

    public function get_all_images() {
        if (file_exists(DOCROOT . $this->image_path)) {
            $contents = \File::read_dir(DOCROOT . $this->image_path);
        } else {
            $contents = array();
        }
        return $contents;
    }

    public function get_form_data_basic($model_data=null) {
        return array(
            'attributes' => array(
                'name' => 'frm_corp_core_values',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Lang ID',
                        'id' => 'ccv_lang_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'ccv_lang_id',
                        'value' => $this->lang_id,
                        'options' => $this->lang_names,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Lang ID',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Name',
                        'id' => 'ccv_name',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'ccv_name',
                        'value' => $this->name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Core Value Name',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Description',
                        'id' => 'ccv_desc',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'ccv_desc',
                        'value' => $this->description,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => 'Core Value Description',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Core Value Icon',
                        'id' => 'ccv_icon',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'ccv_icon',
                        'value' => $this->icon,
                        'options' => $this->get_all_images(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base() . $this->image_path,
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'BG Color Code',
                        'id' => 'ccv_bg_color_code',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'ccv_bg_color_code',
                        'value' => $this->bg_color_code,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'FFFFFF',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'ccv_status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'ccv_status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Seq',
                        'id' => 'ccv_seq',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'ccv_seq',
                        'value' => $this->seq,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => '0',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }
    
    public function fill_form_data($post_data, $admin_id) {
        $this->lang_id = isset($post_data['ccv_lang_id']) ? $post_data['ccv_lang_id'] : null;
        $this->name = isset($post_data['ccv_name']) ? $post_data['ccv_name'] : null;
        $this->description = isset($post_data['ccv_desc']) ? $post_data['ccv_desc'] : null;
        $this->icon = isset($post_data['ccv_icon']) ? $post_data['ccv_icon'] : null;
        $this->bg_color_code = isset($post_data['ccv_bg_color_code']) ? $post_data['ccv_bg_color_code'] : null;
        $this->status = isset($post_data['ccv_status']) ? $post_data['ccv_status'] : 0;
        $this->seq = isset($post_data['ccv_seq']) ? $post_data['ccv_seq'] : null;
        // Set created_by/updated_by
        $tmp_field_name = ($this->id > 0) ? 'updated_by' : 'created_by';
        $this->$tmp_field_name = $admin_id;
    }

}
