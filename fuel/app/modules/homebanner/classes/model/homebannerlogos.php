<?php
namespace Homebanner;

class Model_HomebannerLogos extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	private $image_path = 'media/homebanner/';
	
	protected static $_table_name = 'homebanner_logos';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'homebanner'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'homebanner'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'homebanner'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'name' => array(
			'label' => 'Home Banner Logo Name',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'filename' => array(
			'label' => 'Home Banner Filename',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
        
        protected static $_has_many = array(
            'homebanner' => array(
                'key_from'          =>  'id',
                'model_to'          =>  '\homebanner\Model_Homebanners',
                'key_to'            =>  'image_id',
                'cascade_save'      =>  false,
                'cascade_delete'    =>  false
                
            )
        );
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_image_path() {
		return $this->image_path;
	}
	
        public function get_all_images() {
		if (file_exists(DOCROOT.$this->image_path)) {
			$contents = \File::read_dir(DOCROOT.$this->image_path);
		} else {
			$contents = array();
		}
		return $contents;
	}
        
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
	
	public function get_form_data_basic() {
		return array(
			'attributes' => array(
				'name' => 'frm_homebanner_logo',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Logo Name',
						'id' => 'logo_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'logo_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Logo Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Seq',
						'id' => 'logo_seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'logo_seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '0',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Logo',
						'id' => 'logo_filename',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select_image_picker' => array(
						'name' => 'logo_filename',
						'value' => $this->filename,
						'options' => $this->get_all_images(),
						'attributes' => array(
							'class' => 'form-control image-picker',
						),
						'container_class' => 'col-sm-10',
						'image_url' => \Uri::base().$this->image_path,
					)
				),
			)
		);
	}
}
