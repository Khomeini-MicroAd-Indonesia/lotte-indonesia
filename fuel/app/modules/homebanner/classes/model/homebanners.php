<?php

namespace Homebanner;

class Model_Homebanners extends \Orm\Model {

    private $status_name = array('InActive', 'Active');
    protected static $_table_name = 'homebanners';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'homebanners' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'homebanners' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'homebanners' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'slug' => array(
            'label' => 'Home Banner Slug',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'image_id' => array(
            'label' => 'Image',
            'validation' => array(
                'required',
            )
        ),
        'logo_id' => array(
            'label' => 'Logo',
            'validation' => array(
                'required',
            )
        ),
        'title' => array(
            'label' => 'Home Banner Title',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'meta_title' => array(
            'label' => 'Home Banner Meta Title',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'content_id' => array(
            'label' => 'Home Banner Content (Bahasa)',
            'validation' => array()
        ),
        'content_en' => array(
            'label' => 'Home Banner Content (English)',
            'validation' => array()
        ),
        'bgcolor' => array(
            'label' => 'Home Banner BGColor',
            'validation' => array(
                'required',
                'max_length' => array(6),
            )
        ),
        'fontcolor' => array(
            'label' => 'Home Banner FontColor',
            'validation' => array(
                'required',
                'max_length' => array(6),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'post_date' => array(
            'label' => 'Post Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    protected static $_belongs_to = array(
        'images' => array(
            'key_from' => 'image_id',
            'model_to' => '\homebanner\Model_HomebannerImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'logos' => array(
            'key_from' => 'logo_id',
            'model_to' => '\homebanner\Model_HomebannerLogos',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
    private static $_images, $_logos;

    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public function get_image_name() {
        if (empty(self::$_images)) {
            self::$_images = Model_HomebannerImages::get_as_array();
        }
        $flag = $this->image_id;
        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
    }

    public function get_logo_name() {
        if (empty(self::$_logos)) {
            self::$_logos = Model_HomebannerLogos::get_as_array();
        }
        $flag = $this->logo_id;
        return isset(self::$_logos[$flag]) ? self::$_logos[$flag] : '-';
    }

    public function get_form_data_basic($image, $logo) {
        return array(
            'attributes' => array(
                'name' => 'frm_homebanners',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Slug',
                        'id' => 'slug',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'slug',
                        'value' => $this->slug,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Slug',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_id',
                        'value' => $this->image_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Logo',
                        'id' => 'logo_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'logo_id',
                        'value' => $this->logo_id,
                        'options' => $logo,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Logo',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'homebanner_title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'homebanner_title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Meta Title',
                        'id' => 'homebanner_metatitle',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'homebanner_metatitle',
                        'value' => $this->meta_title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Meta Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Post Date',
                        'id' => 'post_date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'post_date',
                        'value' => $this->post_date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Post Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Home Banner Content (Bahasa)',
                        'id' => 'homebanner_content_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'homebanner_content_id',
                        'value' => $this->content_id,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Home Banner Content (English)',
                        'id' => 'homebanner_content_en',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'homebanner_content_en',
                        'value' => $this->content_en,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'BGColor',
                        'id' => 'homebanner_bgcolor',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'homebanner_bgcolor',
                        'value' => $this->bgcolor,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'BGColor',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'FontColor',
                        'id' => 'homebanner_fontcolor',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'homebanner_fontcolor',
                        'value' => $this->fontcolor,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'FontColor',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
            )
        );
    }

}
