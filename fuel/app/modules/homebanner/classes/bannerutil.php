<?php
namespace Homebanner;

class Bannerutil{
	
    /**
     * get home banner 
     */
    
    public static function get_home_banner(){
        
        $banners = \Homebanner\Model_Homebanners::query()
                ->related('images')
                ->related('logos')
                ->where('status', 1)
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($banners as $banner){
            $temp = 'content_'.\Config::get('language');
            $data[] = array(
                'slug'        => $banner->slug,  
                'title'       => $banner->title,
                'content'     => $banner->$temp, 
                'bgcolor'     => $banner->bgcolor,
                'fontcolor'   => $banner->fontcolor,
                'image'       => $banner->images->filename,
                'logo'        => $banner->logos->filename
            );
        }
        return $data;
    }

    
}