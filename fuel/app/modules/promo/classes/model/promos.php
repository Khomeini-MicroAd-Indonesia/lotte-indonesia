<?php

namespace Promo;

class Model_Promos extends \Orm\Model {
    
    private $flag_status = array('Off','On');
    private $status_name = array('InActive', 'Active');
    protected static $_table_name = 'promos';
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'promos' => array('before_insert'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_UpdatedAt' => array(
            'promos' => array('before_update'),
            'mysql_timestamp' => true
        ),
        'Orm\Observer_Validation' => array(
            'promos' => array('before_save')
        )
    );
    protected static $_properties = array(
        'id',
        'banner_id' => array(
            'label' => 'Banner',
            'validation' => array(
                'required',
            )
        ),
        'image_id' => array(
            'label' => 'Image',
            'validation' => array(
//                'required',
            )
        ),
        'slug' => array(
            'label' => 'Promo Slug',
            'validation' => array(
                'required',
                'max_length' => array(100),
            )
        ),
        'title' => array(
            'label' => 'Promo Title',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'flag' => array(
            'label' => 'Promo Flag',
            'validation' => array(
                'required',
            )
        ),
        'teaser' => array(
            'label' => 'Promo Teaser',
            'validation' => array(
                'max_length' => array(150)
            )
        ),
        'meta_title' => array(
            'label' => 'Meta Title',
            'validation' => array(
                'required',
                'max_length' => array(50),
            )
        ),
        'meta_desc' => array(
            'label' => 'Meta Desc',
            'validation' => array(
                'required',
                'max_length' => array(150),
            )
        ),
        'content' => array(
            'label' => 'Promo Content',
            'validation' => array()
        ),
        'terms' => array(
            'label' => 'Promo Terms and Conditions',
            'validation' => array()
        ),
        'more_info_link' => array(
            'label' => 'Promo More Info Link',
            'validation' => array(
                'max_length' => array(500),
            )
        ),
        'more_info_banner' => array(
            'label' => 'Promo More Info banner',
            'validation' => array(
                'max_length' => array(200),
            )
        ),
        'status' => array(
            'label' => 'Status',
            'validation' => array(
                'required',
            )
        ),
        'post_date' => array(
            'label' => 'Post Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'start_date' => array(
            'label' => 'Start Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'end_date' => array(
            'label' => 'End Date',
            'validation' => array(
                'required',
                'valid_date' => array(
                    'format' => 'Y-m-d'
                )
            )
        ),
        'created_by',
        'created_at',
        'updated_by',
        'updated_at'
    );
    protected static $_belongs_to = array(
        'banners' => array(
            'key_from' => 'banner_id',
            'model_to' => '\promo\Model_PromoBanners',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        ),
        'images' => array(
            'key_from' => 'image_id',
            'model_to' => '\promo\Model_PromoImages',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
    );
    
    private $more_info_banner_path = 'media/promo/';
    
    public function get_more_info_banner_path() {
        return $this->more_info_banner_path;
    }

    public function get_all_more_info_banners() {
        if (file_exists(DOCROOT . $this->more_info_banner_path)) {
            $contents = \File::read_dir(DOCROOT . $this->more_info_banner_path);
        } else {
            $contents = array();
        }
        return $contents;
    }
    
    private static $_banners, $_images;

    public function get_flag_name() {
        $flag = $this->flag;
        return isset($this->flag_status[$flag]) ? $this->flag_status[$flag] : '-';
    }
        
    public function get_status_name() {
        $flag = $this->status;
        return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
    }

    public static function get_as_array($filter = array()) {
        $items = self::find('all', $filter);
        if (empty($items)) {
            $data = array();
        } else {
            foreach ($items as $item) {
                $data[$item->id] = $item->title;
            }
        }
        return $data;
    }

    public function get_banner_name() {
        if (empty(self::$_banners)) {
            self::$_banners = Model_PromoBanners::get_as_array();
        }
        $flag = $this->banner_id;
        return isset(self::$_banners[$flag]) ? self::$_banners[$flag] : '-';
    }

    public function get_image_name() {
        if (empty(self::$_images)) {
            self::$_images = Model_PromoImages::get_as_array();
        }
        $flag = $this->image_id;
        return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
    }

    public function get_form_data_basic($banner, $image) {
        return array(
            'attributes' => array(
                'name' => 'frm_promos',
                'class' => 'form-horizontal',
                'role' => 'form',
                'action' => '',
                'method' => 'post',
            ),
            'hidden' => array(),
            'fieldset' => array(
                array(
                    'label' => array(
                        'label' => 'Banner',
                        'id' => 'banner_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'banner_id',
                        'value' => $this->banner_id,
                        'options' => $banner,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Banner',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Image',
                        'id' => 'image_id',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'image_id',
                        'value' => $this->image_id,
                        'options' => $image,
                        'attributes' => array(
                            'class' => 'form-control bootstrap-select',
                            'placeholder' => 'Image',
                            'data-live-search' => 'true',
                            'data-size' => '3',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Title',
                        'id' => 'promo_title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'promo_title',
                        'value' => $this->title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Meta Title',
                        'id' => 'meta_title',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'meta_title',
                        'value' => $this->meta_title,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Meta Title',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Meta Description',
                        'id' => 'meta_desc',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'meta_desc',
                        'value' => $this->meta_desc,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Meta Description',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Flag',
                        'id' => 'flag',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'flag',
                        'value' => $this->flag,
                        'options' => $this->flag_status,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Flag Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Slug',
                        'id' => 'slug',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'slug',
                        'value' => $this->slug,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Slug',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Post Date',
                        'id' => 'post_date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'post_date',
                        'value' => $this->post_date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Post Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Start Date',
                        'id' => 'start_date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'start_date',
                        'value' => $this->start_date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'Start Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'End Date',
                        'id' => 'end_date',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'end_date',
                        'value' => $this->end_date,
                        'attributes' => array(
                            'class' => 'form-control mask-date',
                            'placeholder' => 'End Date',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Status',
                        'id' => 'status',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select' => array(
                        'name' => 'status',
                        'value' => $this->status,
                        'options' => $this->status_name,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Status',
                            'required' => ''
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Teaser',
                        'id' => 'teaser',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'teaser',
                        'value' => $this->teaser,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'Teaser',
                            'required' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Promo Content',
                        'id' => 'promo_content',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'promo_content',
                        'value' => $this->content,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'Promo Terms and Conditions',
                        'id' => 'promo_terms',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'textarea' => array(
                        'name' => 'promo_terms',
                        'value' => $this->terms,
                        'attributes' => array(
                            'class' => 'form-control ckeditor',
                            'placeholder' => '',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'More Info Link',
                        'id' => 'promo_more_info_link',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'input' => array(
                        'name' => 'promo_more_info_link',
                        'value' => $this->more_info_link,
                        'attributes' => array(
                            'class' => 'form-control',
                            'placeholder' => 'More Info Link',
                        ),
                        'container_class' => 'col-sm-10'
                    )
                ),
                array(
                    'label' => array(
                        'label' => 'More Info Banner',
                        'id' => 'promo_more_info_banner',
                        'attributes' => array(
                            'class' => 'col-sm-2 control-label'
                        )
                    ),
                    'select_image_picker' => array(
                        'name' => 'promo_more_info_banner',
                        'value' => $this->more_info_banner,
                        'options' => $this->get_all_more_info_banners(),
                        'attributes' => array(
                            'class' => 'form-control image-picker',
                        ),
                        'container_class' => 'col-sm-10',
                        'image_url' => \Uri::base() . $this->more_info_banner_path,
                    )
                ),
            )
        );
    }

}
