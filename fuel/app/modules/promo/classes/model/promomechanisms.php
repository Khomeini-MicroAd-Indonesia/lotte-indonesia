<?php
namespace Promo;

class Model_PromoMechanisms extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	private $image_path = 'media/promo/';
	
	protected static $_table_name = 'promo_mechanisms';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'promo'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'promo'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'promo'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'step' => array(
			'label' => 'Step',
			'validation' => array(
				'required',
			)
		),
                'slug' => array(
                        'label' => 'Mechanism Slug',
                        'validation' => array(
                                'required',
                                'max_length' => array(100),
                        )
                ),
                'promo_id' => array(
			'label' => 'Promo',
			'validation' => array(
                'required',
			)
		),
                'image_id' => array(
			'label' => 'Image',
			'validation' => array(
				'required',
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
                'desc' => array(
                        'label' => 'Mechanism Desc',
                        'validation' => array()
                ),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
        

        
        protected static $_belongs_to = array(
            'promos' => array(
                'key_from' => 'promo_id',
                'model_to' => '\promo\Model_Promos',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'images' => array(
                'key_from' => 'image_id',
                'model_to' => '\promo\Model_PromoImages',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        
        );
	
        private static $_promos;
        private static $_images;
        
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_promo_name(){
            if(empty(self::$_promos)){
                self::$_promos = Model_Promos::get_as_array();
            }
            $flag = $this->promo_id;
            return isset(self::$_promos[$flag]) ? self::$_promos[$flag] : '-';
        }
        
        public function get_image_name(){
            if(empty(self::$_images)){
                self::$_images = Model_PromoImages::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
        
        public function get_all_images() {
		if (file_exists(DOCROOT.$this->image_path)) {
			$contents = \File::read_dir(DOCROOT.$this->image_path);
		} else {
			$contents = array();
                        //$contents = "http://localhost:8888/MicroAd/tokyuland/assets/img/pdf-icon.png";
		}
		return $contents;
	}
        
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->title;
                }
            }
            return $data;
	}
	
	public function get_form_data_basic($promo, $image) {
            return array(
			'attributes' => array(
				'name' => 'frm_promo_mechanism',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                                array(
					'label' => array(
                                            'label' => 'Step',
                                            'id' => 'step',
                                            'attributes' => array(
                                                'class' => 'col-sm-2 control-label'
                                            )
					),
					'input' => array(
                                            'name' => 'step',
                                            'value' => $this->step,
                                            'attributes' => array(
                                                'class' => 'form-control',
                                                'placeholder' => '0',
                                                'required' => '',
                                            ),
                                            'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
                                            'label' => 'Slug',
                                            'id' => 'slug',
                                            'attributes' => array(
                                                'class' => 'col-sm-2 control-label'
                                            )
					),
					'input' => array(
                                            'name' => 'slug',
                                            'value' => $this->slug,
                                            'attributes' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Slug',
                                                'required' => '',
                                            ),
                                            'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
                                            'label' => 'Promo Name',
                                            'id' => 'promo_name',
                                            'attributes' => array(
                                                'class' => 'col-sm-2 control-label'
                                            )
					),
					'select' => array(
                                            'name' => 'promo_name',
                                            'value' => $this->promo_id,
                                            'options' => $promo,
                                            'attributes' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Promo',
                                                'data-live-search' => 'true',
                                                'data-size' => '3',
                                            ),
                                            'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Image Name',
						'id' => 'image_name',
						'attributes' => array(
                                                    'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
                                            'name' => 'image_name',
                                            'value' => $this->image_id,
                                            'options' => $image,
                                            'attributes' => array(
                                                'class' => 'form-control',
                                                'placeholder' => 'Image Name',
                                                'data-live-search' => 'true',
                                            ),
                                            'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Status',
                                                    'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Seq',
						'id' => 'mechanism_seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'mechanism_seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '0',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Mechanism Desc',
						'id' => 'mechanism_desc',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'mechanism_desc',
						'value' => $this->desc,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
