<?php
namespace Promo;

class Controller_Backend_Mechanism extends \Controller_Backend
{
	private $_module_url = 'backend/promo/mechanism';
	private $_menu_key = 'promo_mechanism';
	
	public function before() {
		parent::before();
		$this->authenticate();
		// Check menu permission
		if (!$this->check_menu_permission($this->_menu_key, 'read')) {
			// if not have an access then redirect to error page
			\Response::redirect(\Uri::base().'backend/no-permission');
		}
		$this->_data_template['meta_title'] = 'Promo Mechanisms';
		$this->_data_template['menu_parent_key'] = 'promo_management';
		$this->_data_template['menu_current_key'] = 'promo_mechanism';
	}
	
	public function action_index() {
		$this->_data_template['mechanism_list'] = Model_PromoMechanisms::find('all');
		$this->_data_template['success_message'] = \Session::get_flash('success_message');
		$this->_data_template['error_message'] = \Session::get_flash('error_message');
                
		return \Response::forge(\View::forge('promo::backend/list_mechanism.twig', $this->_data_template, FALSE));
	}
	
	public function action_form($id) {
		// find model by id
		$the_model = Model_PromoMechanisms::find($id);
		// if empty then define empty model
		if (empty($the_model)) {
			// The ID "0" is only for add new thing, if greater than 0 then its mean edit thing
			if ($id > 0) {
				\Session::set_flash('error_message', 'The Mechanism with ID "'.$id.'" is not found here');
				\Response::redirect(\Uri::base().$this->_module_url);
			}
			$the_model = Model_PromoMechanisms::forge();
		}
		$this->_save_setting_data($the_model);
		$this->_data_template['content_header'] = 'Promo Mechanisms';
		$this->_data_template['content_subheader'] = 'Form';
		$this->_data_template['breadcrumbs'] = array(
			array(
				'label' => 'Promo Mechanisms',
				'link' => \Uri::base().$this->_module_url
			),
			array(
				'label' => 'Form'
			)
		);
                $promo_data = Model_Promos::get_as_array();
                $image_data = Model_PromoImages::get_as_array();
		$this->_data_template['form_data'] = $the_model->get_form_data_basic($promo_data, $image_data);
		$this->_data_template['cancel_button_link'] = \Uri::base().$this->_module_url;
		$this->_data_template['success_message'] = \Session::get_flash('success_message');
		return \Response::forge(\View::forge('backend/form/basic.twig', $this->_data_template, FALSE));
	}
	
	private function _save_setting_data($the_model) {
		$all_post_input = \Input::post();
		if (count($all_post_input)) {
			// Check menu permission
			$access_name = ($the_model->id > 0) ? 'update' : 'create';
			if (!$this->check_menu_permission($this->_menu_key, $access_name)) {
				// if not have an access then redirect to error no permission page
				\Response::redirect(\Uri::base().'backend/no-permission');
			}
                        $the_model->step = $all_post_input['step'];
                        $the_model->slug = $all_post_input['slug'];
                        $the_model->promo_id = $all_post_input['promo_name'];//isset($all_post_input['promo_id']) ? $all_post_input['promo_id'] : null;
                        $the_model->image_id = $all_post_input['image_name'];
			$the_model->status = $all_post_input['status'];
			$the_model->seq = $all_post_input['mechanism_seq'];
                        $the_model->desc = $all_post_input['mechanism_desc'];
			// Set created_by/updated_by
			if ($the_model->id > 0) {
				$the_model->updated_by = $this->admin_auth->getCurrentAdmin()->id;
				$is_edit = true;
			} else {
				$the_model->created_by = $this->admin_auth->getCurrentAdmin()->id;
				$is_edit = false;
			}
			
			// Save with validation, if error then throw the error
			try {
				$the_model->save();
				\Session::set_flash('success_message', 'Successfully Saved');
				\Response::redirect(\Uri::current());
			} catch (\Orm\ValidationFailed $e) {
				$this->_data_template['error_message'] = $e->getMessage();
			}
		}
	}
	
	public function action_delete($id) {
		// Check menu permission
		if (!$this->check_menu_permission($this->_menu_key, 'delete')) {
			// if not have an access then redirect to no permission page
			\Response::redirect(\Uri::base().'backend/no-permission');
		}
		// find model by id
		$the_model = Model_PromoMechanisms::find($id);
		// if empty then redirect back with error message
		if (empty($the_model)) {
			\Session::set_flash('error_message', 'The Mechanism with ID "'.$id.'" is not found here');
			\Response::redirect(\Uri::base().$this->_module_url);
			exit;
		}
		// Delete the admin
		try {
			$the_model->delete();
			\Session::set_flash('success_message', 'Delete The Mechanism "'.$the_model->promo_id.'" with ID "'.$id.'" is successfully');
		} catch (Orm\ValidationFailed $e) {
			\Session::set_flash('error_message', $e->getMessage());
		}
		\Response::redirect(\Uri::base().$this->_module_url);
	}
	
	
}

