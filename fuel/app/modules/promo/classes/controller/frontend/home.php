<?php
namespace Promo;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'promo';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }

    public function action_index() {
        
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/promo');
        $this->_data_template['promo_image'] = \Promo\Promoutil::get_promo_images();
        return \Response::forge(\View::forge('promo::frontend/promo.twig', $this->_data_template, FALSE));
        
    }
    
    public function action_detail() {
        $slug = $this->param('slug');
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/detail');
        $promo_detail = \Promo\Promoutil::get_promo_details($slug);
        $this->_data_template['promo_detail'] = $promo_detail;
        if (strlen($promo_detail[0]['meta_title']) > 0) {
            $this->_data_template['meta_title'] .= ' - ' . $promo_detail[0]['meta_title'];
        }
        $this->_data_template['meta_desc'] = $promo_detail[0]['meta_desc'];
        $this->_data_template['promo_image'] = \Promo\Promoutil::get_promo_images();
        $this->_data_template['promo_list'] = \Promo\Promoutil::get_promo_lists($slug);
        $this->_data_template['promo_prize'] = \Promo\Promoutil::get_promo_prizes($slug);
        $this->_data_template['promo_mechanism'] = \Promo\Promoutil::get_promo_mechanisms($slug);
        return \Response::forge(\View::forge('promo::frontend/detail.twig', $this->_data_template, FALSE));
        
    }

}
