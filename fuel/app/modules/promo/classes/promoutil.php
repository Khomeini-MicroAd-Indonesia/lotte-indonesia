<?php
namespace Promo;

class Promoutil{
    
    /**
     * get promo image
     */
    
    public static function get_promo_images(){
        
        $images = \Promo\Model_Promos::query()
                ->related('banners')
                ->related('images')
                ->where('status', 1)
                ->get();
                
        $data = array();
        
        foreach ($images as $image){
            
            $data[] = array(
                'id'        => $image->id,
                'title'     => $image->title,
                'slug'      => $image->slug,
                'content'   => $image->content,
                'date'      => $image->post_date,
                'banner'    => $image->banners->filename,
                'image'     => $image->images->filename
            );
        }
        return $data;
    }
    
    /**
     * get promo detail: active
     */
    
    public static function get_promo_details($slug){
        
        $details = \Promo\Model_Promos::query()
                ->related('banners')
                ->related('images')
                ->where('status', 1)
                ->where('slug', $slug)
                ->get();
                
        $data = array();
        
        foreach ($details as $detail){
            if (date('m-Y', strtotime($detail->start_date)) == date('m-Y', strtotime($detail->end_date))) {
                $_startdate = date('d', strtotime($detail->start_date));
            } else if (date('Y', strtotime($detail->start_date)) == date('Y', strtotime($detail->end_date))) {
                $_startdate = date('d F', strtotime($detail->start_date));
            } else {
                $_startdate = date('d F Y', strtotime($detail->start_date));
            }
            
            $data[] = array(
                'id'        => $detail->id,
                'title'     => $detail->title,
                'slug'      => $detail->slug,
                'content'   => $detail->content,
                'terms'   => $detail->terms,
                'more_info_link'   => $detail->more_info_link,
                'more_info_banner'   => $detail->more_info_banner,
                'date'      => $detail->post_date,
                'start'     => $_startdate,
                'end'       => date('d F Y', strtotime($detail->end_date)),
                'banner'    => $detail->banners->filename,
                'image'     => $detail->images->filename,
                'meta_title'=> $detail->meta_title,
                'meta_desc' => $detail->meta_desc,
            );
        }
        return $data;
    }
    
    /**
     * get promo list
     */
    
    public static function get_promo_lists($slug){
        
        $lists = \Promo\Model_Promos::query()
                ->related('banners')
                ->related('images')
                ->where('status', 1)
                ->where('slug', '!=', $slug)
                ->get();
                
        $data = array();
        
        foreach ($lists as $list){
            
            $data[] = array(
                'title'     => $list->title,
                'slug'     => $list->slug
            );
        }
        return $data;
    }
    
    /**
     * get promo prize
     */
    
    public static function get_promo_prizes($slug){
        
        $prizes = \Promo\Model_PromoPrizes::query()
                ->related('promos')
                ->related('images')
                ->where('slug', $slug)
                ->where('status', 1)
                ->get();
                
        $data = array();
        
        foreach ($prizes as $prize){
            
            $data[] = array(
                'image'     => $prize->images->filename,
                'desc'      => $prize->desc,
            );
        }
        return $data;
    }
    
    /**
     * get promo mechanism
     */
    
    public static function get_promo_mechanisms($slug){
        
        $mechanisms = \Promo\Model_PromoMechanisms::query()
                ->related('promos')
                ->related('images')
                ->where('slug', $slug)
                ->where('status', 1)
                ->order_by('seq')
                ->get();
                
        $data = array();
        
        foreach ($mechanisms as $mechanism){
            
            $data[] = array(
                'step'      => $mechanism->step,
                'image'     => $mechanism->images->filename,
                'desc'      => $mechanism->desc,
            );
        }
        return $data;
    }
    
    
    /**
     * get promo preview
     */
    
    public static function get_promo_preview(){
        
        $promos = \Promo\Model_Promos::query()
                ->related('images')
                ->where('status', 1)
                ->order_by('flag', 'DESC')
                ->order_by('post_date', 'DESC')
                ->limit(4)
                ->get();
                
        $data = array();
        
        foreach ($promos as $promo){
            
            $data[] = array(
                'type'      => 'promo',
                'slug'      => $promo->slug,
                'title'     => $promo->title,
                'teaser'    => $promo->teaser,
                'image'     => $promo->images->filename,
                'post_date' => $promo->post_date,
                'flag'      => $promo->flag,
            );
        }
        return $data;
    }
    
}