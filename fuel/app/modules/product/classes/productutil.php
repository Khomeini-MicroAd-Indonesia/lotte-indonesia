<?php

namespace Product;

use Twitter\TwitterAPIExchange;

class Productutil {

    /**
     * get product data
     */
    public static function get_product_details($slug) {

        $details = \Product\Model_Products::query()
                ->related('banners')
                ->where('slug', $slug)
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title' => $detail->title,
                'meta_title' => $detail->meta_title,
                'meta_desc' => $detail->meta_desc,
                'banner' => $detail->banners->filename
            );
        }
        return $data;
    }

    /*
     * get product variant
     */

    public static function get_product_variant($slug) {

        $details = \Product\Model_ProductVariants::query()
                ->related('images')
                ->where('status', 1)
                ->where('slug', $slug)
                ->order_by('seq')
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title' => $detail->title,
                'desc' => $detail->desc,
                'image' => $detail->images->filename
            );
        }
        return $data;
    }

    /*
     * get product tvc
     */

    public static function get_product_tvc($slug) {

        $details = \Product\Model_ProductTVCs::query()
                ->where('status', 1)
                ->where('slug', $slug)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title' => $detail->name,
                'desc' => $detail->desc,
                'link' => $detail->link,
                'tvc' => $detail->filename
            );
        }
        return $data;
    }

    /*
     * get product socmed
     */

    public static function get_product_socmed($slug) {

        $details = \Product\Model_ProductSocmeds::query()
                ->where('slug', $slug)
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title' => $detail->title,
                'date' => date('l, d F Y', strtotime($detail->post_date)),
                'type' => $detail->type,
                'link' => $detail->link,
                'content' => $detail->content
            );
        }
        return $data;
    }

    public static function get_facebook_data($slug) {
        $data = null;
        $model = \Product\Model_ProductSocmeds::query()
                ->where('slug', $slug)
                ->where('type', 'FACEBOOK')
                ->where('status', 1)
                ->get_one();
        if (!empty($model)) {
            $page_id = $model->link;
            $app_id = \Config::get('facebook.app_id');
            $client_secret = \Config::get('facebook.secret_id');
            $response_access_token = @file_get_contents('https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id=' . $app_id . '&client_secret=' . $client_secret);
            if (!empty($response_access_token)) {
                $temp = explode('=', $response_access_token);
                $access_token = $temp[1];
                $json_object = @file_get_contents('https://graph.facebook.com/' . $page_id . '?access_token=' . $access_token . '&fields=name,likes,posts.limit(3)');
                $data = json_decode($json_object, true);
            }
        }
        return $data;
    }

    private static function _twitter_perform_request($url, $getField, $requestMethod) {
        $settings = \Config::get('twitter');
        $twitter = new TwitterAPIExchange($settings);
        $response = $twitter->setGetfield($getField)
                ->buildOauth($url, $requestMethod)
                ->performRequest();
        return $response;
    }

    public static function get_twitter_data($slug) {
        $data = null;
        $model = \Product\Model_ProductSocmeds::query()
                ->where('slug', $slug)
                ->where('type', 'TWITTER')
                ->where('status', 1)
                ->get_one();
        if (!empty($model)) {
            $page_id = $model->link;
            $getField = '?screen_name='.$page_id;
            $requestMethod = 'GET';
            $responseTimeline = self::_twitter_perform_request('https://api.twitter.com/1.1/statuses/user_timeline.json', $getField.'&count=3', $requestMethod);
            $data['timeline'] = json_decode($responseTimeline, true);
            $responseUser = self::_twitter_perform_request('https://api.twitter.com/1.1/users/show.json', $getField, $requestMethod);
            $data['user'] = json_decode($responseUser, true);
        }
        return $data;
    }

    /*
     * get product socmed
     */

    public static function get_product_instagram($slug) {

        $details = \Product\Model_ProductInstagrams::query()
                ->related('images')
                ->where('slug', $slug)
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[] = array(
                'title' => $detail->title,
                'date' => date('l, d F Y', strtotime($detail->post_date)),
                'link' => $detail->link,
                'content' => $detail->content,
                'image' => $detail->images->filename
            );
        }
        return $data;
    }

    /*
     * get home product
     */

    public static function get_home_product() {

        $brands = \Product\Model_ProductImages::query()
                //->related('images')
                ->where('name', 'like', 'home%')
                ->where('status', 1)
                ->order_by('seq')
                ->order_by('name')
                ->get();

        $data = array();

        foreach ($brands as $brand) {

            $data[] = array(
                'name' => $brand->name,
                'image' => $brand->filename,
                'link' => $brand->link
            );
        }
        return $data;
    }
    
    /**
     * get product overview
     */
    
    public static function get_product_overview($slug){
        
        $banners = \Homebanner\Model_Homebanners::query()
                ->related('images')
                ->related('logos')
                ->where('slug', $slug)
                ->where('status', 1)
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($banners as $banner){
            $temp = 'content_'.\Config::get('language');
            $data[] = array(
                'slug'        => $banner->slug,
                'content'     => $banner->$temp 
            );
        }
        return $data;
    }

}
