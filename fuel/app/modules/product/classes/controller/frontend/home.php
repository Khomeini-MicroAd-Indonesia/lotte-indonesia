<?php
namespace Product;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'product';
    private $_meta_slug = '/';

    public function before() {
        parent::before();
    }
    
//    public function after($response) {
//        
//        parent::after($response);
//        $slug = $this->param('slug');
//        $this->_data_template['product_socmed_facebook']= \Product\Productutil::get_facebook_data($slug);
//        $this->_data_template['product_socmed_twitter']= \Product\Productutil::get_twitter_data($slug);
//        $this->_data_template['product_instagram']= \Product\Productutil::get_product_instagram($slug);
//        
//        return $response;
//    }


    public function action_detail() {
        $slug = $this->param('slug');
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/product-details');
        $product_detail = \Product\Productutil::get_product_details($slug);
        //var_dump($product_detail); exit;
        if (strlen($product_detail[0]['meta_title']) > 0) {
            $this->_data_template['meta_title'] .= ' - ' . $product_detail[0]['meta_title'];
        }
        $this->_data_template['meta_desc'] = $product_detail[0]['meta_desc'];
        $this->_data_template['product_overview'] = \Product\Productutil::get_product_overview($slug);
        //if(isset($product_detail)){
          $this->_data_template['product_detail'] = $product_detail;  
        //}
        $this->_data_template['product_variant']= \Product\Productutil::get_product_variant($slug);
        $this->_data_template['product_tvc']= \Product\Productutil::get_product_tvc($slug);
        $this->_data_template['product_slug']= $slug;
        $this->_data_template['product_instagram']= \Product\Productutil::get_product_instagram($slug);

        return \Response::forge(\View::forge('product::frontend/product_details.twig', $this->_data_template, FALSE));
    }
    
    public function action_get_socmed() {
        $slug = \Input::post('slug');
        $data_fb = \Product\Productutil::get_facebook_data($slug);
        $data_twitter = \Product\Productutil::get_twitter_data($slug);
        //\Log::debug(json_encode($data_fb));
        //\Log::debug(json_encode($data_twitter));
        $html = '';
        if (count($data_fb['posts']['data']) > 0 || count($data_twitter['timeline'])) {
            $html = '
                <div class="pre-title-center">
                    <p class="title">SOCIAL MEDIA</p>
                    <div style="clear: both"></div>
                </div>';
            
            if (count($data_fb['posts']['data']) > 0) {
                $html .= '
                <div class="large-6 medium-6 small-12 columns socmed-section '. (count($data_twitter['timeline']) <= 0 ? 'small-centered' : '').'">
                    <p class="socmed-kind">FACEBOOK<span class="right">'.$data_fb['name'].'</span></p>
                    <div class="description-socmed">
                        <div class="main facebook-main">';
                
                $idx = 0;
                foreach ($data_fb['posts']['data'] as $fb_data_item) {
                    if (isset($fb_data_item['message'])) {
                        $tmp = '';
                        if ($idx == 0) {
                            $tmp = 'first current';
                        }
                        if ($idx == count($data_fb['posts']['data'])-1) {
                            $tmp = 'last';
                        }
                        $html .= '<div class="'.$tmp.'">
                                        <p class="date"> ('.date('l, d F Y', strtotime($fb_data_item['created_time'])).')</p>
                                        <p>'.$fb_data_item['message'].'</p>
                                    </div>';
                        $idx++;
                    }
                }
                
                $html .= '
                        </div>
                        <ul class="large-block-grid-2 medium-block-grid-2 small-block-grid-2">
                            <li>
                                <div class="">
                                    <a class="button-socmed" href="https://www.facebook.com/'.$data_fb['id'].'" target="_blank">Like us on FACEBOOK</a>
                                    <div style="clear: both"></div>
                                </div>
                            </li>
                            <li>
                                <div class="socmed-button-play">
                                    <a class="socmed-play back disabled" id="facebook-back"></a>
                                    <a class="socmed-play next" id="facebook-next"></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>';
            }
            
            if (count($data_twitter['timeline']) > 0) {
                $html .= '
                <div class="large-6 medium-6 small-12 columns socmed-section '. (count($data_fb['posts']['data']) <= 0 ? 'small-centered' : '').'">
                    <p class="socmed-kind">TWITTER<span class="right">@'.$data_twitter['user']['screen_name'].'</span></p>
                    <div class="description-socmed">
                        <div class="main twitter-main">';
                
                $idx = 0;
                foreach ($data_twitter['timeline'] as $twitter_data_item) {
                    $tmp = '';
                    if ($idx == 0) {
                        $tmp = 'first current';
                    }
                    if ($idx == count($data_twitter['timeline'])-1) {
                        $tmp = 'last';
                    }
                    $html .= '<div class="'.$tmp.'">
                                    <p class="date"> ('.date('l, d F Y', strtotime($twitter_data_item['created_at'])).')</p>
                                    <p>'.$twitter_data_item['text'].'</p>
                                </div>';
                    $idx++;
                }
                
                $html .= '
                        </div>
                        <ul class="large-block-grid-2 medium-block-grid-2 small-block-grid-2">
                            <li>
                                <div class="">
                                    <a class="button-socmed" href="https://www.twitter.com/'.$data_twitter['user']['screen_name'].'" target="_blank">Like us on TWITTER</a>
                                    <div style="clear: both"></div>
                                </div>
                            </li>
                            <li>
                                <div class="socmed-button-play">
                                    <a class="socmed-play back disabled" id="twitter-back"></a>
                                    <a class="socmed-play next" id="twitter-next"></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>';
            }
        }
        return $html;
    }
        
    public function action_other() {
        $this->set_meta_info($this->_meta_slug.$this->_current_lang.'/product-others');
        $slug = $this->param('slug');
        
        $this->_data_template['product_overview'] = \Product\Productutil::get_product_overview('lotte-other');
        $this->_data_template['banner'] = \Product\Otherutil::get_banner('product-others');
        $this->_data_template['other_detail'] = \Product\Otherutil::get_other_detail();
        
        return \Response::forge(\View::forge('product::frontend/product_others.twig', $this->_data_template, FALSE));
    }

}
