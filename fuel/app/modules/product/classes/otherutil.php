<?php

namespace Product;

class Otherutil {

    /**
     * get banner
     */
    public static function get_banner($slug) {
        
        $details = \Product\Model_Products::query()
                ->related('banners')
                ->where('slug', $slug)
                ->where('status', 1)
                ->get();
                
        $data = array();
        
        foreach ($details as $detail){
            
            $data[] = array(
                'title'     => $detail->title,
                'banner'    => $detail->banners->filename
            );
        }
        return $data;
    }

    /**
     * get other product detail
     */
    public static function get_otherproduct_detail() {

        $details = \Product\Model_OtherProducts::query()
                ->related('others')
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[$detail->others->name][] = array(
                'other_name' => $detail->others->name,
                'id' => $detail->id,
                'title' => $detail->title
            );
        }
        return $data;
    }

    /**
     * get variant detail
     */
    public static function get_variant_detail() {

        $details = \Product\Model_OtherVariants::query()
                ->related('otherproducts')
                ->related('images')
                ->where('status', 1)
                ->get();

        $data = array();

        foreach ($details as $detail) {

            $data[$detail->otherproducts->id][] = array(
                'other' => $detail->otherproducts->others->name,
                'otherproduct_id' => $detail->otherproduct_id,
                'title' => $detail->title,
                'desc' => $detail->desc,
                'image' => $detail->images->filename
            );
        }
        return $data;
    }

    /**
     * get other detail
     */
    public static function get_other_detail() {
        $details = \Product\Model_Others::query()
                ->related('otherproducts')
                ->related('otherproducts.othervariants')
                ->related('otherproducts.othervariants.images')
                ->where('status', 1)
                ->where('otherproducts.status', 1)
//<<<<<<< HEAD
//=======
                ->where('otherproducts.othervariants.status', 1)
//>>>>>>> f6f49fd7c85267489d2431ff2cc1a8a4fe528cb7
                ->order_by('seq')
                ->order_by('otherproducts.seq')
                ->order_by('otherproducts.othervariants.seq')
                ->get();
        return $details;
    }

}
