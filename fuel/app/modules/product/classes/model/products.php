<?php
namespace Product;

class Model_Products extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'products';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'products'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'products'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'products'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'banner_id' => array(
                    'label' => 'Banner',
			'validation' => array(
				'required',
			)
                ),
                'tvc_id' => array(
			'label' => 'TVC',
			'validation' => array(
				'required',
			)
		),
                'slug' => array(
                    'label' => 'Product Slug',
                    'validation' => array(
                            'required',
                            'max_length' => array(100),
                    )
		),
		'title' => array(
			'label' => 'Product Title',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'meta_title' => array(
			'label' => 'Meta Title',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'meta_desc' => array(
			'label' => 'Meta Description',
			'validation' => array(
				'required',
				'max_length' => array(150),
			)
		),
                'content' => array(
                        'label' => 'Product Content',
                        'validation' => array()
                ),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
                'post_date' => array(
			'label' => 'Post Date',
			'validation' => array(
				'required',
				'valid_date' => array(
					'format' => 'Y-m-d'
				)
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
        protected static $_belongs_to = array(
            'banners' => array(
                'key_from' => 'banner_id',
                'model_to' => '\product\Model_productBanners',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'tvcs' => array(
                'key_from' => 'tvc_id',
                'model_to' => '\product\Model_productTVCs',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        
        );
        
        private static $_banners, $_tvcs;
        
	public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->title;
                }
            }
            return $data;
	}
        
        public function get_banner_name(){
            if(empty(self::$_banners)){
                self::$_banners = Model_ProductBanners::get_as_array();
            }
            $flag = $this->banner_id;
            return isset(self::$_banners[$flag]) ? self::$_banners[$flag] : '-';
        }
        
        public function get_tvc_name(){
            if(empty(self::$_tvcs)){
                self::$_tvcs = Model_ProductTVCs::get_as_array();
            }
            $flag = $this->tvc_id;
            return isset(self::$_tvcs[$flag]) ? self::$_tvcs[$flag] : '-';
        }
        
	public function get_form_data_basic($banner, $tvc) {
		return array(
			'attributes' => array(
				'name' => 'frm_products',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                                array(
					'label' => array(
						'label' => 'Banner',
						'id' => 'banner_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'banner_id',
						'value' => $this->banner_id,
						'options' => $banner,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Banner',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'TVC',
						'id' => 'tvc_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'tvc_id',
						'value' => $this->tvc_id,
						'options' => $tvc,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'TVC',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Title',
						'id' => 'product_title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'product_title',
						'value' => $this->title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                            
                            array(
                                    'label' => array(
                                            'label' => 'Meta Title',
                                            'id' => 'meta_title',
                                            'attributes' => array(
                                                    'class' => 'col-sm-2 control-label'
                                            )
                                    ),
                                    'input' => array(
                                            'name' => 'meta_title',
                                            'value' => $this->meta_title,
                                            'attributes' => array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Meta Title',
                                                    'required' => '',
                                            ),
                                            'container_class' => 'col-sm-10'
                                    )
                            ),
                            array(
                                    'label' => array(
                                            'label' => 'Meta Description',
                                            'id' => 'meta_desc',
                                            'attributes' => array(
                                                    'class' => 'col-sm-2 control-label'
                                            )
                                    ),
                                    'textarea' => array(
                                            'name' => 'meta_desc',
                                            'value' => $this->meta_desc,
                                            'attributes' => array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Meta Description',
                                                    'required' => '',
                                            ),
                                            'container_class' => 'col-sm-10'
                                    )
                            ),
                            
                                array(
					'label' => array(
						'label' => 'Slug',
						'id' => 'slug',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'slug',
						'value' => $this->slug,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Slug',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Post Date',
						'id' => 'post_date',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'post_date',
						'value' => $this->post_date,
						'attributes' => array(
							'class' => 'form-control mask-date',
							'placeholder' => 'Post Date',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Product Content',
						'id' => 'product_content',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'product_content',
						'value' => $this->content,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
