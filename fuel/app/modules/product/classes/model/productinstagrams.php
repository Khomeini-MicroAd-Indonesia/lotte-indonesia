<?php
namespace Product;

class Model_ProductInstagrams extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'product_instagrams';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'product'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'product'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'product'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'image_id' => array(
			'label' => 'Image',
			'validation' => array(
				'required',
			)
		),
                'product_id' => array(
			'label' => 'Product',
			'validation' => array(
				'required',
			)
		),
                'slug' => array(
                        'label' => 'Instagram Slug',
                        'validation' => array(
                                'required',
                                'max_length' => array(100),
                        )
                ),
                'title' => array(
			'label' => 'Instagram Title',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'link' => array(
                        'label' => 'Instagram Link',
                        'validation' => array(
                                'required',
                                'max_length' => array(100),
                        )
                ),
                'content' => array(
                        'label' => 'Instagram Content',
                        'validation' => array()
                ),
                'post_date' => array(
			'label' => 'Post Date',
			'validation' => array(
				'required',
				'valid_date' => array(
					'format' => 'Y-m-d'
				)
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
        
        protected static $_belongs_to = array(
            'products' => array(
                'key_from' => 'product_id',
                'model_to' => '\product\Model_Products',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'images' => array(
                'key_from' => 'image_id',
                'model_to' => '\product\Model_ProductImages',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        
        );
	
        private static $_products, $_images;
        
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_product_name(){
            if(empty(self::$_products)){
                self::$_products = Model_Products::get_as_array();
            }
            $flag = $this->product_id;
            return isset(self::$_products[$flag]) ? self::$_products[$flag] : '-';
        }
       
        public function get_image_name(){
            if(empty(self::$_images)){
                self::$_images = Model_ProductImages::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
        
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->title;
                }
            }
            return $data;
	}
	
	public function get_form_data_basic($product, $image) {
            return array(
			'attributes' => array(
				'name' => 'frm_product_instagram',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                                array(
					'label' => array(
						'label' => 'Image',
						'id' => 'image_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'image_id',
						'value' => $this->image_id,
						'options' => $image,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Image',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Product Name',
						'id' => 'product_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'product_name',
						'value' => $product,//$this->name,
                                                'options' => $product,
						'attributes' => array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Product',
                                                    'data-live-search' => 'true',
                                                    'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Title',
						'id' => 'instagram_title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'instagram_title',
						'value' => $this->title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Link',
						'id' => 'instagram_link',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'instagram_link',
						'value' => $this->link,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Link',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Slug',
						'id' => 'slug',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'slug',
						'value' => $this->slug,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Slug',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Post Date',
						'id' => 'post_date',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'post_date',
						'value' => $this->post_date,
						'attributes' => array(
							'class' => 'form-control mask-date',
							'placeholder' => 'Post Date',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Status',
                                                    'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Seq',
						'id' => 'instagram_seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'instagram_seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '0',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Instagram Content',
						'id' => 'instagram_content',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'instagram_content',
						'value' => $this->content,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
			)
		);
	}
}
