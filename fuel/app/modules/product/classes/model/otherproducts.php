<?php
namespace Product;

class Model_OtherProducts extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'otherproducts';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'product'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'product'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'product'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'other_id' => array(
                    'label' => 'Other',
                    'validation' => array(
                            'required',
                    )
		),
                'title' => array(
			'label' => 'Other Product Title',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
        
//        protected static $_belongs_to = array(
//            
//            'others' => array(
//                'key_from' => 'other_id',
//                'model_to' => '\product\Model_Others',
//                'key_to' => 'id',
//                'cascade_save' => true,
//                'cascade_delete' => false,
//            )
//        
//        );
        
    protected static $_has_many = array(
        
        'othervariants' => array(
            'key_from' => 'id',
            'model_to' => '\product\Model_OtherVariants',
            'key_to' => 'otherproduct_id',
            'cascade_save' => true,
            'cascade_delete' => false,
        )
        
    );
        
        private static $_others;
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->title;
                }
            }
            return $data;
	}
        
        public function get_other_name(){
            if(empty(self::$_others)){
                self::$_others = Model_Others::get_as_array();
            }
            $flag = $this->other_id;
            return isset(self::$_others[$flag]) ? self::$_others[$flag] : '-';
        }
	
	public function get_form_data_basic($other) {
		return array(
			'attributes' => array(
				'name' => 'frm_other_product',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Other',
						'id' => 'other_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'other_id',
						'value' => $this->other_id,
						'options' => $other,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Other',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Title',
						'id' => 'title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'title',
						'value' => $this->title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Seq',
						'id' => 'seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '0',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
			)
		);
	}
}
