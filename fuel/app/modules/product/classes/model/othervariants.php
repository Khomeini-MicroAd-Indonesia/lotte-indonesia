<?php
namespace Product;

class Model_OtherVariants extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	//private $image_path = 'media/product/';
	
	protected static $_table_name = 'othervariants';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'product'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'product'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'product'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'otherproduct_id' => array(
                    'label' => 'Other Product',
                    'validation' => array(
                            'required',
                    )
		),
                'image_id' => array(
                    'label' => 'Image',
                    'validation' => array(
                            'required',
                    )
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
                'title' => array(
			'label' => 'Title',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'desc' => array(
                        'label' => 'Desc',
                        'validation' => array()
                ),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
        
//        protected static $_belongs_to = array(
//            
//            'otherproducts' => array(
//                'key_from' => 'otherproduct_id',
//                'model_to' => '\product\Model_OtherProducts',
//                'key_to' => 'id',
//                'cascade_save' => true,
//                'cascade_delete' => false,
//            ),
//            'images' => array(
//                'key_from' => 'image_id',
//                'model_to' => '\product\Model_ProductImages',
//                'key_to' => 'id',
//                'cascade_save' => true,
//                'cascade_delete' => false,
//            )
//        
//        );
        
        protected static $_belongs_to = array(
        
            'images' => array(
                'key_from' => 'image_id',
                'model_to' => '\product\Model_ProductImages',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        );
        
        private static $_otherproducts, $_images;
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->title;
                }
            }
            return $data;
	}
        
        public function get_otherproduct_name(){
            if(empty(self::$_otherproducts)){
                self::$_otherproducts = Model_OtherProducts::get_as_array();
            }
            $flag = $this->otherproduct_id;
            return isset(self::$_otherproducts[$flag]) ? self::$_otherproducts[$flag] : '-';
        }
        
        public function get_image_name(){
            if(empty(self::$_images)){
                self::$_images = Model_ProductImages::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
	
	public function get_form_data_basic($otherproduct, $image) {
		return array(
			'attributes' => array(
				'name' => 'frm_other_product',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                                array(
                                    'label' => array(
                                        'label' => 'Other Product',
                                        'id' => 'otherproduct_id',
                                        'attributes' => array(
                                            'class' => 'col-sm-2 control-label'
                                        )
                                    ),
                                    'select' => array(
                                        'name' => 'otherproduct_id',
                                        'value' => $this->otherproduct_id,
                                        'options' => $otherproduct,
                                        'attributes' => array(
                                            'class' => 'form-control bootstrap-select',
                                            'placeholder' => 'Other Product',
                                            'data-live-search' => 'true',
                                            'data-size' => '3',
                                        ),
                                        'container_class' => 'col-sm-10'
                                    )
				),
                                array(
                                    'label' => array(
                                        'label' => 'Image',
                                        'id' => 'image_id',
                                        'attributes' => array(
                                            'class' => 'col-sm-2 control-label'
                                        )
                                    ),
                                    'select' => array(
                                        'name' => 'image_id',
                                        'value' => $this->image_id,
                                        'options' => $image,
                                        'attributes' => array(
                                            'class' => 'form-control bootstrap-select',
                                            'placeholder' => 'Image',
                                            'data-live-search' => 'true',
                                            'data-size' => '3',
                                        ),
                                        'container_class' => 'col-sm-10'
                                    )
				),
                                array(
                                    'label' => array(
                                        'label' => 'Status',
                                        'id' => 'status',
                                        'attributes' => array(
                                            'class' => 'col-sm-2 control-label'
                                        )
                                    ),
                                    'select' => array(
                                        'name' => 'status',
                                        'value' => $this->status,
                                        'options' => $this->status_name,
                                        'attributes' => array(
                                            'class' => 'form-control',
                                            'placeholder' => 'Status',
                                            'required' => ''
                                        ),
                                        'container_class' => 'col-sm-10'
                                    )
				),
				array(
                                    'label' => array(
                                        'label' => 'Seq',
                                        'id' => 'seq',
                                        'attributes' => array(
                                            'class' => 'col-sm-2 control-label'
                                        )
                                    ),
                                    'input' => array(
                                        'name' => 'seq',
                                        'value' => $this->seq,
                                        'attributes' => array(
                                            'class' => 'form-control',
                                            'placeholder' => '0',
                                            'required' => '',
                                        ),
                                        'container_class' => 'col-sm-10'
                                    )
				),
                                array(
                                    'label' => array(
                                        'label' => 'Title',
                                        'id' => 'title',
                                        'attributes' => array(
                                            'class' => 'col-sm-2 control-label'
                                        )
                                    ),
                                    'input' => array(
                                        'name' => 'title',
                                        'value' => $this->title,
                                        'attributes' => array(
                                            'class' => 'form-control',
                                            'placeholder' => 'Title',
                                            'required' => '',
                                        ),
                                        'container_class' => 'col-sm-10'
                                    )
				),
                                array(
                                    'label' => array(
                                        'label' => 'Description',
                                        'id' => 'desc',
                                        'attributes' => array(
                                            'class' => 'col-sm-2 control-label'
                                        )
                                    ),
                                    'textarea' => array(
                                        'name' => 'desc',
                                        'value' => $this->desc,
                                        'attributes' => array(
                                            'class' => 'form-control ckeditor',
                                            'placeholder' => '',
                                        ),
                                        'container_class' => 'col-sm-10'
                                    )
				)
                                
			)
		);
	}
}
