<?php
namespace Product;

class Model_ProductTVCs extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	private $image_path = 'media/product/';
	
	protected static $_table_name = 'product_tvcs';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'product'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'product'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'product'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'name' => array(
			'label' => 'TVC Name',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'slug' => array(
                        'label' => 'TVC Slug',
                        'validation' => array(
                                'required',
                                'max_length' => array(100),
                        )
                ),
                'link' => array(
                        'label' => 'TVC Link',
                        'validation' => array(
                                'required',
                                'max_length' => array(100),
                        )
                ),
		'filename' => array(
			'label' => 'TVC Filename',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
                'desc' => array(
                        'label' => 'Product Desc',
                        'validation' => array()
                ),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
        
        protected static $_has_many = array(
            'product' => array(
                'key_from'          =>  'id',
                'model_to'          =>  '\Product\Model_Products',
                'key_to'            =>  'image_id',
                'cascade_save'      =>  false,
                'cascade_delete'    =>  false
                
            )
        );
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_image_path() {
		return $this->image_path;
	}
	
        public function get_all_images() {
		if (file_exists(DOCROOT.$this->image_path)) {
			$contents = \File::read_dir(DOCROOT.$this->image_path);
		} else {
			$contents = array();
                        //$contents = "http://localhost:8888/MicroAd/tokyuland/assets/img/pdf-icon.png";
		}
		return $contents;
	}
        
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
	
	public function get_form_data_basic() {
		return array(
			'attributes' => array(
				'name' => 'frm_product_tvc',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'TVC Name',
						'id' => 'tvc_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'tvc_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'TVC Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Slug',
						'id' => 'slug',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'slug',
						'value' => $this->slug,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Slug',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Link',
						'id' => 'link',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'link',
						'value' => $this->link,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Link',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Seq',
						'id' => 'tvc_seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'tvc_seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '0',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'TVC Desc',
						'id' => 'tvc_desc',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'tvc_desc',
						'value' => $this->desc,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				
				array(
					'label' => array(
						'label' => 'TVC',
						'id' => 'tvc_filename',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select_image_picker' => array(
						'name' => 'tvc_filename',
						'value' => $this->filename,
						'options' => $this->get_all_images(),
						'attributes' => array(
							'class' => 'form-control image-picker',
						),
						'container_class' => 'col-sm-10',
						'image_url' => \Uri::base().$this->image_path,
					)
				),
			)
		);
	}
}
