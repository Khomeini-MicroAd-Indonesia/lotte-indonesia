<?php
namespace Product;

class Model_ProductVariants extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	private $image_path = 'media/product/';
	
	protected static $_table_name = 'product_variants';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'product'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'product'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'product'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'slug' => array(
                        'label' => 'Variant Slug',
                        'validation' => array(
                                'required',
                                'max_length' => array(100),
                        )
                ),
                'product_id' => array(
			'label' => 'Product',
			'validation' => array(
				'required',
			)
		),
                'title' => array(
			'label' => 'Variant Title',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'image_id' => array(
			'label' => 'Image',
			'validation' => array(
				'required',
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
                'desc' => array(
                        'label' => 'Variant Desc',
                        'validation' => array()
                ),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
        
        protected static $_belongs_to = array(
            'products' => array(
                'key_from' => 'product_id',
                'model_to' => '\product\Model_Products',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'images' => array(
                'key_from' => 'image_id',
                'model_to' => '\product\Model_ProductImages',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        
        );
	
        private static $_products;
        private static $_images;
        
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_product_name(){
            if(empty(self::$_products)){
                self::$_products = Model_Products::get_as_array();
            }
            $flag = $this->product_id;
            return isset(self::$_products[$flag]) ? self::$_products[$flag] : '-';
        }
        
        public function get_image_name(){
            if(empty(self::$_images)){
                self::$_images = Model_ProductImages::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
        
        public function get_all_images() {
		if (file_exists(DOCROOT.$this->image_path)) {
			$contents = \File::read_dir(DOCROOT.$this->image_path);
		} else {
			$contents = array();
                        //$contents = "http://localhost:8888/MicroAd/tokyuland/assets/img/pdf-icon.png";
		}
		return $contents;
	}
        
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->title;
                }
            }
            return $data;
	}
	
	public function get_form_data_basic($product, $image) {
            return array(
			'attributes' => array(
				'name' => 'frm_product_variant',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                                array(
					'label' => array(
						'label' => 'Product Name',
						'id' => 'product_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'product_name',
						'value' => $product,//$this->name,
                                                'options' => $product,
						'attributes' => array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Product',
                                                    'data-live-search' => 'true',
                                                    'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Title',
						'id' => 'variant_title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'variant_title',
						'value' => $this->title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Slug',
						'id' => 'slug',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'slug',
						'value' => $this->slug,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Slug',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Image Name',
						'id' => 'image_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'image_name',
						'value' => $image,//$this->name,
                                                'options' => $image,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Image Name',
                                                    'data-live-search' => 'true',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Status',
                                                    'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Seq',
						'id' => 'variant_seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'variant_seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '0',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Variant Desc',
						'id' => 'variant_desc',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'variant_desc',
						'value' => $this->desc,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
