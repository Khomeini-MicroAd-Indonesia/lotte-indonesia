<?php

class Controller_Frontend extends Controller_Mama {

    protected $_data_template = array(
        'meta_title' => '',
        'meta_desc' => '',
        'frontend_menus' => array(),
        'menu_parent_key' => '',
        'menu_current_key' => '',
    );
    protected $_check_lang = true;
    protected $_valid_lang = array(
        'en' => array(
            'active' => false,
            'label' => 'English',
            'label_mobile' => 'English',
            'link' => '',
            'icon' => 'assets/img/icon-lang-eng-grey.png',
            'class' => 'eng',
        ),
        'id' => array(
            'active' => false,
            'label' => 'Bahasa',
            'label_mobile' => 'Bahasa',
            'link' => '',
            'icon' => 'assets/img/icon-lang-ina-grey.png',
            'class' => 'ind',
        ),
    );
    protected $_current_lang = 'id';

    private function _handle_multilanguage() {
        $_change_lang_url = '/home';
        if ($this->_check_lang) {
            $_is_lang_valid = true;
            $temp_lang = $this->param('lang_code');
            if (empty($temp_lang)) {
                $_is_lang_valid = false;
            } else {
                if (isset($this->_valid_lang[$temp_lang])) {
                    $this->_current_lang = $temp_lang;
                    $this->_valid_lang[$temp_lang]['active'] = true;
                    $tmp = $this->_valid_lang[$temp_lang]['icon'];
                    $tmp1 = substr($tmp, 0, strpos($tmp, "-grey"));
                    $tmp2 = substr($tmp, strpos($tmp, "-grey")+5);
                    $this->_valid_lang[$temp_lang]['icon'] = $tmp1.$tmp2;
                } else {
                    $_is_lang_valid = false;
                    \Log::error('Lang code ' . $temp_lang . ' is Invalid!');
                }
            }
            if (!$_is_lang_valid) {
                \Response::redirect(\Uri::base() . $this->_current_lang . '/home');
            }
            $temp = '';
            foreach (\Uri::segments() as $key => $value) {
                if ($key > 0) {
                    $temp .= '/' . $value;
                } else {
                    if (!isset($this->_valid_lang[$value])) {
                        break;
                    }
                }
            }
            if (!empty($temp)) {
                $_change_lang_url = $temp;
                unset($temp);
            }
        }
        foreach ($this->_valid_lang as $key => $value) {
            $this->_valid_lang[$key]['link'] = \Uri::base() . $key . $_change_lang_url;
        }
        $this->_data_template['valid_lang'] = $this->_valid_lang;
        $this->_data_template['current_lang'] = $this->_current_lang;
        \Config::set('language', $this->_current_lang);
        \Lang::load($this->_current_lang);
    }

    public function before() {
        parent::before();
        $this->_handle_multilanguage();
        $this->fetch_config_data('facebook');
        $this->fetch_config_data('twitter');
        $this->_data_template['product_submenus'] = \Product\Model_Products::query()
                ->where('status', 1)
                ->order_by('post_date')
                ->get();
        $this->_data_template['frontend_menus'] = array(
            'home' => array(
                'lang_code' => 'mn_home',
                'label' => 'HOME',
                'route' => \Uri::base().$this->_current_lang.'/home',
                'menu_id' => 'menu-',
                'menu_class' => 'menu-item',
            ),
            'about_us' => array(
                'lang_code' => 'mn_about_us',
                'label' => 'ABOUT US',
                'route' => \Uri::base().$this->_current_lang.'/about-us',
                'menu_id' => 'menu-',
                'menu_class' => 'menu-item',
            ),
            'product' => array(
                'lang_code' => 'mn_product',
                'label' => 'PRODUCT',
                'route' => \Uri::base().$this->_current_lang.'/product',
                'menu_id' => 'menu-',
                'menu_class' => 'menu-item',
            ),
            'news_and_events' => array(
                'lang_code' => 'mn_news_and_events',
                'label' => 'NEWS & EVENTS',
                'route' => \Uri::base().$this->_current_lang.'/news_and_events',
                'menu_id' => 'menu-',
                'menu_class' => 'menu-item',
            ),
            'careers' => array(
                'lang_code' => 'mn_careers',
                'label' => 'CAREERS',
                'route' => \Uri::base().$this->_current_lang.'/career',
                'menu_id' => 'menu-',
                'menu_class' => 'menu-item',
            ),
            'contact_us' => array(
                'lang_code' => 'mn_contact_us',
                'label' => 'CONTACT US',
                'route' => \Uri::base().$this->_current_lang.'/contact-us',
                'menu_id' => 'menu-',
                'menu_class' => 'menu-item',
            ),
        );
    }

    protected function set_meta_info($current_route) {
        $this->_data_template['meta_title'] = \Config::get('config_basic.app_name');
        $model_page = \Pages\Model_Pages::query()
                ->where('url_path', $current_route)
                ->where('status', 1)
                ->get_one();
        if (!empty($model_page)) {
            $this->_data_template['page_title'] = $model_page->title;
            $this->_data_template['page_content'] = $model_page->content;
            if (strlen($model_page->meta_title) > 0) {
                $this->_data_template['meta_title'] .= ' - ' . $model_page->meta_title;
            }
            $this->_data_template['meta_desc'] = $model_page->meta_desc;
        }
    }
    
    protected function get_section_page($path){
        $data = \Pages\Model_Pages::query()
            ->where('url_path', $path)
            ->where('status', 1)
            ->get_one();
        return $data;
    }

}
