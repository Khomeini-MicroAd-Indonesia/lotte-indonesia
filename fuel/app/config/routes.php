<?php

return array(
    '_404_' => 'welcome/404', // The main 404 route
    '_root_' => 'pages/frontend/home', // The default route
    
    
// Backend //
    
    // Installation
    'backend/installation' => 'backend/install/setup',
    
    // Backend routes
    'backend' => 'backend/dashboard', // The default route for backend
    'backend/sign-in' => 'backend/dashboard/sign_in',
    'backend/sign-out' => 'backend/dashboard/sign_out',
    'backend/change-password' => 'backend/dashboard/change_current_password',
    'backend/my-profile' => 'backend/dashboard/my_profile_form',
    'backend/no-permission' => 'error/no_permission/backend',

    
    
// Admin Management //
    
    // Admin User
    'backend/admin-user' => 'adminmanagement/adminuser',
    'backend/admin-user/add' => 'adminmanagement/adminuser/form/0',
    'backend/admin-user/edit/(:num)' => 'adminmanagement/adminuser/form/$1',
    'backend/admin-user/delete/(:num)' => 'adminmanagement/adminuser/delete/$1',
    'backend/admin-user/reset-password/(:num)' => 'adminmanagement/adminuser/reset_password/$1',
    
    // Admin Role Permission
    'backend/admin-role-permission' => 'adminmanagement/adminrolepermission',
    'backend/admin-role-permission/add' => 'adminmanagement/adminrolepermission/form/0',
    'backend/admin-role-permission/edit/(:num)' => 'adminmanagement/adminrolepermission/form/$1',
    'backend/admin-role-permission/delete/(:num)' => 'adminmanagement/adminrolepermission/delete/$1',
    'backend/admin-role-permission/assign-admin/(:num)' => 'adminmanagement/adminrolepermission/assign_admin/$1',
    'backend/admin-role-permission/do-assign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/1',
    'backend/admin-role-permission/do-unassign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/0',
    'backend/admin-role-permission/set-permission/(:num)' => 'adminmanagement/adminrolepermission/set_permission/$1',
    
    // Basic Setting
    'backend/setting' => 'adminmanagement/setting',
    
    
// Facebook Management //
    'backend/fb-participant' => 'facebookmanagement/participant',
    'backend/fb-setting' => 'facebookmanagement/setting',
    
    // Twitter Management
    'backend/twitter-setting' => 'twittermanagement/setting',
    
// Home Banner //
    
    // Organize
    'backend/homebanner' => 'homebanner/backend',
    'backend/homebanner/add' => 'homebanner/backend/form/0',
    'backend/homebanner/edit/(:num)' => 'homebanner/backend/form/$1',
    'backend/homebanner/delete/(:num)' => 'homebanner/backend/delete/$1',
    
    // Image
    'backend/homebanner/image' => 'homebanner/backend/image',
    'backend/homebanner/image/add' => 'homebanner/backend/image/form/0',
    'backend/homebanner/image/edit/(:num)' => 'homebanner/backend/image/form/$1',
    'backend/homebanner/image/delete/(:num)' => 'homebanner/backend/image/delete/$1',
    
    // Logo
    'backend/homebanner/logo' => 'homebanner/backend/logo',
    'backend/homebanner/logo/add' => 'homebanner/backend/logo/form/0',
    'backend/homebanner/logo/edit/(:num)' => 'homebanner/backend/logo/form/$1',
    'backend/homebanner/logo/delete/(:num)' => 'homebanner/backend/logo/delete/$1',
    

// Pages //
    'backend/pages' => 'pages/backend',
    'backend/pages/add' => 'pages/backend/form/0',
    'backend/pages/edit/(:num)' => 'pages/backend/form/$1',
    'backend/pages/delete/(:num)' => 'pages/backend/delete/$1',
    

// Media Uploader //

    // Banner
    'backend/media-uploader/homebanner' => 'mediauploader/homebanner',
    'backend/media-uploader/homebanner/upload' => 'mediauploader/homebanner/upload',

    // Event
    'backend/media-uploader/event' => 'mediauploader/event',
    'backend/media-uploader/event/upload' => 'mediauploader/event/upload',

    // Other
    'backend/media-uploader/other' => 'mediauploader/other',
    'backend/media-uploader/other/upload' => 'mediauploader/other/upload',

    // Product
    'backend/media-uploader/product' => 'mediauploader/product',
    'backend/media-uploader/product/upload' => 'mediauploader/product/upload',

    // Promo
    'backend/media-uploader/promo' => 'mediauploader/promo',
    'backend/media-uploader/promo/upload' => 'mediauploader/promo/upload',

    // Achievement
    'backend/media-uploader/achievement' => 'mediauploader/achievement',
    'backend/media-uploader/achievement/upload' => 'mediauploader/achievement/upload',
    
    // Core Value
    'backend/media-uploader/core-value' => 'mediauploader/corevalue',
    'backend/media-uploader/core-value/upload' => 'mediauploader/corevalue/upload',
    

// Career //
    'backend/career' => 'career/backend',
    'backend/career/add' => 'career/backend/form/0',
    'backend/career/edit/(:num)' => 'career/backend/form/$1',
    'backend/career/delete/(:num)' => 'career/backend/delete/$1',
    
    // Channel
    
    // Home Banner or Main Banner
    'backend/channel/home-banner' => 'channel/backend/homebanner',
    'backend/channel/home-banner/add' => 'channel/backend/homebanner/form/0',
    'backend/channel/home-banner/edit/(:num)' => 'channel/backend/homebanner/form/$1',
    'backend/channel/home-banner/rearrange/(:num)' => 'channel/backend/homebanner/rearrange/$1',
    'backend/channel/home-banner/delete/(:num)' => 'channel/backend/homebanner/delete/$1',
    
// Event //
    
    // Organize
    'backend/event' => 'event/backend',
    'backend/event/add' => 'event/backend/form/0',
    'backend/event/edit/(:num)' => 'event/backend/form/$1',
    'backend/event/delete/(:num)' => 'event/backend/delete/$1',
    
    // Banner
    'backend/event/banner' => 'event/backend/banner',
    'backend/event/banner/add' => 'event/backend/banner/form/0',
    'backend/event/banner/edit/(:num)' => 'event/backend/banner/form/$1',
    'backend/event/banner/delete/(:num)' => 'event/backend/banner/delete/$1',
    
    // Slide Image
    'backend/event/slideimage' => 'event/backend/slideimage',
    'backend/event/slideimage/add' => 'event/backend/slideimage/form/0',
    'backend/event/slideimage/edit/(:num)' => 'event/backend/slideimage/form/$1',
    'backend/event/slideimage/delete/(:num)' => 'event/backend/slideimage/delete/$1',
    
    // Image
    'backend/event/image' => 'event/backend/image',
    'backend/event/image/add' => 'event/backend/image/form/0',
    'backend/event/image/edit/(:num)' => 'event/backend/image/form/$1',
    'backend/event/image/delete/(:num)' => 'event/backend/image/delete/$1',
    
    // Publication
    'backend/event/publication' => 'event/backend/publication',
    'backend/event/publication/add' => 'event/backend/publication/form/0',
    'backend/event/publication/edit/(:num)' => 'event/backend/publication/form/$1',
    'backend/event/publication/delete/(:num)' => 'event/backend/publication/delete/$1',
    

// Product //

    // Organize
    'backend/product' => 'product/backend',
    'backend/product/add' => 'product/backend/form/0',
    'backend/product/edit/(:num)' => 'product/backend/form/$1',
    'backend/product/delete/(:num)' => 'product/backend/delete/$1',

    // Banner
    'backend/product/banner' => 'product/backend/banner',
    'backend/product/banner/add' => 'product/backend/banner/form/0',
    'backend/product/banner/edit/(:num)' => 'product/backend/banner/form/$1',
    'backend/product/banner/delete/(:num)' => 'product/backend/banner/delete/$1',

    // Image
    'backend/product/image' => 'product/backend/image',
    'backend/product/image/add' => 'product/backend/image/form/0',
    'backend/product/image/edit/(:num)' => 'product/backend/image/form/$1',
    'backend/product/image/delete/(:num)' => 'product/backend/image/delete/$1',

    // Variant
    'backend/product/variant' => 'product/backend/variant',
    'backend/product/variant/add' => 'product/backend/variant/form/0',
    'backend/product/variant/edit/(:num)' => 'product/backend/variant/form/$1',
    'backend/product/variant/delete/(:num)' => 'product/backend/variant/delete/$1',

    // TVC
    'backend/product/tvc' => 'product/backend/tvc',
    'backend/product/tvc/add' => 'product/backend/tvc/form/0',
    'backend/product/tvc/edit/(:num)' => 'product/backend/tvc/form/$1',
    'backend/product/tvc/delete/(:num)' => 'product/backend/tvc/delete/$1',

    // Social Media
    'backend/product/socmed' => 'product/backend/socmed',
    'backend/product/socmed/add' => 'product/backend/socmed/form/0',
    'backend/product/socmed/edit/(:num)' => 'product/backend/socmed/form/$1',
    'backend/product/socmed/delete/(:num)' => 'product/backend/socmed/delete/$1',

    // Instagram
    'backend/product/instagram' => 'product/backend/instagram',
    'backend/product/instagram/add' => 'product/backend/instagram/form/0',
    'backend/product/instagram/edit/(:num)' => 'product/backend/instagram/form/$1',
    'backend/product/instagram/delete/(:num)' => 'product/backend/instagram/delete/$1',

    // Others
    'backend/product/other' => 'product/backend/other',
    'backend/product/other/add' => 'product/backend/other/form/0',
    'backend/product/other/edit/(:num)' => 'product/backend/other/form/$1',
    'backend/product/other/delete/(:num)' => 'product/backend/other/delete/$1',

    // Other Products
    'backend/product/otherproduct' => 'product/backend/otherproduct',
    'backend/product/otherproduct/add' => 'product/backend/otherproduct/form/0',
    'backend/product/otherproduct/edit/(:num)' => 'product/backend/otherproduct/form/$1',
    'backend/product/otherproduct/delete/(:num)' => 'product/backend/otherproduct/delete/$1',

    // Other Variants
    'backend/product/othervariant' => 'product/backend/othervariant',
    'backend/product/othervariant/add' => 'product/backend/othervariant/form/0',
    'backend/product/othervariant/edit/(:num)' => 'product/backend/othervariant/form/$1',
    'backend/product/othervariant/delete/(:num)' => 'product/backend/othervariant/delete/$1',
    
    
    
// Promo //
    
    // Organize
    'backend/promo' => 'promo/backend',
    'backend/promo/add' => 'promo/backend/form/0',
    'backend/promo/edit/(:num)' => 'promo/backend/form/$1',
    'backend/promo/delete/(:num)' => 'promo/backend/delete/$1',
    
    // Mechanism
    'backend/promo/mechanism' => 'promo/backend/mechanism',
    'backend/promo/mechanism/add' => 'promo/backend/mechanism/form/0',
    'backend/promo/mechanism/edit/(:num)' => 'promo/backend/mechanism/form/$1',
    'backend/promo/mechanism/delete/(:num)' => 'promo/backend/mechanism/delete/$1',
    
    // Banner
    'backend/promo/banner' => 'promo/backend/banner',
    'backend/promo/banner/add' => 'promo/backend/banner/form/0',
    'backend/promo/banner/edit/(:num)' => 'promo/backend/banner/form/$1',
    'backend/promo/banner/delete/(:num)' => 'promo/backend/banner/delete/$1',
    
    // Image
    'backend/promo/image' => 'promo/backend/image',
    'backend/promo/image/add' => 'promo/backend/image/form/0',
    'backend/promo/image/edit/(:num)' => 'promo/backend/image/form/$1',
    'backend/promo/image/delete/(:num)' => 'promo/backend/image/delete/$1',
    
    // Prize
    'backend/promo/prize' => 'promo/backend/prize',
    'backend/promo/prize/add' => 'promo/backend/prize/form/0',
    'backend/promo/prize/edit/(:num)' => 'promo/backend/prize/form/$1',
    'backend/promo/prize/delete/(:num)' => 'promo/backend/prize/delete/$1',
    

// Achievement //
    'backend/achievement' => 'achievement/backend',
    'backend/achievement/add' => 'achievement/backend/form/0',
    'backend/achievement/edit/(:num)' => 'achievement/backend/form/$1',
    'backend/achievement/delete/(:num)' => 'achievement/backend/delete/$1',
    
    

// Frontend //
    
    '(:lang_code)/home' => 'pages/frontend/home',
    
    // Event
    '(:lang_code)/event' => 'event/frontend/home',
    '(:lang_code)/event/detail/(:slug)' => 'event/frontend/home/detail',
    '(:lang_code)/event/(:selected_year)/(:selected_month)' => 'event/frontend/home',
    '(:lang_code)/event/(:selected_year)/(:selected_month)' => 'event/frontend/home',
    
    // Product
    '(:lang_code)/product-details/(:slug)' => 'product/frontend/home/detail',
    '(:lang_code)/product-others' => 'product/frontend/home/other',
    '(:lang_code)/get-product-socmed' => 'product/frontend/home/get_socmed',
    
    // Promo
    '(:lang_code)/promo' => 'promo/frontend/home',
    '(:lang_code)/promo/detail/(:slug)' => 'promo/frontend/home/detail',
    
    // Career
    '(:lang_code)/career' => 'career/frontend/home',
    
    // About Us
    '(:lang_code)/about-us' => 'pages/frontend/home/about_us',
    
    // Contact Us
    '(:lang_code)/contact-us' => 'pages/frontend/home/contact_us',
    
    // Search 
    '(:lang_code)/search' => 'pages/frontend/home/search', 
    
// History //
    'backend/history' => 'history/backend',
    'backend/history/add' => 'history/backend/form/0',
    'backend/history/edit/(:num)' => 'history/backend/form/$1',
    'backend/history/delete/(:num)' => 'history/backend/delete/$1',
    
    // Corporate Core Values
    'backend/corp-core-values' => 'corpcorevalues/backend',
    'backend/corp-core-values/add' => 'corpcorevalues/backend/form/0',
    'backend/corp-core-values/edit/(:num)' => 'corpcorevalues/backend/form/$1',
    'backend/corp-core-values/delete/(:num)' => 'corpcorevalues/backend/delete/$1',
);
