<?php

return array(
    'cms_name' => 'LOTTE INDONESIA',
    'max_lock_count' => 5,
    'cms_session_name' => array(
        'admin_id' => 'LotteIndonesia'
    ),
    'admin_default_password' => 'lotte',
    'max_email_sent_daily' => 500,
    'max_email_sent_per_task' => 10,
    'menus' => array(
        'dashboard' => array(
            'label' => 'Dashboard',
            'route' => 'backend',
            'icon_class' => 'fa fa-dashboard',
            'permission' => false,
        ),
        'pages' => array(
            'label' => 'Pages',
            'route' => 'backend/pages',
            'icon_class' => 'fa fa-sitemap',
            'permission' => true,
        ),
        'admin_management' => array(
            'label' => 'Admin Management',
            'icon_class' => 'fa fa-columns',
            'permission' => false,
            'submenus' => array(
                'admin_user' => array(
                    'label' => 'Admin User',
                    'route' => 'backend/admin-user',
                    'icon_class' => 'fa fa-users',
                    'permission' => true,
                ),
                'admin_role_permission' => array(
                    'label' => 'Admin Role Permission',
                    'route' => 'backend/admin-role-permission',
                    'icon_class' => 'fa fa-shield',
                    'permission' => true,
                ),
                'admin_setting' => array(
                    'label' => 'Basic Setting',
                    'route' => 'backend/setting',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'admin_role_permission' => array(
                    'label' => 'Admin Role Permission',
                    'route' => 'backend/admin-role-permission',
                    'icon_class' => 'fa fa-shield',
                    'permission' => true,
                )
            )
        ),
        'admin_fb_management' => array(
            'label' => 'Facebook Management',
            'icon_class' => 'fa fa-facebook',
            'permission' => false,
            'submenus' => array(
                'admin_fb_participant' => array(
                    'label' => 'Participant',
                    'route' => 'backend/fb-participant',
                    'icon_class' => 'fa fa-users',
                    'permission' => true,
                ),
                'admin_fb_setting' => array(
                    'label' => 'Facebook Setting',
                    'route' => 'backend/fb-setting',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
            )
        ),
        'admin_twitter_management' => array(
            'label' => 'Twitter Management',
            'icon_class' => 'fa fa-twitter',
            'permission' => false,
            'submenus' => array(
                'admin_twitter_setting' => array(
                    'label' => 'Twitter Setting',
                    'route' => 'backend/twitter-setting',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
            )
        ),
        'career' => array(
            'label' => 'Career Management',
            'icon_class' => 'fa fa-suitcase',
            'permission' => false,
            'submenus' => array(
                'career_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/career/',
                    'icon_class' => 'fa fa-user-plus',
                    'permission' => true,
                )
            )
        ),
        'product_management' => array(
            'label' => 'Product Management',
            'icon_class' => 'fa fa-cube',
            'permission' => false,
            'submenus' => array(
                'product_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/product/',
                    'icon_class' => 'fa fa-cubes',
                    'permission' => true,
                ),
                'product_banner' => array(
                    'label' => 'Banner',
                    'route' => 'backend/product/banner',
                    'icon_class' => 'fa fa-picture-o',
                    'permission' => true,
                ),
                'product_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/product/image',
                    'icon_class' => 'fa fa-camera',
                    'permission' => true,
                ),
                'product_variant' => array(
                    'label' => 'Variant',
                    'route' => 'backend/product/variant',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                ),
                'product_tvc' => array(
                    'label' => 'TVC',
                    'route' => 'backend/product/tvc',
                    'icon_class' => 'fa fa-television',
                    'permission' => true,
                ),
                'product_socmed' => array(
                    'label' => 'Social Media',
                    'route' => 'backend/product/socmed',
                    'icon_class' => 'fa fa-facebook',
                    'permission' => true,
                ),
                'product_instagram' => array(
                    'label' => 'Instagram',
                    'route' => 'backend/product/instagram',
                    'icon_class' => 'fa fa-instagram',
                    'permission' => true,
                ),
                'product_other' => array(
                    'label' => 'Others',
                    'route' => 'backend/product/other',
                    'icon_class' => 'fa fa-gift',
                    'permission' => true,
                ),
                'product_other_product' => array(
                    'label' => 'Other Products',
                    'route' => 'backend/product/otherproduct',
                    'icon_class' => 'fa fa-gift-o',
                    'permission' => true,
                ),
                'product_other_variant' => array(
                    'label' => 'Other Variants',
                    'route' => 'backend/product/othervariant',
                    'icon_class' => 'fa fa-gift-o',
                    'permission' => true,
                ),
            )
        ),
        'history_management' => array(
            'label' => 'History Management',
            'route' => 'backend/history/',
            'icon_class' => 'fa fa-history',
            'permission' => false,
        ),
        'event_management' => array(
            'label' => 'Event Management',
            'icon_class' => 'fa fa-calendar',
            'permission' => false,
            'submenus' => array(
                'event_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/event/',
                    'icon_class' => 'fa fa-puzzle-piece',
                    'permission' => true,
                ),
                'event_banner' => array(
                    'label' => 'Banner',
                    'route' => 'backend/event/banner',
                    'icon_class' => 'fa fa-picture-o',
                    'permission' => true,
                ),
                'event_slideimage' => array(
                    'label' => 'Slide Image',
                    'route' => 'backend/event/slideimage',
                    'icon_class' => 'fa fa-tv',
                    'permission' => true,
                ),
                'event_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/event/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                ),
                'event_publication' => array(
                    'label' => 'Publication',
                    'route' => 'backend/event/publication',
                    'icon_class' => 'fa fa-book',
                    'permission' => true,
                )
            )
        ),
        'promo_management' => array(
            'label' => 'Promo Management',
            'icon_class' => 'fa fa-bolt',
            'permission' => false,
            'submenus' => array(
                'promo_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/promo/',
                    'icon_class' => 'fa fa-puzzle-piece',
                    'permission' => true,
                ),
                'promo_mechanism' => array(
                    'label' => 'Mechanism',
                    'route' => 'backend/promo/mechanism',
                    'icon_class' => 'fa fa-cogs',
                    'permission' => true,
                ),
                'promo_banner' => array(
                    'label' => 'Banner',
                    'route' => 'backend/promo/banner',
                    'icon_class' => 'fa fa-picture-o',
                    'permission' => true,
                ),
                'promo_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/promo/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                ),
                'promo_prize' => array(
                    'label' => 'Prize',
                    'route' => 'backend/promo/prize',
                    'icon_class' => 'fa fa-automobile',
                    'permission' => true,
                )
            )
        ),
        'homebanner_management' => array(
            'label' => 'Home Banner Management',
            'icon_class' => 'fa fa-map',
            'permission' => false,
            'submenus' => array(
                'homebanner_organizer' => array(
                    'label' => 'Organize',
                    'route' => 'backend/homebanner/',
                    'icon_class' => 'fa fa-puzzle-piece',
                    'permission' => true,
                ),
                'homebanner_image' => array(
                    'label' => 'Image',
                    'route' => 'backend/homebanner/image',
                    'icon_class' => 'fa fa-camera-retro',
                    'permission' => true,
                ),
                'homebanner_logo' => array(
                    'label' => 'Logo',
                    'route' => 'backend/homebanner/logo',
                    'icon_class' => 'fa fa-bug',
                    'permission' => true,
                )
            )
        ),
        'achievement' => array(
            'label' => 'Achievements',
            'route' => 'backend/achievement',
            'icon_class' => 'fa fa-asterisk',
            'permission' => true,
        ),
        'corp_core_values' => array(
            'label' => 'Corporate Core Values',
            'route' => 'backend/corp-core-values',
            'icon_class' => 'fa fa-building',
            'permission' => true,
        ),
        'media_uploader' => array(
            'label' => 'Media Uploader',
            'icon_class' => 'fa fa-upload',
            'permission' => false,
            'submenus' => array(
                'media_uploader_banner' => array(
                    'label' => 'Home Banner',
                    'route' => 'backend/media-uploader/homebanner',
                    'icon_class' => 'fa fa-map-o',
                    'permission' => true,
                ),
                'media_uploader_event' => array(
                    'label' => 'Event',
                    'route' => 'backend/media-uploader/event',
                    'icon_class' => 'fa fa-calendar-o',
                    'permission' => true,
                ),
                'media_uploader_product' => array(
                    'label' => 'Product',
                    'route' => 'backend/media-uploader/product',
                    'icon_class' => 'fa fa-cube',
                    'permission' => true,
                ),
                'media_uploader_promo' => array(
                    'label' => 'Promo',
                    'route' => 'backend/media-uploader/promo',
                    'icon_class' => 'fa fa-bolt',
                    'permission' => true,
                ),
                'media_uploader_achievement' => array(
                    'label' => 'Achievement',
                    'route' => 'backend/media-uploader/achievement',
                    'icon_class' => '',
                    'permission' => true,
                ),
                'media_uploader_core_value' => array(
                    'label' => 'Core Value',
                    'route' => 'backend/media-uploader/core-value',
                    'icon_class' => '',
                    'permission' => true,
                ),
            ),
        )
    ),
);
